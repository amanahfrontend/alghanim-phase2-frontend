import { AuthGuardGuard } from './../api-module/guards/auth-guard.guard';
import { RouterModule, Routes } from '@angular/router';
import { SharedModuleModule } from './../shared-module/shared-module.module';
import { LoginComponentComponent } from './../login-register-module/login-component/login-component.component';
import { HomePageComponent } from './../login-register-module/home-page/home-page.component';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { MatDialogModule } from '@angular/material';
import { MatSnackBar, MatSnackBarModule } from '@angular/material';

const routes: Routes = [
  {
    path: '',
    component: HomePageComponent,
    canActivate: [AuthGuardGuard],
    data: { roles: ['CallCenter', 'Maintenance', 'Supervisor', 'Admin', 'Dispatcher'] }
  },
  { path: 'login', component: LoginComponentComponent },
];

@NgModule({
  imports: [
    SharedModuleModule,
    RouterModule.forChild(routes),
    TranslateModule,
    MatDialogModule,
    MatSnackBarModule
  ],
  declarations: [
    LoginComponentComponent,
    HomePageComponent,
  ]
})
export class LoginRegisterModuleModule {
}
