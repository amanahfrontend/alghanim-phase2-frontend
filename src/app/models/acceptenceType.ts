export enum AcceptenceType {
  NoAction = 1,
  Accepted = 2,
  Rejected = 3,
}
