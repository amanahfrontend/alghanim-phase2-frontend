import { AcceptenceType } from "./acceptenceType";

export class OrderCardViewModel {
  id: number;
  long: number;
  lat: number;
  code: string;
  customerName: string;
  saP_AreaName: string;
  saP_BlockName: string;
  areaName: string;
  orderTypeCode: string;
  priorityName: string;
  saP_CreatedDate: Date;
  isExceedTime: boolean;
  isRepeatedCall: boolean;
  rejectionReason: string;
  rankInTeam: number;
  rankInDispatcher: number;
  acceptanceFlag: AcceptenceType;
  statusName: string;
}
