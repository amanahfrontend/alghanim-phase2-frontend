export class MapOrderModel {
  code: string = null;
  lat: number;
  long: number;
  pin: string = null;
}
