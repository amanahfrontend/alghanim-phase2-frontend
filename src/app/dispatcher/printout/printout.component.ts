import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { LookupService } from '../../api-module/services/lookup-services/lookup.service';
import { Printd } from 'printd';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-printout',
  templateUrl: './printout.component.html',
  styleUrls: ['./printout.component.css']
})
export class PrintoutComponent implements OnInit {
  cornerMessage: any[];
  fetchingDataDone: boolean = true;
  toggleLoading: boolean = false;
  printedData: any[] = [];
  numberOfOrders: number = null;
  foremen: any[] = [];
  teams: any[] = [];
  date_from: Date;
  date_to: Date;
  selected_foremen: any[] = [];
  selected_teams: any = [];
  includeOpenOrders: boolean = false;
  foremenWithOrders: any[] = [];

  filterObj = {
    dateFrom: null,
    dateTo: null,
    dispatcherId: +localStorage.getItem('AlghanimCurrentUserId'),
    teamsId: null,
    includeOpenOrders: false
  };

  printedSectionStyle = [`
  
  .orders-container {
    page-break-inside: avoid;
  }
  
  .order {
      display: flex;
      flex-direction: row;
      justify-content: flex-start;
      align-items: flex-start;
      padding: 10px;
      border: 01px solid #857c7c;
      margin: 20px;
      page-break-inside: avoid;
    }

    .order-left-column , .order-right-column {
      flex: 1;
    }

    .order-left-column p , .order-right-column p {
      margin: 0;
    }

    .order-left-column p span , .order-right-column p span {
      font-weight: bold;
      margin-right: 10px;
    }

    .orders-container h3 , .orders-container h5 {
      margin-top: 05px;
      margin-bottom: 05px;
    }
  `];

  @ViewChild('printoutForm', { static: false }) printoutForm: NgForm;
  @ViewChild('printedSection', { static: false }) printedSection: ElementRef;

  constructor(private lookup: LookupService) { }

  ngOnInit() {
    this.filterObj.teamsId = null;
    this.filterObj.includeOpenOrders = false;
    this.getData(this.filterObj);
    let dispatcherId = +localStorage.getItem('AlghanimCurrentUserId');
    this.lookup.getForemanByDispatcher(dispatcherId).subscribe(
      res => {
        console.log(res);
        this.foremen = res.data;
        this.foremen.map((f) => {
          f.label = f.fullName;
          f.value = +f.id;
        });
      }, err => console.log(err)
    );
  }

  onForemenSelect() {
    this.filterObj.dateFrom = this.date_from;
    this.filterObj.dateTo = this.date_to;
    this.selected_teams = [];
    if (this.selected_foremen.length > 0) {
      this.toggleLoading = true;
      let obj = { params: { formansId: this.selected_foremen } };
      this.lookup.getTeamsWithBlockedForForemen(obj).subscribe(
        res => {
          this.toggleLoading = false;
          this.teams = res.data;
          let teamsIds = this.teams.map(
            team => team = +team.id
          );
          this.filterObj.teamsId = teamsIds;
          this.filterObj.includeOpenOrders = this.includeOpenOrders;
        }, err => {
          console.log(err);
          this.toggleLoading = false;
        }
      );
    } else {
      this.filterObj.teamsId = null;
      this.filterObj.includeOpenOrders = this.includeOpenOrders;
    }
  }

  onTeamSelect() {
    this.filterObj.dateFrom = this.date_from;
    this.filterObj.dateTo = this.date_to;
    if (this.selected_teams.length > 0) {
      let teamsIds = this.selected_teams.map(
        team => team = +team.id
      );
      this.filterObj.teamsId = teamsIds;
      this.filterObj.includeOpenOrders = this.includeOpenOrders;
    } else {
      let teamsIds = this.teams.map(
        team => team = +team.id
      );
      this.filterObj.teamsId = teamsIds;
      this.filterObj.includeOpenOrders = this.includeOpenOrders;
    }
  }

  getData(obj) {
    this.toggleLoading = true;
    this.numberOfOrders = 0;
    this.lookup.getFilteredOrdersForPrint(obj).subscribe(
      (res: any) => {
        this.toggleLoading = false;
        this.printedData = [];
        this.foremenWithOrders = [];
        if (res && res.data && res.data[0]) {
          this.printedData = res.data[0];
          this.foremenWithOrders = res.data[0].foremenWithOrders || [];
        }
        for (let i = 0; i < this.foremenWithOrders.length; i++) {
          this.numberOfOrders = this.numberOfOrders + this.foremenWithOrders[i].orders.length;
        }
        this.foremenWithOrders.map(
          foreman => {
            foreman.pages = Math.ceil(foreman.orders.length / 4);
            /* foreman => pages => orders */
            foreman.ordersWithPages = [];
            for (let i = 1; i <= foreman.pages; i++) {
              foreman.ordersWithPages[i - 1] = [];
              for (let j = (i - 1) * 4; (j < foreman.orders.length) && (j < (i * 4)); j++) {
                foreman.ordersWithPages[i - 1].push(foreman.orders[j]);
              }
            }
          }
        );
      }, err => {
        console.log(err);
        this.toggleLoading = false;
      }
    );
  }

  onPrint() {
    let doc = new Printd();
    doc.print(this.printedSection.nativeElement, this.printedSectionStyle);
  }

  onReset(form: NgForm) {
    this.printoutForm.reset();
    this.selected_foremen = [];
    this.selected_teams = [];
    this.includeOpenOrders = false;
    this.filterObj.teamsId = null;
    this.filterObj.dateFrom = null;
    this.filterObj.dateTo = null;
    this.getData(this.filterObj);
  }

  onChangeIncludeOpenOrders() {
    if (this.selected_foremen.length > 0) {
      if (this.selected_teams.length > 0) {
        this.onTeamSelect();
      } else {
        this.onForemenSelect();
      }
    } else {
      this.filterObj.teamsId = null;
      this.filterObj.includeOpenOrders = this.includeOpenOrders;
    }
  }
}
