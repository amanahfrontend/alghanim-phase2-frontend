/**
 *
 * Dispatcher Main Board Component
 *
 * Drag and drop applied via using angular material here urls
 *
 * Angular material https://material.angular.io/cdk/drag-drop/overview
 *
 * ------------------------------------------------------------------------
 * Events used
 * ===========
 *
 * (cdkDropListDropped) called when item dropped at same list or other list /
 * (cdkDragMoved) called when dragging item
 * --------------------------------------------------------------------------
 * --------------------------------------------------------------------------
 * Directives used
 * ===============
 *
 * cdkDropListGroup to target a group of lists such as assigned orders & unassigned orders /
 * cdkDropList called when target array of items such as orders /
 * cdkDrag to target which div (item) to drag
 * --------------------------------------------------------------------------
 * Inputs Used
 * ===========
 *
 *  [cdkDropListData] to pass list data /
 *  [cdkDragData] to pass item data /
 *  [cdkDragDisabled] to disable item from dragging depending on condition
 *
 */

import { Component, OnInit, Inject, OnDestroy, ViewChild, ElementRef, ViewChildren, ViewContainerRef, QueryList } from '@angular/core';
import { LookupService } from '../../api-module/services/lookup-services/lookup.service';
import { UtilitiesService } from '../../api-module/services/utilities/utilities.service';
import { MessageService } from 'primeng/api';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem
} from '@angular/cdk/drag-drop';
import { DOCUMENT } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';
import { interval, Subscription } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/refresh-rate.reducer';
import { RefreshRateService } from 'src/app/services/refresh-rate.service';

@Component({
  selector: 'app-supervisor-board',
  templateUrl: './supervisor-board.component.html',
  styleUrls: ['./supervisor-board.component.scss'],
})

export class SupervisorBoardComponent implements OnInit, OnDestroy {
  @ViewChild('unassignedContainer', { static: false }) unassignedContainer: ElementRef;
  @ViewChildren('assignedContainer', { read: ViewContainerRef }) assignedContainer: QueryList<ViewContainerRef>;
  isLoading: boolean;
  showFilter: boolean;
  isFilterActive: boolean = false;
  fetchingDataDone: boolean;
  isBulkAssign: boolean;
  cornerMessage: any[];
  toggleOverflowX: string[];
  allUnassignedOrders = [];
  disabledStatus: string[];
  orders: any[];
  dispatchers: any = [];
  numberOfServicesCalled: number = 0;
  numberOfServicesFinished: number = 0;
  pageNumber: number = 1;
  orderCodeSupervisor: string = "";
  prevOrderCodeSupervisor: string = "";
  allUnassignedOrdersForSupervisor: any = [];
  terms: any = {};
  containersToBeRefreshed: any = [];
  subs = new Subscription();
  rightMoves: number = 0;
  leftMoves: number = 0;
  CurrentUserHasDispatcherRole: boolean;
  CurrentUserHasSupervisorRole: boolean;
  constructor(
    private lookup: LookupService,
    private utilities: UtilitiesService,
    private messageService: MessageService,
    @Inject(DOCUMENT) private document: Document,
    private translateService: TranslateService,
    private refreshRateService: RefreshRateService,
    private store: Store<AppState>,
  ) { }

  ngOnInit() {
    let currentUserRoles: string[] = JSON.parse(localStorage.getItem('AlghanimCurrentUser')).data.roles;
    this.CurrentUserHasDispatcherRole = currentUserRoles.includes('Dispatcher');
    this.CurrentUserHasSupervisorRole = currentUserRoles.includes('Supervisor');
    this.resetBoardFilter();
    this.subscribeToRefreshRate();
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  subscribeToRefreshRate() {
    this.subs.add(this.store.select("refreshRateData")
      .pipe(
        switchMap((refreshRateState) => {
          return interval(refreshRateState.refreshRate * 60000);
        })).
      subscribe(() => {
        this.refreshRateService.setSettings();
        this.pageNumber = 1;
        this.terms.pageInfo = {
          pageNo: this.pageNumber,
          pageSize: 10
        }
        this.applyFilterBoardTemplateWithoutLoading();
      }));
  }

  seriveCalled() {
    this.isLoading = true;
    this.numberOfServicesCalled++;
  }

  checkIfServicesFinished() {
    this.numberOfServicesFinished++;
    if (this.numberOfServicesFinished == this.numberOfServicesCalled) {
      this.isLoading = false;
    }
  }


  applyFilterBoardTemplateWithoutLoading() {
    let supervisorId = +localStorage.getItem('AlghanimCurrentUserId');
    let emptyObj = {
      supervisorId: supervisorId,
      pageInfo: {
        pageNo: this.pageNumber,
        pageSize: 10,
      }
    };
    if (this.terms.fromWhere == 3) {
      this.lookup.postAllAssignedOrdersForSupervisorFilter(this.terms).subscribe((res: any) => {
        if (res.isSucceeded) {
          this.setUnassignedOrdersToDispatchers(res);
        } else {
          this.alertServiceError(res);
        }
      }, (err: any) => this.alertServiceError(err));
      this.lookup.postAllUnAssignedOrdersForSupervisorFilter(this.terms).subscribe((res: any) => {
        if (res.isSucceeded) {
          this.setUnassignedOrders(res);
        } else {
          this.alertServiceError(res);
        }
      }, (err: any) => this.alertServiceError(err));
    } else if (this.terms.fromWhere == 2) {
      this.lookup.postAllAssignedOrdersForSupervisorFilter(this.terms).subscribe((res: any) => {
        if (res.isSucceeded) {
          this.setUnassignedOrdersToDispatchers(res);
        } else {
          this.alertServiceError(res);
        }
      }, (err: any) => this.alertServiceError(err));
      this.lookup.postAllUnAssignedOrdersForSupervisorFilter(emptyObj).subscribe((res: any) => {
        if (res.isSucceeded) {
          this.setUnassignedOrders(res);
        } else {
          this.alertServiceError(res);
        }
      }, (err: any) => this.alertServiceError(err));
    } else if (this.terms.fromWhere == 1) {
      this.lookup.postAllAssignedOrdersForSupervisorFilter(emptyObj).subscribe((res: any) => {
        if (res.isSucceeded) {
          this.setUnassignedOrdersToDispatchers(res);
        } else {
          this.alertServiceError(res);
        }
      }, (err: any) => this.alertServiceError(err));

      this.lookup.postAllUnAssignedOrdersForSupervisorFilter(this.terms).subscribe((res: any) => {
        if (res.isSucceeded) {
          this.setUnassignedOrders(res);
        } else {
          this.alertServiceError(res);
        }
      }, (err: any) => this.alertServiceError(err))
    }
  }


  getSupervisorUnassignedOrders() {
    this.seriveCalled();
    let supervisorId = localStorage.getItem('AlghanimCurrentUserId');
    let objToPost: any = {
      supervisrId: supervisorId,
      pageInfo: {
        pageNo: this.pageNumber,
        pageSize: 10,
      }
    };
    this.lookup.postAllUnAssignedOrdersForSupervisor(objToPost)
      .subscribe((supervisor: any) => {
        this.checkIfServicesFinished();
        this.orders = supervisor.data.data;
        this.allUnassignedOrdersForSupervisor = this.orders;
      }, (err: any) => this.onServerError(err));
  }

  onDispatcherScroll(id, index) {
    this.dispatchers[index].pageNumber++;
    let objToPost: any = Object.assign({}, this.terms);
    delete objToPost.supervisorId;
    objToPost.dispatcherId = id;
    objToPost.pageInfo.pageNo = this.dispatchers[index].pageNumber;
    this.lookup.postAllUnAssignedOrdersForDispatcherFilter(objToPost).subscribe((response: any) => {
      if (response.isSucceeded) {
        if (response.data) {
          let orders = response.data.data;
          this.dispatchers[index].orders.push(...orders);
          this.dispatchers[index].filterOrders = this.dispatchers[index].orders.slice();
        }
      } else {
        this.alertServiceError(response);
      }
    }, (err: any) => this.alertServiceError(err));
  }

  onChangeCodeForSupervisor() {
    if (this.prevOrderCodeSupervisor != this.orderCodeSupervisor && this.orderCodeSupervisor.length > 9) {
      this.seriveCalled();
      let supervisorId = localStorage.getItem('AlghanimCurrentUserId');
      let objToPost = {
        supervisrId: supervisorId,
        orderCode: this.orderCodeSupervisor,
        pageInfo: {
          pageNo: 1,
          pageSize: 10,
        }
      };
      this.prevOrderCodeSupervisor = this.orderCodeSupervisor;
      this.lookup.postAllUnAssignedOrdersForSupervisor(objToPost).subscribe((supervisor: any) => {
        if (supervisor.isSucceeded) {
          this.checkIfServicesFinished();
          this.orders = supervisor.data.data;
        } else {
          this.onServerError(supervisor)
        }
      }, (err: any) => this.onServerError(err));
    }
  }

  onResetSearchForSupervisor() {
    this.orderCodeSupervisor = "";
    this.prevOrderCodeSupervisor = "";
    this.orders = this.allUnassignedOrdersForSupervisor;
  }

  onCodeChangeForDispatcher(id, index) {
    if (this.dispatchers[index].orderCode != this.dispatchers[index].prevOrderCode && this.dispatchers[index].orderCode.length > 9) {
      let objToPost = {
        dispatcherId: id,
        orderCode: this.dispatchers[index].orderCode,
        pageInfo: {
          pageNo: 1,
          pageSize: 10
        }
      };
      this.seriveCalled();
      this.dispatchers[index].prevOrderCode = this.dispatchers[index].orderCode;
      this.lookup.postAllUnAssignedOrdersForDispatcher(objToPost).subscribe((response: any) => {
        if (response.isSucceeded) {
          if (response.data) {
            let orders = response.data.data;
            this.dispatchers[index].filterOrders = orders;
          }
          this.checkIfServicesFinished();

        } else {
          this.onServerError(response);
        }
      }, (err: any) => this.onServerError(err));
    }
  }

  onResetSearchForDispatcher(index) {
    this.dispatchers[index].prevOrderCode = "";
    this.dispatchers[index].orderCode = "";
    this.dispatchers[index].filterOrders = this.dispatchers[index].orders.slice()
  }

  showHideFilter() {
    this.showFilter = !this.showFilter;
  }

  setUnassignedOrders(res) {
    if (res.isSucceeded) {
      this.orders = res.data.data;
      this.allUnassignedOrdersForSupervisor = this.orders;
      setTimeout(() => {
        this.unassignedContainer.nativeElement.scroll(0, 0);
      }, 0);
    } else {
      this.onServerError(res);
    }
  }

  setUnassignedOrdersToDispatchers(res: any) {
    if (res.isSucceeded) {
      this.dispatchers = res.data;
      this.dispatchers.map(dispatcher => {
        dispatcher.pageNumber = 1;
        if (dispatcher.orders) {
          dispatcher.filterOrders = dispatcher.orders.slice();
        } else {
          dispatcher.filterOrders = [];
          dispatcher.orders = [];
        }
      });
    } else {
      this.onServerError(res);
    }

  }

  applyFilterBoardTemplateAndRestPage(terms: any) {
    this.pageNumber = 1;
    this.applyFilterBoardTemplate(terms);
  }

  applyFilterBoardTemplate(terms: any) {
    let supervisorId = +localStorage.getItem('AlghanimCurrentUserId');
    let obj = terms;
    obj.supervisorId = supervisorId;
    obj.pageInfo = {
      pageNo: this.pageNumber,
      pageSize: 10,
    };

    let emptyObj = {
      supervisorId: supervisorId,
      pageInfo: {
        pageNo: this.pageNumber,
        pageSize: 10,
      }
    };

    this.terms = obj;
    this.seriveCalled();
    this.seriveCalled();
    if (terms.fromWhere == 3) {
      this.lookup.postAllAssignedOrdersForSupervisorFilter(obj).subscribe((res: any) => {
        if (res.isSucceeded) {
          this.checkIfServicesFinished();
          this.setUnassignedOrdersToDispatchers(res);
        } else {
          this.onServerError(res);
        }
      }, (err: any) => this.onServerError(err));
      this.lookup.postAllUnAssignedOrdersForSupervisorFilter(obj).subscribe((res: any) => {
        if (res.isSucceeded) {
          this.checkIfServicesFinished();
          this.setUnassignedOrders(res);
        } else {
          this.onServerError(res);
        }
      }, (err: any) => this.onServerError(err));
    } else if (terms.fromWhere == 2) {
      this.lookup.postAllAssignedOrdersForSupervisorFilter(terms).subscribe((res: any) => {
        if (res.isSucceeded) {
          this.checkIfServicesFinished();
          this.setUnassignedOrdersToDispatchers(res);
        } else {
          this.onServerError(res);
        }
      }, (err: any) => this.onServerError(err));
      this.lookup.postAllUnAssignedOrdersForSupervisorFilter(obj).subscribe((res: any) => {
        if (res.isSucceeded) {
          this.checkIfServicesFinished();
          this.setUnassignedOrders(res);
        } else {
          this.onServerError(res);
        }
      }, (err: any) => this.onServerError(err));

    } else if (terms.fromWhere == 1) {

      this.lookup.postAllAssignedOrdersForSupervisorFilter(emptyObj).subscribe((res: any) => {
        if (res.isSucceeded) {
          this.checkIfServicesFinished();
          this.setUnassignedOrdersToDispatchers(res);
        } else {
          this.onServerError(res);
        }
      }, (err: any) => this.onServerError(err));

      this.lookup.postAllUnAssignedOrdersForSupervisorFilter(obj).subscribe((res: any) => {
        if (res.isSucceeded) {
          this.checkIfServicesFinished();
          this.setUnassignedOrders(res);
        } else {
          this.onServerError(res);
        }
      }, (err: any) => this.onServerError(err))
    }
  }

  resetBoardFilter() {
    this.pageNumber = 1;
    let supervisorId = +localStorage.getItem('AlghanimCurrentUserId');
    this.terms = {
      supervisorId: supervisorId,
      pageInfo: {
        pageNo: this.pageNumber,
        pageSize: 10,
      },
      fromWhere: 3
    };
    this.applyFilterBoardTemplate(this.terms);
  }

  onSupervisorScroll() {
    this.pageNumber++;
    let objToPost = this.terms;
    objToPost.pageInfo.pageNo = this.pageNumber;
    this.lookup.postAllUnAssignedOrdersForSupervisorFilter(objToPost)
      .subscribe((supervisor: any) => {
        if (supervisor.isSucceeded) {
          let array = supervisor.data.data;
          this.orders.push(...array);
          this.allUnassignedOrdersForSupervisor = this.orders;
        } else {
          this.alertServiceError(supervisor)
        }
      }, (err: any) => this.alertServiceError(err));
  }

  /**
   * assign order to dispatcher
   * 
   * 
   * @param body 
   */
  assignOrder(body: object) {
    this.seriveCalled();
    this.lookup.assginOrderToDispatcher(body).subscribe((res: any) => {
      if (res.isSucceeded) {
        this.onDragSucceeded();
      } else {
        this.onServerError(res);
      }
    }, (err: any) => this.onServerError(err));
  }

  /**
   * unassign order from dispatcher
   * 
   * 
   * @param body 
   */
  unAssignOrder(body: object) {
    this.seriveCalled();
    this.lookup.unAssginOrderFromDispatcher(body).subscribe((res: any) => {
      if (res.isSucceeded) {
        this.onDragSucceeded();
      } else {
        this.onServerError(res)
      }
    }, (err: any) => this.onServerError(err));
  }

  onDragSucceeded() {
    this.pageNumber = 1;
    this.terms.pageInfo.pageNo = this.pageNumber;
    this.seriveCalled();
    this.containersToBeRefreshed.map((dispatcherId) => {
      if (dispatcherId) {
        let dispatcherObjToPost = Object.assign({}, this.terms);
        dispatcherObjToPost.dispatcherId = dispatcherId;
        delete dispatcherObjToPost.supervisorId;
        this.lookup.postAllUnAssignedOrdersForDispatcher(dispatcherObjToPost)
          .subscribe(res => {
            if (res.isSucceeded) {
              let dispatcherOrders = res.data.data;
              this.dispatchers.map((d) => {
                if (dispatcherId == d.dispatcherId) {
                  let selectedContainer = this.assignedContainer.filter((container: any) => container.element.nativeElement.id == dispatcherId)[0].element.nativeElement;
                  selectedContainer.scroll(0, 0);
                  d.orders = d.filterOrders = [...dispatcherOrders];
                  d.pageNumber = 1;
                }
              })
              this.checkIfServicesFinished();
            } else {
              this.onServerError(res)
            }
          }, (err: any) => this.onServerError(err));
      } else {
        this.lookup.postAllUnAssignedOrdersForSupervisorFilter(this.terms).subscribe((res: any) => {
          if (res.isSucceeded) {
            this.checkIfServicesFinished();
            this.setUnassignedOrders(res);
          } else {
            this.onServerError(res);
          }
        }, (err: any) => this.onServerError(err));
      }
    })
  }


  /**
  * Assign order from
  * 
  * 
  * @param event 
  * @param teamId 
  */
  onDropOrder(event: CdkDragDrop<string[]>, type: string, dispatcherId?: number) {
    this.leftMoves = 0;
    this.rightMoves = 0;
    this.containersToBeRefreshed = []

    const item = event.item.data;
    const sameContainer = event.previousContainer === event.container ? true : false;

    if (sameContainer) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }

    // no action to be taken if the order dragged to the same container
    if (!sameContainer) {
      // assign case
      if (type === 'assign') {
        const body = {
          dispatcherId: dispatcherId,
          orderId: item.id,
        };
        this.containersToBeRefreshed.push(item.dispatcherId, dispatcherId)
        this.assignOrder(body);

        // unassign case
      } else if (type === 'unassign') {
        const body = {
          orderId: item.id,
          dispatcherId: null,
        };
        this.containersToBeRefreshed.push(item.dispatcherId, null)
        this.unAssignOrder(body);
      }
    }
  }

  /**
   * calculate left moves and right moves to start Horizontal scroll moving 
   * 
   * 
   * @param event 
   */
  onDragMoved(event) {
    let xPosition = event.event.pageX;
    let totalWidth = this.document.body.offsetWidth;
    let poistionRatio = xPosition / totalWidth;
    console.log(poistionRatio);
    if (poistionRatio > 0.93 && poistionRatio < 0.971) {
      this.document.getElementById('technican-overflow-container').scrollBy({
        left: 15
      });;
    }
    if (poistionRatio < 0.246 && poistionRatio > 0.186) {
      this.document.getElementById('technican-overflow-container').scrollBy({
        left: -15
      });;
    }
    //   if (event.delta.x === 1) {
    //     this.rightMoves++;
    //     if (this.rightMoves > 25) {
    //       this.document.getElementById('technican-overflow-container').scrollLeft += 10;
    //     }
    //     if (this.rightMoves > 50) {
    //       this.document.getElementById('technican-overflow-container').scrollLeft += 10;
    //     }
    //   } else {
    //     this.leftMoves++;
    //     if (this.leftMoves > 25) {
    //       this.document.getElementById('technican-overflow-container').scrollLeft -= 10;
    //     }
    //     if (this.leftMoves > 50) {
    //       this.document.getElementById('technican-overflow-container').scrollLeft -= 10;
    //     }
    //   }
  }
  // onDragMoved(event) {
  //   if (event.delta.x === 1) {
  //     this.rightMoves++;
  //     if (this.rightMoves > 25) {
  //       this.document.getElementById('technican-overflow-container').scrollLeft += 10;
  //     }
  //   } else {
  //     this.leftMoves++;
  //     if (this.leftMoves > 25) {
  //       this.document.getElementById('technican-overflow-container').scrollLeft -= 10;
  //     }
  //   }
  // }

  /**
   * alert service error
   * 
   *  
   * @param error 
   */
  alertServiceError(error: string) {
    this.messageService.add({
      severity: 'error',
      summary: this.translateService.instant('Failed'),
      detail: this.translateService.instant(`Failed due to server error.`)
    })
  }

  onServerError(error) {
    this.alertServiceError(error);
    this.checkIfServicesFinished();
  }

}
