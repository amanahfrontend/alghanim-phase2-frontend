import { element } from "protractor";
import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef,
} from "@angular/core";
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import { NotificationsService } from "../notifications.service";
import { Subscription, interval, Subject, Observable } from "rxjs";
import { SharedNotificationService } from "../../shared-module/shared/shared-notification.service";
import { MessageService } from "primeng/api";
import { RefreshRateService } from "src/app/services/refresh-rate.service";
import { Store } from "@ngrx/store";
import { AppState } from "src/app/store/refresh-rate.reducer";
import { switchMap } from "rxjs/operators";
import { OrderCardViewModel } from "src/app/models/orderCard.model";

@Component({
  selector: "app-supervisor-board-for-dispatcher",
  templateUrl: "./supervisor-board-for-dispatcher.component.html",
  styleUrls: ["./supervisor-board-for-dispatcher.component.scss"],
})
export class SupervisorBoardForDispatcherComponent
  implements OnInit, OnDestroy {
  toggleLoading: boolean;
  fullScreenMap: any;
  showFilter: boolean;
  isFilterActive: boolean = false;
  fetchingDataDone: boolean;
  isBulkAssign: boolean;
  cornerMessage: any[];
  toggleOverflowX: string[];
  allUnassignedOrders = [];
  disabledStatus: string[];
  orders: OrderCardViewModel[] = [];
  foremen: any = [];
  dispatcherList: any = [];
  selectedDispatcher: any = {};
  terms: any = {
    fromWhere: 3,
  };
  pageNumber: number = 1;
  CurrentUserHasDispatcherRole: boolean;
  CurrentUserHasSupervisorRole: boolean;
  subs = new Subscription();
  filterSubject = new Subject();
  constructor(
    private lookup: LookupService,
    private utilities: UtilitiesService,
    private notificationsService: NotificationsService,
    public sharedNotificationService: SharedNotificationService,
    private messageService: MessageService,
    private refreshRateService: RefreshRateService,
    private store: Store<AppState>
  ) { }

  ngOnInit() {
    this.getDispatcherBySuper();
    this.filterSubscribtion();
    let currentUserRoles: string[] = JSON.parse(
      localStorage.getItem("AlghanimCurrentUser")
    ).data.roles;
    this.CurrentUserHasDispatcherRole = currentUserRoles.includes("Dispatcher");
    this.CurrentUserHasSupervisorRole = currentUserRoles.includes("Supervisor");
    this.subscribeToRefreshRate();
  }

  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  filterSubscribtion() {
    this.subs.add(
      this.filterSubject.pipe(switchMap(() => this.applyFilterBoardTemplate(this.terms)))
        .subscribe((res: any) => {
          if (res.isSucceeded) {
            this.setAssignedOrders(res.data.assignedOrders);
            this.setUnassignedOrders(res.data.unAssignedOrders.data);
          } else {
            this.onServerError(res);
          }
        }, (err: any) => this.onServerError(err)));
  }

  onClickFilter(e) {
    this.toggleLoading = true;
    this.fetchingDataDone = false;
    this.terms = e;
    this.showHideFilter();
    this.filterSubject.next()
  }

  subscribeToRefreshRate() {
    this.subs.add(
      this.store
        .select("refreshRateData")
        .pipe(
          switchMap((refreshRateState) => {
            return interval(refreshRateState.refreshRate * 60000);
          })
        )
        .subscribe(() => {
          if (this.selectedDispatcher.id) {
            this.refreshRateService.setSettings();
            this.pageNumber = 1;
            this.terms = {
              pageInfo: {
                pageNo: this.pageNumber,
                pageSize: 10,
              },
              fromWhere: 3,
            };
            this.filterSubject.next();
          }
        })
    );

  }

  onSelectDispatcher(e: any) {
    this.fetchingDataDone = false;
    this.toggleLoading = true;
    this.selectedDispatcher = {};
    this.selectedDispatcher.id = e.value;
    this.orders = [];
    this.foremen = [];
    this.resetBoardFilter();
    this.notificationsService.getPermission();
    this._receiveNotification();
    this.onRecieveChangeRank();
  }

  getDispatcherBySuper() {
    this.fetchingDataDone = true;
    this.toggleLoading = true;
    let superId = localStorage.getItem("AlghanimCurrentUserId");
    this.lookup.getDispatcherBySuper(superId).subscribe(
      (res: any) => {
        this.dispatcherList = res.data;
        this.dispatcherList.map((dis: any) => {
          dis.label = dis.fullName;
          dis.value = dis.id;
        });
        this.toggleLoading = false;
      },
      (err) => this.onServerError(err)
    );
  }

  onRecieveChangeRank() {
    this.sharedNotificationService.acceptChangeRankSubject.subscribe(() =>
      this.getTeamsAndOrders()
    );
  }

  CheckToSetUnassignedOrders(res) {
    if (res.isSucceeded) {
      this.setUnassignedOrders(res.data.data);
    } else {
      this.onServerError(res);
    }
  }

  setUnassignedOrders(orders) {
    this.orders = orders || [];
    this.toggleLoading = false;
  }

  CheckToSetAssignedOrders(res) {
    if (res.isSucceeded) {
      this.setAssignedOrders(res.data);
    } else {
      this.onServerError(res);
    }
  }

  setAssignedOrders(foremen) {
    this.foremen = foremen || [];
    this.foremen.map((forman) => {
      forman.teams.map((team: any) => {
        if (team.orders) {
          team.filterOrders = team.orders.slice();
        } else {
          team.filterOrders = [];
          team.orders = [];
        }
      });
    });
    this.fetchingDataDone = true;
  }

  getDispatcherUnassignedOrders() {
    this.toggleLoading = true;
    let objToPost = {
      dispatcherId: this.selectedDispatcher.id,
      pageInfo: {
        pageNo: 1,
        pageSize: 10
      }
    };
    this.lookup.postAllUnAssignedOrdersForDispatcherFilter(objToPost)
      .subscribe(
        (res: any) => this.CheckToSetUnassignedOrders(res),
        (err: any) => this.onServerError(err));
  }

  onScroll() {
    this.pageNumber++;
    let objToPost = {
      dispatcherId: this.selectedDispatcher.id,
      pageInfo: {
        pageNo: this.pageNumber,
        pageSize: 10,
      },
    };
    this.lookup.postAllUnAssignedOrdersForDispatcher(objToPost).subscribe(
      (res: any) => {
        if (res.isSucceeded) {
          let arr2 = res.data.data;
          this.orders.push(...arr2);
        } else {
          this.onServerError(res);
        }
      },
      (err) => this.onServerError(err)
    );
  }

  getTeamsAndOrders() {
    this.fetchingDataDone = false;
    this.lookup
      .getAllAssignedOrdersForDispatcher(this.selectedDispatcher.id)
      .subscribe(
        (res: any) => this.CheckToSetAssignedOrders(res),
        (err: any) => this.onServerError(err)
      );
  }

  showHideFilter() {
    this.showFilter = !this.showFilter;
  }

  applyFilterBoardTemplate(terms: any) {
    terms.dispatcherId = this.selectedDispatcher.id;
    this.terms = Object.assign({}, terms);
    return this.lookup.postAllOrdersBoardOrderFilter(this.terms);
    // if (terms.fromWhere == 3) {
    //   // terms.fromWhere = 1;
    //   this.lookup.postBoardOrderFilter(terms).subscribe(
    //     (res: any) => this.CheckToSetAssignedOrders(res),
    //     (err: any) => this.onServerError(err)
    //   );
    //   // terms.fromWhere = 2;
    //   this.lookup.postAllUnAssignedOrdersForDispatcherFilter(terms).subscribe(
    //     (res: any) => this.CheckToSetUnassignedOrders(res),
    //     (err: any) => this.onServerError(err)
    //   );
    // } else if (terms.fromWhere == 2) {
    //   // emptyFilterObj.fromWhere = 1;
    //   this.lookup.postBoardOrderFilter(terms).subscribe(
    //     (res: any) => this.CheckToSetAssignedOrders(res),
    //     (err: any) => this.onServerError(err)
    //   );
    //   // terms.fromWhere = 2;
    //   this.lookup
    //     .postAllUnAssignedOrdersForDispatcherFilter(emptyFilterObj)
    //     .subscribe(
    //       (res: any) => this.CheckToSetUnassignedOrders(res),
    //       (err: any) => this.onServerError(err)
    //     );
    // } else if (terms.fromWhere == 1) {
    //   // terms.fromWhere = 1;
    //   this.lookup.postBoardOrderFilter(emptyFilterObj).subscribe(
    //     (res: any) => this.CheckToSetAssignedOrders(res),
    //     (err: any) => this.onServerError(err)
    //   );
    //   // emptyFilterObj.fromWhere = 2;
    //   this.lookup.postAllUnAssignedOrdersForDispatcherFilter(terms).subscribe(
    //     (res: any) => this.CheckToSetUnassignedOrders(res),
    //     (err: any) => this.onServerError(err)
    //   );
    // }
    // this.showHideFilter();
  }

  resetBoardFilter() {
    this.pageNumber = 1;
    this.terms = {
      pageInfo: {
        pageNo: this.pageNumber,
        pageSize: 10,
      },
      fromWhere: 3,
    };
    this.filterSubject.next();
  }

  private _receiveNotification() {
    this.notificationsService.receiveMessage().subscribe((msg: any) => {
      const order = JSON.parse(msg.data.order);
      this._searchAndUpdateOrAddOrder(order);
    });
  }

  private _searchAndUpdateOrAddOrder(order) {
    this.foremen.map((foreman: any) => {
      foreman.teams.forEach((team: any) => {
        team.filterOrders.forEach((singleOrder, i) => {
          if (singleOrder.id == order.id) {
            if (
              order.statusName == "Compelete" ||
              order.statusName == "Cancelled"
            ) {
              team.filterOrders.splice(i, 1);
            } else if (order.statusName == "On-Hold") {
              team.filterOrders.splice(i, 1);
            } else if (order.acceptanceFlag == 3) {
              team.filterOrders.splice(i, 1);
            } else {
              team.filterOrders[i] = order;
            }
          }
        });
      });
    });
    if (order.statusName == "On-Hold" || order.statusName == "Open") {
      this.orders.push(order);
    }
  }

  onServerError(err, msg = "Failed due to network error!") {
    console.error(err);
    this.messageService.add({
      severity: "error",
      summary: "Failed!",
      detail: msg,
    });
    this.toggleLoading = false;
    this.fetchingDataDone = true;
  }
}
