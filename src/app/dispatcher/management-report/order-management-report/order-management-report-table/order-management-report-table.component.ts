import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from "@angular/core";
import { DatatableComponent } from "@swimlane/ngx-datatable";
import { OrderManagementReportService } from "../order-management-report.service";
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-order-management-report-table',
  templateUrl: './order-management-report-table.component.html',
  styleUrls: ['./order-management-report-table.component.css']
})

export class OrderManagementReportTableComponent implements OnInit {
  @Output() pageClicked = new EventEmitter<any>();
  @Input() orders: any[];
  @Input() count: number;
  @Input() filterObj: any;
  @Input() isLoading: boolean;
  @Input() $filterView: BehaviorSubject<boolean>
  // pageSizeContainer: any;





  pageLimitOptions = [
    {
      label: '10',
      value: 10
    },
    {
      label: '20',
      value: 20
    },
    {
      label: '50',
      value: 50
    }
  ];

  pageSize: number = 10;

  page: any = {
    pageNo: 0,
    pageSize: this.orderManagementReportService.pageSize
  };

  constructor(
    private orderManagementReportService: OrderManagementReportService
  ) { }

  ngOnInit() {
    this.orderManagementReportService.resetPagination.subscribe(() => {
      this.page.pageNo = 0;
    });
  }


  setPage(e) {
    if (this.isLoading) {
      this.orderManagementReportService.resetPagination.unsubscribe();
    }
    this.page.pageNo = e.offset;
    this.filterObj.pageInfo = {
      pageNo: e.offset + 1,
      pageSize: this.orderManagementReportService.pageSize
    };
    this.pageClicked.emit(this.filterObj);
    this.$filterView.next(false);
  }

  onChangePageSize() {
    this.orderManagementReportService.pageSize = this.pageSize;
    let e: any = {};
    e.offset = 0;
    this.setPage(e);
  }
}