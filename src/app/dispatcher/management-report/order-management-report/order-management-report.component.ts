import { Component, OnInit, OnDestroy } from "@angular/core";
import { LookupService } from "../../../api-module/services/lookup-services/lookup.service";
import { MessageService } from 'primeng/components/common/messageservice';
import { BaseUrlOrderManagementNew } from "../../../api-module/services/globalPath";
import { TranslateService } from '@ngx-translate/core';
import { BehaviorSubject } from 'rxjs';
import { OrderManagementReportService } from './order-management-report.service';

@Component({
  selector: 'app-order-management-report',
  templateUrl: './order-management-report.component.html',
  styleUrls: ['./order-management-report.component.css']
})


export class OrderManagementReportComponent implements OnInit, OnDestroy {

  filterObj: any;
  orders: any[] = [];
  total: number = null;
  skip: number = null;
  toggleLoading: boolean = false;
  count: number;
  pageChangedFromFilter: boolean;
  isExporting: boolean = false;
  isExported: boolean = false;
  $filterView: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);




  constructor(
    private lookup: LookupService,
    private messageService: MessageService,
    private translateService: TranslateService,
    private orderManagementReportService: OrderManagementReportService,
  ) { }

  ngOnInit() {
    this.$filterView.next(true);
  }
  ngOnDestroy(): void {
    this.$filterView.unsubscribe();
  }

  onServerExportError(err) {
    console.error(err)
    this.isExporting = false;
    this.filterObj = null;
    this.messageService.add({
      severity: 'error',
      summary: this.translateService.instant('Failed!'),
      detail: this.translateService.instant("Failed to export !")
    });
  }

  applyFilter(event) {
    this.isExported = false;
    this.toggleLoading = true;
    this.filterObj = event;
    this.filterObj.pageInfo = {
      pageNo: (this.filterObj.pageInfo && this.filterObj.pageInfo.pageNo) || 1,
      pageSize: this.orderManagementReportService.pageSize
    };

    this.lookup.getOrdersManagementReport(this.filterObj)
      .subscribe(
        res => {
          if (res.isSucceeded) {
            this.orders = res.data.result;
            this.count = res.data.count;
            this.$filterView.next(this.count == 0);

          } else {
            this.$filterView.next(false);
            this.messageService.add({
              severity: 'error',
              summary: this.translateService.instant('Failed!'),
              detail: this.translateService.instant('Failed Due To Server Error!')
            });
          }
          this.toggleLoading = false;
        }, err => {
          this.$filterView.next(false);
          this.toggleLoading = false;
          console.log(err);
          this.messageService.add({
            severity: 'error',
            summary: this.translateService.instant('Failed!'),
            detail: this.translateService.instant('Failed Due To Server Error!')
          });
        }
      );
  }

  onReset() {
    this.orders = [];
    this.filterObj = null;
    this.toggleLoading = false;
  }


  export(event) {
    this.isExported = true;
    this.isExporting = true;
    this.filterObj = event;
    delete this.filterObj.pageInfo;
    this.lookup.exportOrdersManagementReport(this.filterObj)
      .subscribe(
        (res: any) => {
          this.isExporting = false;
          if (res.isSucceeded) {
            let link = document.createElement("a");
            link.download = "Order Management Report";
            const baseurl = BaseUrlOrderManagementNew.substring(0, BaseUrlOrderManagementNew.length - 4);
            const excelURL = res.data.substring(9);
            // link.href = baseurl + excelURL;
            // link.target = "_blank";
            // document.body.appendChild(link);
            // link.click();
            // document.body.removeChild(link);
            window.open(baseurl + excelURL, "_blank");

          } else {
            this.onServerExportError(res);
          }
        }, err => this.onServerExportError(err));
  }
}