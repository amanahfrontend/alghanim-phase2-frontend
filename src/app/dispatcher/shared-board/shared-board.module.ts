import { NgModule } from '@angular/core';
import { SharedModuleModule } from '../../shared-module/shared-module.module';
import { OrderProgressModalComponent } from './order-progress-modal/order-progress-modal.component';
import { EditOrderComponent } from './edit-order/edit-order.component';
import { OrderCardComponent } from './order-card/order-card.component';
import { LAZY_MAPS_API_CONFIG, AgmCoreModule } from '@agm/core';
import { GoogleMapsConfig } from '../agm-module';
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [
    OrderProgressModalComponent,
    EditOrderComponent,
    OrderCardComponent,
  ],
  imports: [
    SharedModuleModule,
    AgmCoreModule.forRoot(),
    AgmJsMarkerClustererModule,
    DragDropModule,
    TranslateModule
  ],
  entryComponents: [
    OrderProgressModalComponent
  ],
  exports: [
    SharedModuleModule,
    OrderProgressModalComponent,
    EditOrderComponent,
    OrderCardComponent,
    DragDropModule,
  ], providers: [
    { provide: LAZY_MAPS_API_CONFIG, useClass: GoogleMapsConfig },
  ]
})
export class SharedBoardModule { }
