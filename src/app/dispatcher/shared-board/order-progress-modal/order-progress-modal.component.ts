import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LookupService } from '../../../api-module/services/lookup-services/lookup.service';
import { UtilitiesService } from '../../../api-module/services/utilities/utilities.service';
import { AlertServiceService } from '../../../api-module/services/alertservice/alert-service.service';

@Component({
  selector: 'app-order-progress-modal',
  templateUrl: './order-progress-modal.component.html',
  styleUrls: ['./order-progress-modal.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class OrderProgressModalComponent implements OnInit {
  @Input() order: any;
  progress: any[];
  toggleLoading: boolean;

  constructor(public activeModal: NgbActiveModal,
    private lookupService: LookupService,
    private utilitiesService: UtilitiesService,
    private alertService: AlertServiceService) {
  }

  ngOnInit() {
    this.getOrderProgress();
  }

  getOrderProgress() {
    this.toggleLoading = true;
    this.lookupService.getOrderProgress(this.order.id)
      .subscribe(
        (progress) => {
          this.progress = progress.data || [];
          this.toggleLoading = false;
        },
        err => {
          console.log(err);
        }
      );
  }

  close() {
    this.activeModal.dismiss();
  }
}
