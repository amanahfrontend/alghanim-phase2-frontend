import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { OrderProgressModalComponent } from '../order-progress-modal/order-progress-modal.component';

@Component({
  selector: 'app-order-card',
  templateUrl: './order-card.component.html',
  styleUrls: ['./order-card.component.css']
})

export class OrderCardComponent {
  @Input() order: any = {};
  @Input() cssClass: string = "";
  @Input() orderIconIsView: boolean = false;

  @Output() orderSelected = new EventEmitter;
  @Output() orderTransfered = new EventEmitter;
  currentDetailedOrderId: any;
  openOrderProgress: any;

  constructor(private modalService: NgbModal) { }

  selectOrder() {
    this.orderSelected.emit(this.order);
  }

  //coloring logic
  chooseColor(orderType, orderPriority, orderDate) {
    let style = {
      backgroundColor: '',
      color: null
    };
    // let's get the date number
    let date = new Date(orderDate).getDay();
    if (orderPriority && orderPriority.toLowerCase() === 'Very High'.toLowerCase()) {
      style.backgroundColor = 'red';
      style.color = 'white';
    } else if (orderPriority && orderPriority.toLowerCase() === 'High'.toLowerCase()) {
      style.backgroundColor = 'gold';
      style.color = 'black';
    } else { // condition : orderPriority === 'Medium'
      if (orderType === 'ZS02' ||
        orderType === 'ZS05') {
        style.backgroundColor = 'darkgreen';
        style.color = 'white';
      } else if (orderType === 'ZS99') {
        style.backgroundColor = 'orange';
        style.color = 'black';
      } else if (orderType === 'ZS07') {
        style.backgroundColor = 'white';
        style.color = 'black';
      } else if (
        orderType === 'ZS01' ||
        orderType === 'ZS03' ||
        orderType === 'ZS06'
      ) {
        switch (date) {
          case 0:
            style.backgroundColor = 'pink';
            style.color = 'black';
            break;
          case 1:
            style.backgroundColor = 'lightgreen';
            style.color = 'black';
            break;
          case 2:
            style.backgroundColor = 'lightblue';
            style.color = 'black';
            break;
          case 3:
            style.backgroundColor = 'purple';
            style.color = 'white';
            break;
          case 4:
            style.backgroundColor = 'turquoise';
            style.color = 'white';
            break;
          case 5:
            style.backgroundColor = 'gray';
            style.color = 'white';
            break;
          case 6:
            style.backgroundColor = 'yellow';
            style.color = 'black';
        }
      }
    }
    return style;
  }

  setCurrentDetailedOrderId(id) {
    this.currentDetailedOrderId = id;
  }


  openOrderProgressModal(order) {
    this.openOrderProgress = this.modalService.open(OrderProgressModalComponent);
    this.openOrderProgress.componentInstance.order = order;
  }

}
