import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DispatcherMainBoardComponent } from './dispatcher-main-board/dispatcher-main-board.component';
import { SharedBoardModule } from '../shared-board/shared-board.module';
import { EditOrderComponent } from '../shared-board/edit-order/edit-order.component';
import { TranslateModule } from '@ngx-translate/core';

const dispatcherBoardRoutes: Routes = [
  {
    path: "",
    component: DispatcherMainBoardComponent,
    data: { title: 'Orders Board' }
  },
  {
    path: 'edit-order/:orderId',
    component: EditOrderComponent,
  },
]

@NgModule({
  declarations: [
    DispatcherMainBoardComponent
  ],
  imports: [
    SharedBoardModule,
    RouterModule.forChild(dispatcherBoardRoutes),
    TranslateModule
  ]
})
export class DispatcherBoardModule { }
