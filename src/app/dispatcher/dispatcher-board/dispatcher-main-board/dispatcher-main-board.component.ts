/**
 * 
 * Dispatcher Main Board Component
 * 
 * Drag and drop applied via using angular material here urls
 *  
 * Angular material https://material.angular.io/cdk/drag-drop/overview
 * 
 * ------------------------------------------------------------------------
 * Events used
 * ===========
 * 
 * (cdkDropListDropped) called when item dropped at same list or other list / 
 * (cdkDragMoved) called when dragging item
 * --------------------------------------------------------------------------
 * --------------------------------------------------------------------------
 * Directives used 
 * ===============
 * 
 * cdkDropListGroup to target a group of lists such as assigned orders & unassigned orders /
 * cdkDropList called when target array of items such as orders /
 * cdkDrag to target which div (item) to drag 
 * --------------------------------------------------------------------------
 * Inputs Used
 * ===========
 * 
 *  [cdkDropListData] to pass list data /
 *  [cdkDragData] to pass item data /
 *  [cdkDragDisabled] to disable item from dragging depending on condition
 * 
 */
import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { LookupService } from '../../../api-module/services/lookup-services/lookup.service';
import { UtilitiesService } from '../../../api-module/services/utilities/utilities.service';
import { NotificationsService } from '../../notifications.service';
import { Subscription, interval, Observable } from 'rxjs';
import { SharedNotificationService } from '../../../shared-module/shared/shared-notification.service';
import { MessageService } from 'primeng/primeng';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem
} from '@angular/cdk/drag-drop';
import { DOCUMENT } from '@angular/common';
import { Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/refresh-rate.reducer';
import { RefreshRateService } from 'src/app/services/refresh-rate.service';
import { switchMap } from 'rxjs/operators';


@Component({
  selector: 'app-dispatcher-main-board',
  templateUrl: './dispatcher-main-board.component.html',
  styleUrls: ['./dispatcher-main-board.component.scss'],

})

export class DispatcherMainBoardComponent implements OnInit, OnDestroy {
  @ViewChild('unassignedContainer', { static: false }) unassignedContainer: ElementRef;
  gettingUnassignedOdersInProgress: boolean = false;
  gettingAssignedOdersInProgress: boolean = false;
  fullScreenBoard: boolean = false;
  isFilterActive: boolean = false;
  showFilter: boolean;
  isBulkAssign: boolean;
  toggleOverflowX: string[];
  allUnassignedOrders = [];
  orders: any[] = [];
  foremen: any = [];
  subs: Subscription = new Subscription();
  pageNumber: number = 1;
  terms: any = {
    fromWhere: 3
  };
  scroll$ = new Subscription();
  dragLocation: number = 0;
  leftMoves: number = 0;
  rightMoves: number = 0;

  constructor(
    private lookup: LookupService,
    private utilities: UtilitiesService,
    private notificationsService: NotificationsService,
    public sharedNotificationService: SharedNotificationService,
    private messageService: MessageService,
    @Inject(DOCUMENT) private document: Document,
    private translateService: TranslateService,
    private refreshRateService: RefreshRateService,
    private store: Store<AppState>,
  ) { }

  ngOnInit() {
    this.gettingAssignedOdersInProgress = true;
    this.gettingUnassignedOdersInProgress = true;
    this.notificationsService.getPermission();
    this.receiveNotification();
    this.onRecieveChangeRank();
    this.resetBoardFilter();
    this.subscribeToRefreshRate();
  }

  subscribeToRefreshRate() {
    this.subs.add(this.store.select("refreshRateData")
      .pipe(
        switchMap((refreshRateState) => {
          return interval(refreshRateState.refreshRate * 60000);
        })).
      subscribe(() => {
        this.refreshRateService.setSettings();
        this.showHideFilter();
        this.pageNumber = 1;
        this.terms.pageInfo = {
          pageNo: this.pageNumber,
          pageSize: 10
        }
        this.applyFilterBoardTemplate(this.terms);
      }));
  }

  // Sorting numbers in JS 

  // //Just building on all of the above answers, they can also be done in one line like this:

  // var numArray = [500, 1 ,3 12, 140000, 104, 99];

  // // ES5
  // numArray = numArray.sort(function (a, b) {  return a - b;  });

  // // ES2015
  // numArray = numArray.sort((a, b) => a - b);
  // document.getElementById("demo").innerHTML = numArray;

  // function myFunction() {
  //   fruits.sort();
  //   document.getElementById("demo").innerHTML = numArray;
  // }


  onGetUnassignedOrders(response) {
    if (response.isSucceeded) {
      if (response.data && response.data.data) {
        this.orders = response.data.data;
      } else {
        this.orders = [];
      }
      setTimeout(() => {
        this.unassignedContainer.nativeElement.scroll(0, 0);
      }, 0);
      this.gettingUnassignedOdersInProgress = false;
    } else {
      this.onAPIFailure(response);
    }
  }

  onGetAssignedOrders(res) {
    if (res.isSucceeded) {
      this.foremen = res.data;
      this.foremen.map(forman => {
        forman.teams.map(team => {
          if (team.orders) {
            team.filterOrders = team.orders.slice();
          } else {
            team.filterOrders = [];
            team.orders = [];
          }
        });
      });
      this.gettingAssignedOdersInProgress = false;
    } else {
      this.onAPIFailure(res);
    }
  }


  onAPIFailure(err, msg = `Failed due to server error.`) {
    console.error(err);
    this.gettingAssignedOdersInProgress = false;
    this.gettingUnassignedOdersInProgress = false;
    this.alert('error', this.translateService.instant('Failed'), this.translateService.instant(`Failed due to server error.`));
  }

  // used to update the board data once a notification is received
  private receiveNotification() {
    this.subs.add(this.notificationsService.receiveMessage().subscribe((msg: any) => {
      if (msg.data.NotificationType == "assignDispatcher") {
        let order: any = JSON.parse(msg.data.Order);
        this.orders.push(order);
      } else if (msg.data.NotificationType == "unAssignDispatcher") {
        const orderId = msg.data.Order;
        let unassignedOrders = this.orders;
        this.orders = unassignedOrders.filter((order) => order.id !== +orderId);
      } else if (msg.data.NotificationType == "New Bulk Status") {
        let order = JSON.parse(msg.data.order);
        if (order.teamId) {
          this.foremen.map(foreman => {
            foreman.teams.forEach(team => {
              if (team.teamId == order.teamId) {
                team.filterOrders.push(order);
              }
            });
          });
          this.orders = this.orders.filter((o) => o.id != order.id);
        } else {
          this.orders.push(order);
        }
      } else {
        const order = JSON.parse(msg.data.order);
        this.searchAndUpdateOrAddOrder(order);
      }
    }, (err: any) => this.onAPIFailure(err)));
  }

  // used to decide whether to put order in a team, or to put it in the unassigned orders 
  private searchAndUpdateOrAddOrder(order) {
    this.foremen.map(foreman => {
      foreman.teams.forEach(team => {
        team.filterOrders.forEach((singleOrder, i) => {
          if (singleOrder.id == order.id) {
            if (order.statusName == "Compelete" || order.statusName == "Cancelled") {
              team.filterOrders.splice(i, 1);
            } else if (order.statusName == "On-Hold") {
              team.filterOrders.splice(i, 1);
            } else if (order.acceptanceFlag == 3) {
              team.filterOrders.splice(i, 1);
            } else {
              team.filterOrders[i] = order;
            }
          }
        });
      });
    });

    if ((order.dispatcherId) && (order.statusName == 'On-Hold' || order.statusName == 'Open')) {
      this.orders.push(order);
    }
  }

  // get teams and assigned orders again once user change the position of an order in its team or in another team
  onRecieveChangeRank() {
    this.subs.add(this.sharedNotificationService.acceptChangeRankSubject.subscribe(() => this.getTeamsAndOrders()));
  }

  // get teams and assigned orders
  getTeamsAndOrders() {
    this.gettingAssignedOdersInProgress = true;
    this.lookup.postBoardOrderFilter(this.terms)
      .subscribe(
        (res: any) => this.onGetAssignedOrders(res),
        (err: any) => this.onAPIFailure(err));
  }

  getDispatcherUnassignedOrders(firstPage: boolean) {
    this.gettingUnassignedOdersInProgress = true;
    if (firstPage) {
      this.pageNumber = 1;
    }

    this.terms.pageInfo = {
      pageNo: this.pageNumber,
      pageSize: 10
    };


    this.terms.dispatcherId = localStorage.getItem('AlghanimCurrentUserId'),
      this.lookup.postAllUnAssignedOrdersForDispatcher(this.terms)
        .subscribe(
          (res: any) => this.onGetUnassignedOrders(res),
          (err: any) => this.onAPIFailure(err));
  }

  onScroll() {
    this.pageNumber++;
    this.terms.pageInfo = {
      pageNo: this.pageNumber,
      pageSize: 10
    };
    this.lookup.postAllUnAssignedOrdersForDispatcher(this.terms).subscribe(dispatcher => {
      let array = dispatcher.data.data;
      this.orders.push(...array);
    },
      (err: any) => this.onAPIFailure(err));
  }

  showHideFilter() {
    this.showFilter = !this.showFilter;
  }






  applyFilterBoardTemplate(terms: any) {
    if (terms.dateFrom && terms.dateTo) {
      this.isFilterActive = true;
    }
    this.showHideFilter();
    let emptyFilterObj: any = {
      dispatcherId: localStorage.getItem("AlghanimCurrentUserId")
    };
    this.terms = terms;
    this.terms.dispatcherId = localStorage.getItem("AlghanimCurrentUserId");
    if (terms.fromWhere == 3) {
      this.lookup.postBoardOrderFilter(terms)
        .subscribe(
          (res: any) => this.onGetAssignedOrders(res),
          (err: any) => this.onAPIFailure(err));
      this.lookup.postAllUnAssignedOrdersForDispatcherFilter(terms)
        .subscribe(
          (response: any) => this.onGetUnassignedOrders(response),
          (err: any) => this.onAPIFailure(err));
    } else if (terms.fromWhere == 2) {
      this.lookup.postBoardOrderFilter(terms)
        .subscribe(
          (res: any) => this.onGetAssignedOrders(res),
          (err: any) => this.onAPIFailure(err));

      this.lookup.postAllUnAssignedOrdersForDispatcherFilter(emptyFilterObj)
        .subscribe(
          (response: any) => this.onGetUnassignedOrders(response),
          (err: any) => this.onAPIFailure(err));
    } else if (terms.fromWhere == 1) {
      this.lookup.postBoardOrderFilter(emptyFilterObj)
        .subscribe(
          (res: any) => this.onGetAssignedOrders(res),
          (err: any) => this.onAPIFailure(err));
      this.lookup.postAllUnAssignedOrdersForDispatcherFilter(terms)
        .subscribe(
          (response: any) => this.onGetUnassignedOrders(response),
          (err: any) => this.onAPIFailure(err));
    }
  }

  resetBoardFilter() {
    this.showHideFilter();
    this.isFilterActive = false;
    this.terms = {};
    this.pageNumber = 1;
    this.terms.pageInfo = {
      pageNo: this.pageNumber,
      pageSize: 10
    };
    this.terms.dispatcherId = localStorage.getItem('AlghanimCurrentUserId');
    this.terms.fromWhere = 3;
    this.applyFilterBoardTemplate(this.terms);
  }

  // onChangeRankWithoutNotification(obj) {
  //   this.toggleLoading = true;
  //   this.lookup.rankTeamOrdersWithoutNotification(obj).subscribe(res => {
  //     this.toggleLoading = false;
  //     this.getTeamsAndOrders();
  //   }, err => {
  //     this.toggleLoading = false;
  //     this.alert('error', this.translateService.instant('Failed'), this.translateService.instant(`Failed due to server error.`));
  //   });
  // }

  /**
   * 
   * @param body 
   */
  onChangeRankOld(body: Object) {
    debugger
    this.gettingAssignedOdersInProgress = true;
    this.lookup.rankTeamOrders(body).subscribe(
      res => {
        this.gettingAssignedOdersInProgress = false;
        this.getTeamsAndOrders();
      }, (err: any) => this.onAPIFailure(err, `Can't Assign Order To Unavailable Team!`));
  }


  onChangeRank(body: Object) {
    this.gettingAssignedOdersInProgress = true;
    this.lookup.rankTeamOrders(body).subscribe(
      res => {
        this.gettingAssignedOdersInProgress = false;
        this.getTeamsAndOrders();
        debugger
        if (!res.isSucceeded) {
          this.alert('error', this.translateService.instant('Failed'), this.translateService.instant(res.status.message));

        }
      }, (err: any) => this.onAPIFailure(err, `Can't Assign Order To Unavailable Team!`));
  }


  ngOnDestroy() {
    this.subs.unsubscribe();
  }

  /**
   * Assign order from
   * @param event 
   * @param teamId 
   */
  onDropOrder(event: CdkDragDrop<string[]>, type: string, teamId?: number) {
    this.scroll$.unsubscribe();
    this.leftMoves = 0;
    this.rightMoves = 0;
    const item = event.item.data;
    const sameContainer = event.previousContainer === event.container ? true : false;
    if (sameContainer) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
    let ordersIds = {};
    const array = event.container.data;
    for (let index = 0; index < array.length; index++) {
      const element = array[index];
      ordersIds = { ...ordersIds, ...{ [element['id']]: index } };
    }

    if (type === 'assign' && !sameContainer) {
      const body = {
        teamId: teamId,
        orderId: item.id,
        ordersIds: ordersIds
      };
      this.assignOrder(body);

    } else if (type === 'assign' && sameContainer) {
      const body = {
        teamId: teamId,
        orders: ordersIds,
        orderId: item.id,
      };
      if (event.currentIndex != event.previousIndex) {
        this.onChangeRank(body);
      } else {
        this.alert('error', this.translateService.instant('Failed'), this.translateService.instant(`Please Change Order Position to store Team Rank!`));
      }

    } else if (type === 'unassign' && !sameContainer) {
      const body = {
        orderId: item.id,
      };
      this.unAssignOrder(body);
    }

  }


  /**
   * 
   * @param body 
   */
  assignOrder(body: object) {
    this.gettingAssignedOdersInProgress = true;
    this.lookup.assginOrder(body).subscribe(res => {
      this.gettingAssignedOdersInProgress = false;

      if (res.isSucceeded) {
        delete body['orderId'];
        this.getTeamsAndOrders();
        // this.lookup.rankTeamOrdersWithoutNotification(body).subscribe(res => {
        //   this.toggleLoading = false;
        //   this.getTeamsAndOrders();
        // }, err => {
        //   this.toggleLoading = false;
        //   this.alert('error', 'Failed', `Failed due to server error.`);
        // });
      } else if (res.status.code == 6) {
        this.getTeamsAndOrders();
        this.getDispatcherUnassignedOrders(true);
        this.alert('error', this.translateService.instant('Failed'), this.translateService.instant(`Can't Assign Order To Unavailable Team!`));
      } else {
        this.onAPIFailure(res)
      }
    }, (err: any) => this.onAPIFailure(err));
  }


  /**
   * 
   * @param body 
   */
  unAssignOrder(body: Object) {
    this.gettingUnassignedOdersInProgress = true;
    this.lookup.unAssginOrder(body)
      .subscribe((res: any) => {
        if (res.isSucceeded) {
          this.gettingUnassignedOdersInProgress = false;
          this.getDispatcherUnassignedOrders(true);
        } else {
          this.onAPIFailure(res);
        }
      }, (err: any) => this.onAPIFailure(err));
  }

  /**
   * 
   * @param tpye 
   * @param title 
   * @param details 
   */
  alert(tpye: string, title: string, details?: string) {
    this.messageService.add({
      severity: tpye,
      summary: title,
      detail: details
    });
  }

  scrollSubscription() {
    this.scroll$ = interval(25).subscribe(() => {
      // console.log("inside sub");
      if (this.dragLocation > 0.9 && this.dragLocation < 0.971) {
        this.document.getElementById('technican-overflow-container').scrollBy({
          left: 50,
        });
        // console.log("in drag position", this.dragLocation);
      } else if (this.dragLocation < 0.3 && this.dragLocation > 0.186) {
        this.document.getElementById('technican-overflow-container').scrollBy({
          left: -50,
        });
        // console.log("in drag position", this.dragLocation);
      } else {
        console.log("%cOut Of Drag Area", 'padding: 5px; margin: 5px; background: #111; color: #bada55; font-size: 20px; font-weight: bold;');
      }
    });
  }

  /**
   * calculate left moves and right moves to start Horizontal scroll moving 
   * 
   * 
   * @param event 
   */
  onDragMoved(event) {
    let xPosition = event.event.pageX;
    let totalWidth = this.document.body.offsetWidth;
    this.dragLocation = xPosition / totalWidth;
  }

  /**
   * calculate left moves and right moves to start Horizontal scroll moving 
   * 
   * 
   * @param event 
   */
  onDragStart(event) {
    console.log("onDragStart", event);
    this.scrollSubscription();
  }
}
