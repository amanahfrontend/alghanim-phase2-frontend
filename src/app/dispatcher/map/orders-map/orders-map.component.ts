/// <reference path="../../../../../node_modules/@types/googlemaps/index.d.ts"/>

import {
  Component,
  ViewChild,
  ElementRef,
  NgZone,
  OnInit,
  OnDestroy,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  AfterViewInit,
} from "@angular/core";
import { Subscription, interval, Subject } from "rxjs";
import { LookupService } from "../../../api-module/services/lookup-services/lookup.service";
import { Message } from "primeng/components/common/message";
import Swal from "sweetalert2";
declare const google: any;
import { FormControl } from "@angular/forms";
import { MapsAPILoader, LatLngBounds, AgmMap } from "@agm/core";
import { debounceTime, switchMap, map } from "rxjs/operators";
import { MessageService } from "primeng/api";
import { TranslateService } from "@ngx-translate/core";
import { Router, NavigationEnd } from "@angular/router";
import { RefreshRateService } from "src/app/services/refresh-rate.service";
import { Store } from "@ngrx/store";
import { AppState } from "src/app/store/refresh-rate.reducer";
import { MapOrderModel } from "src/app/models/mapOrder.model";
import { MapFilterComponent } from '../map-filter/map-filter.component';

interface Marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}

@Component({
  selector: "app-orders-map",
  templateUrl: "./orders-map.component.html",
  styleUrls: ["./orders-map.component.css"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrdersMapComponent implements OnInit, OnDestroy {
  @ViewChild(MapFilterComponent,{static: false}) mapFilter : MapFilterComponent;
  isLoading: boolean = false;
  showFilter: boolean;
  unAssignedStatusClass: string;
  fullScreenMap: boolean;
  isBulkAssign: boolean;
  toggleOverflowX: string[];
  mapOrders: MapOrderModel[] = [];
  cachedMapOrders: MapOrderModel[] = [];
  allUnassignedOrders = [];
  filterStatusClass = [];
  orders = [];
  filterObj: any = {};

  lat: number = 29.371586;
  lng: number = 47.990341;
  zoom: number = 14;

  bounds: LatLngBounds;
  public searchControl: FormControl;
  searchResultMarker: Marker = null;
  cornerMessage: Message[];
  boundsObservable = new Subject();
  @ViewChild("search", { static: false })
  public searchElementRef: ElementRef;
  @ViewChild("AgmMap", { static: false })
  public AgmMapElementRef: any;
  showMap: boolean;
  isGetingNewCriteria: boolean = true;
  mapView: string = "roadmap";
  searchBox: any;
  subscriptions = new Subscription();

  constructor(
    private lookup: LookupService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private cd: ChangeDetectorRef,
    private messageService: MessageService,
    private translateService: TranslateService,
    private refreshRateService: RefreshRateService,
    private store: Store<AppState>
  ) { }

  ngOnInit() {
    this.filterStatusClass = ["zero-filter-width"];
    this.unAssignedStatusClass = "full-unassigned-width";
    let dispatcherId = +localStorage.getItem("AlghanimCurrentUserId");
    let dte = new Date();
    dte.setDate(dte.getDate() - 7);
    this.filterObj = {
      dispatcherId: [dispatcherId],
      statusId: [1, 2, 3, 4, 5, 6],
      dateFrom: dte,
      dateTo: new Date(),
    };
    this.showHideFilter();
    this.onBoundsChangeSubscribtion();
    this.searchMap();
    // this.onRefreshRate();
    this.refreshMap();
  }

  ListenForPlaceChanging() {
    // Create the search box and link it to the UI element.
    var input = document.getElementById("txtsearchinput");
    if (!this.searchBox) {
      this.searchBox = new google.maps.places.SearchBox(input);

      this.searchBox.addListener("places_changed", () => {
        var places = this.searchBox.getPlaces();
        if (places.length > 0) {
          // For each place, get the icon, name and location.
          var bounds = new google.maps.LatLngBounds();
          let firstPlace = places[0];
          if (!firstPlace.geometry) {
            console.log("Returned place contains no geometry");
            return;
          } else {
            this.lat = firstPlace.geometry.location.lat();
            this.lng = firstPlace.geometry.location.lng();
            if (!this.cd["destroyed"]) this.cd.detectChanges();
          }
        }
      });
    }

    // let place: google.maps.places.PlaceResult = this.autocomplete.getPlace();

    // let placesList = autocomplete.getPlace();
    // let result = placesList[0];
    // if (!result.geometry) return;
    // if (result.geometry.viewport) {
    //   this.AgmMapElementRef.fitBounds(result.geometry.viewport);
    // } else {
    //   this.AgmMapElementRef.setCenter(result.geometry.location);
    //   this.AgmMapElementRef.setZoom(16);
    // }

    // }
  }

  searchMap() {
    //create search FormControl
    this.searchControl = new FormControl();

    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      this.ListenForPlaceChanging();
      let autocomplete = new google.maps.places.Autocomplete(
        this.searchElementRef.nativeElement,
        {
          keywords: ["address"],
        }
      );
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          //set latitude, longitude and zoom
          this.lat = place.geometry.location.lat();
          this.lng = place.geometry.location.lng();
          this.searchResultMarker = {
            lat: this.lat,
            lng: this.lng,
            label: this.translateService.instant("search result marker"),
            draggable: false,
          };
          if (!this.cd["destroyed"]) {
            this.cd.detectChanges();
          }
        });
      });
    });
  }

  onToggleMap() {
    if (this.mapView == "roadmap") {
      this.mapView = "hybrid";
    } else {
      this.mapView = "roadmap";
    }
    if (!this.cd["destroyed"]) {
      this.cd.detectChanges();
    }
  }

  applyFilterMap(terms: any) {
    this.filterObj = terms;
    this.isGetingNewCriteria = true;
    this.onBoundsChange(this.bounds);
  }

  onBoundsChangeSubscribtion() {
    this.boundsObservable
      .pipe(
        debounceTime(500),
        switchMap(() => this.applyFilterMapObservable())).subscribe((res: any) => {
          // if (res.isSucceeded) {
          this.isLoading = false;
          if (!this.cd["destroyed"]) {
            this.cd.detectChanges();
          }
          this.onDataReady(res);
          // } else {
          //   this.onServerError(res);
          // }
        }, (err) => this.onServerError(err));
  }

  onDataReady(res = []) {
    console.log("orders ", res);
    if (this.mapOrders.length == res.length) {
      for (var i = 0; i < res.length; i++) {
        if (
          res[i].lat !== this.mapOrders[i].lat
          || res[i].long !== this.mapOrders[i].long
          || res[i].code !== this.mapOrders[i].code
          || res[i].pin !== this.mapOrders[i].pin
        ) {
          this.onSetMapPins(res);
          break;
        }
      }
    } else {
      this.onSetMapPins(res);
    }
  }

  onSetMapPins(res) {
    this.mapOrders = [];
    if (!this.cd["destroyed"]) {
      this.cd.detectChanges();
    }
    if (res.length > 0) {
      this.mapOrders = this.cachedMapOrders = res;

      if (!this.cd["destroyed"]) {
        this.cd.detectChanges();
      }
    } else {
      this.messageService.add({
        severity: "info",
        summary: this.translateService.instant("No orders found"),
        detail: this.translateService.instant(`No orders for this criteria!`),
      });
    }
  }

  refreshMap() {
    this.subscriptions.add(
      this.store.select("refreshRateData").pipe(switchMap((refreshRateState) => {
        return interval(refreshRateState.refreshRate * 60000);
      }))
        .subscribe(() => {
          this.refreshRateService.setSettings();
          this.boundsObservable.next();
        })
    );
  }

  toggleUnAssigned(status) {
    if (status) {
      this.unAssignedStatusClass = "full-unassigned-width";
    } else {
      this.unAssignedStatusClass = "zero-filter-width";
    }
  }

  toggleFilter(status) {
    if (status) {
      this.filterStatusClass = ["full-filter-width"];
      if (!this.cd["destroyed"]) {
        this.cd.detectChanges();
      }
    } else {
      this.filterStatusClass = ["zero-filter-width"];
      if (!this.cd["destroyed"]) {
        this.cd.detectChanges();
      }
    }
  }

  showHideFilter() {
    this.showFilter = !this.showFilter;
  }

  resetFilterMap() {
    this.filterObj = {};
    // Set Defaul dates on filter
    this.mapFilter.setMapFilterDateRange(this.filterObj);
    this.isGetingNewCriteria = true;
    this.onBoundsChange(this.bounds);
  }

  applyFilterMapObservable() {
    return this.lookup.postMapOrderFilter(this.filterObj);
  }

  toggleFullScreen() {
    this.fullScreenMap = !this.fullScreenMap;
  }

  onMapReady(e) {
    // console.log("Map Ready", e);
    // console.log("Bounds:", this.bounds);
  }

  onBoundsChange(latLngBounds) {
    // console.log(latLngBounds);
    this.bounds = latLngBounds;
    const neLat = latLngBounds.getNorthEast().lat();
    const neLng = latLngBounds.getNorthEast().lng();
    const swLat = latLngBounds.getSouthWest().lat();
    const swLng = latLngBounds.getSouthWest().lng();
    this.filterObj.SouthWest = { latitude: swLat, longitude: swLng };
    this.filterObj.NorthEast = { latitude: neLat, longitude: neLng };
    if (this.isGetingNewCriteria) {
      this.isLoading = true;
      this.isGetingNewCriteria = false;
      if (!this.cd["destroyed"]) {
        this.cd.detectChanges();
      }
    }
    this.boundsObservable.next();
  }

  markerClicked(item) {
    this.isLoading = true;
    if (!this.cd["destroyed"]) {
      this.cd.detectChanges();
    }
    this.lookup.getOrderByCode(item.code).subscribe(
      (res: any) => {
        if (res.isSucceeded) {
          this.isLoading = false;
          if (!this.cd["destroyed"]) {
            this.cd.detectChanges();
          }
          let item = res.data;
          if (
            item.updatedDate == "0001-01-01T00:00:00" &&
            !(item.statusId == 7 || item.statusId == 8)
          ) {
            item.updatedDate = "";
          }
          Swal.fire({
            title: "<strong>Order details</strong>",
            html: `
              <table class="table">
              <tr>
                <td><b>${this.translateService.instant("Order Code")}: </b></td>
                <td>${item.code || "--"}
                </td>
              </tr>
              <tr>
                <td><b>${this.translateService.instant("Creation Date")}: </b></td>
                <td>${item.saP_CreatedDate.replace("T", " ") || "--"}
                </td>
              </tr>
              <tr>
                <td><b>${this.translateService.instant("Finish Date")}: </b></td>
                <td>${item.updatedDate.replace("T", " ").substring(0, 19) || "--"}
                </td>
              </tr>
              <tr>
                <td><b>${this.translateService.instant("status")}: </b></td>
                <td>${item.statusName || "--"}</td>
              </tr>
              <tr>
                <td><b>${this.translateService.instant("problem")}: </b></td>
                <td>${item.problemName || "--"}</td>
              </tr>
              <tr>
                <td><b>${this.translateService.instant("division")}: </b></td>
                <td>${item.divisionName || "--"}</td>
              </tr>
              <tr>
                <td><b>${this.translateService.instant("dispatcher")}: </b></td>
                <td>${item.dispatcherName || "--"}</td>
              </tr>
              <tr>
                <td><b>${this.translateService.instant("technician")}: </b></td>
                <td>${item.techName || "--"}</td>
      
              </tr>
              <tr>
                <td><b>${this.translateService.instant("area")}: </b></td>
                <td>${item.saP_AreaName || "--"}</td>
              </tr>
              <tr>
                <td><b>${this.translateService.instant("order")} type: </b></td>
                <td>${item.typeName || "--"}</td>
              </tr>
            </table>`,
            showCloseButton: true,
            focusConfirm: false,
          });
        } else {
          this.onServerError(res);
        }

      }, (err) => this.onServerError(err));
  }

  onServerError(err, msg = "failed to load order data due to netowrk error") {
    console.error(err);
    this.messageService.add({
      severity: "error",
      summary: this.translateService.instant("error!"),
      detail: this.translateService.instant(msg),
    });
    this.isLoading = false;
    if (!this.cd["destroyed"]) {
      this.cd.detectChanges();
    }
  }

  ngOnDestroy() {
    this.boundsObservable.unsubscribe();
    this.subscriptions.unsubscribe();
  }
}
