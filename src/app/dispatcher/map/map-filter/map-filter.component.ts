import { Component, OnInit, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { LookupService } from '../../../api-module/services/lookup-services/lookup.service';

@Component({
  selector: 'app-map-filter',
  templateUrl: './map-filter.component.html',
  styleUrls: ['./map-filter.component.css']
})
export class MapFilterComponent implements OnInit {
  filter: any;
  allStatuses = [];
  allAreas: any[] = [];
  allProblems: any[] = [];
  allDispatchers: any[] = [];
  allTechnicians: any[] = [];
  allOrderTypes: any[] = [];
  allDivisions: any[] = [];
  filterObjectToPost: object;
  tecs: any[];
  ListOfFromWhere: any[];
  startDateFrom: Date;
  startDateTo: Date;
  reportOrders: any[] = [];

  @Output() todayOrders = new EventEmitter;
  @Output() filteredOrders = new EventEmitter;
  @Output() resetFilter = new EventEmitter;
  @Output() orderProgressMode = new EventEmitter;
  @Output() filterTermsBoard = new EventEmitter;
  @Output() filterTermsMap = new EventEmitter;


  constructor(private lookup: LookupService) { }

  ngOnInit() {
    this.filter = {};
    this.filter.fromWhere = 3;
    this.filter.RejectionStatus = 4;
    this.getAllStatuses();
    this.getAllAreas();
    this.getAllProblems();
    this.getAllTechnicians();
    this.getAllOrderTypes();
    this.getAllDivisions();
    this.getAllDispatchers();
    this.ListOfFromWhere = [
      {
        label: "Assigned only",
        value: "1"
      },
      {
        label: "Unassigned only",
        value: "2"
      },
      {
        label: "Both",
        value: "3"
      },
    ];

    this.setMapFilterDateRange(this.filter);
  }

  setMapFilterDateRange(filter: any)
  {
     // Set from date a week ago
     let currentdate = new Date()
     currentdate.setDate(currentdate.getDate()-7);
     filter.dateFrom = currentdate;
     // set to date to today
     filter.dateTo = new Date();

     // Preserve the Binding
     this.filter.dateFrom =   filter.dateFrom;
     this.filter.dateTo   =   filter.dateTo;
  }
  onDateFromChanged(e) {
    this.filter.dateFrom = e;
  }

  onDateToChanged(e) {
    this.filter.dateTo = e;
  }

  ngOnDestroy() {
  }

  applyFilter() {
    let filterAreas = [];
    let filterProblems = [];
    let filterStatus = [];
    let filterTechnicians = [];
    let filterOrderTypes = [];
    let filterDivisions = [];
    let filterDispatchers = [];
    // get selected areas
    this.filter.areas && this.filter.areas.map(
      (area) => {
        filterAreas.push(area.id);
      }
    );
    // get selected problems
    this.filter.problems && this.filter.problems.map(
      (problem) => {
        filterProblems.push(problem.id);
      }
    );
    // get selected orders
    this.filter.status && this.filter.status.map(
      (status) => {
        filterStatus.push(status.id);
      }
    );
    // get selected technicians
    this.filter.technicians && this.filter.technicians.map(
      (technician) => {
        if (technician.teamId) {
          filterTechnicians.push(technician.teamId)
        }
      }
    );
    // get selected order types
    this.filter.orderTypes && this.filter.orderTypes.map(
      (ordertype) => {
        filterOrderTypes.push(ordertype.id)
      }
    );
    // get selected divisions
    this.filter.divisions && this.filter.divisions.map(
      (division) => {
        filterDivisions.push(division.id)
      }
    );
    // get selected dispatchers
    this.filter.dispatchers && this.filter.dispatchers.map(
      (dispatcher) => {
        filterDispatchers.push(dispatcher.id)
      }
    );
    this.filterObjectToPost = {
      dateFrom: this.filter.dateFrom,
      dateTo: this.filter.dateTo,
      areaId: filterAreas,
      teamId: filterTechnicians,
      orderTypeId: filterOrderTypes,
      problemId: filterProblems,
      statusId: filterStatus,
      divisionId: filterDivisions,
      dispatcherId: filterDispatchers,
      assigned: this.filter.fromWhere
    };
    // removing unused filter objects to send the used objects only
    for (let property in this.filterObjectToPost) {
      if (this.filterObjectToPost.hasOwnProperty(property)) {
        if (Array.isArray(this.filterObjectToPost[property])) {
          if (!this.filterObjectToPost[property].length) {
            delete this.filterObjectToPost[property];
          }
        } else {
          if (!this.filterObjectToPost[property]) {
            delete this.filterObjectToPost[property];
          }
        }
      }
    }
    this.filterTermsMap.emit(this.filterObjectToPost);
  }


  emitTodayOrders() {
    this.filter = {};
    this.todayOrders.emit();
  }

  checkFilter() {
    let checkingObject = Object.assign({}, this.filter);
    if (JSON.stringify(checkingObject) == JSON.stringify({})) {
      return true;
    } else {
      for (let property in checkingObject) {
        if (checkingObject.hasOwnProperty(property)) {
          if (Array.isArray(checkingObject[property])) {
            if (checkingObject[property].length) {
              return false;
            }
          } else {
            if (checkingObject[property]) {
              return false;
            }
          }
        }
      }
      return true;
    }
  }

  applyResetFilter() {
    this.filter = {
      fromWhere: 3,
      RejectionStatus: 4,
    };
    this.reportOrders = [];
    this.resetFilter.emit();
  }

  getAllStatuses() {
    this.lookup.getAllStatusWithBlockedList()
      .subscribe(
        res => {
          this.allStatuses = res.data;
          this.filter.status = this.allStatuses.filter((s) => s.id != 7 && s.id != 8);
        },
        err => console.log(err)
      );
  }

  getAllProblems() {
    this.lookup.getAllOrderProblemWithBlockedList()
      .subscribe(
        res => this.allProblems = res.data,
        err => console.log(err)
      );
  }

  getAllAreas() {
    this.lookup.getAllAreasWithBlockedList()
      .subscribe(
        res => this.allAreas = res.data,
        err => console.log(err)
      );
  }

  getAllTechnicians() {
    this.lookup.getAllTechniciansWithBlockedList()
      .subscribe(
        res => {
          this.allTechnicians = res.data
          this.allTechnicians = this.allTechnicians.filter((tech) => tech.teamId);
        },
        err => console.log(err)
      );
  }

  getAllOrderTypes() {
    this.lookup.getAllOrderTypesWithBlockedList()
      .subscribe(
        res => this.allOrderTypes = res.data,
        err => console.log(err)
      );
  }

  getAllDivisions() {
    this.lookup.getAllDivisionsWithBlockedList()
      .subscribe(
        res => this.allDivisions = res.data,
        err => console.log(err)
      );
  }

  getAllDispatchers() {
    this.lookup.getAllDispatchersWithBlockedList()
      .subscribe(
        res => {
          this.allDispatchers = res.data;
          let dispatcherId = +localStorage.getItem("AlghanimCurrentUserId");
          let currentDispatcher = this.allDispatchers.filter((dis) => dis.id == dispatcherId);
          this.filter.dispatchers = currentDispatcher || [];
        },
        err => console.log(err)
      );
  }
}
