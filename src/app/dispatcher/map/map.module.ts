import { NgModule } from '@angular/core';
import { SharedModuleModule } from '../../shared-module/shared-module.module';
import { OrdersMapComponent } from './orders-map/orders-map.component';
import { RouterModule } from '@angular/router';
import { AgmCoreModule, LAZY_MAPS_API_CONFIG } from '@agm/core';
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer';
import { GoogleMapsConfig } from '../agm-module';
import { MapFilterComponent } from './map-filter/map-filter.component';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [
    OrdersMapComponent,
    MapFilterComponent
  ],
  imports: [
    SharedModuleModule,
    AgmCoreModule.forRoot(),
    AgmJsMarkerClustererModule,
    RouterModule.forChild([{
      path: "",
      component: OrdersMapComponent
    }]),
    TranslateModule
  ], providers: [
    { provide: LAZY_MAPS_API_CONFIG, useClass: GoogleMapsConfig },
  ]
})
export class MapModule { }
