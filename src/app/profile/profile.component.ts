import { Component, OnInit } from '@angular/core';
import * as myGlobals from '../api-module/services/globalPath'
import { HttpClient } from '@angular/common/http';
import { LanguageService } from './../services/language.service';
import { LowerCasePipe } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers: [
    LowerCasePipe
  ]
})
export class ProfileComponent implements OnInit {

  selectedLanguage: any = {};
  loading = false;
  languageList: any = [];

  constructor(private http: HttpClient,
    private languageService: LanguageService,
    private lowerCasePipe: LowerCasePipe,
    private router: Router) { }

  ngOnInit() {
    this.getLanguages();
  }

  onSubmitLanguage() {
    if (localStorage.getItem('AlghanimCurrentUser')) {
      const userId = JSON.parse(localStorage.getItem('AlghanimCurrentUser')).data.userId;

      let objToPost = {
        UserId: userId,
        LanguageId: this.selectedLanguage.id
      };

      this.loading = true;

      this.http.post(myGlobals.submitLanguageUrlManagement + myGlobals.submitLanguageUrl, objToPost).subscribe((res: any) => {
        if (res.isSucceeded) {
          this.loading = false;

          const lngCode = this.lowerCasePipe.transform(this.selectedLanguage.code);

          this.languageService.changeLanguage(lngCode);
          this.languageService.lngIntoLocalStorage(lngCode);
          this.languageService.refershLanguageId(this.selectedLanguage.id, this.selectedLanguage.code.toLowerCase());

          this.router.navigate(['/']);
        }
      });
    }
  }

  getLanguages() {
    this.http.get(myGlobals.BaseUrlDataManagement + myGlobals.languagesUrl).subscribe((res: any) => {
      if (res.isSucceeded) {
        this.languageList = res.data;

        const id = this.languageService.languageId;
        this.languageList.forEach(element => {
          if (element.id == id) {
            this.selectedLanguage = element;
          }
        });
      }
    });
  }

}
