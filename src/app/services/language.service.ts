/**
 * @author Mustafa Omran <promustafaomran@hotmail.com>
 */
import { Injectable, Inject } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { DOCUMENT } from '@angular/common';
@Injectable({
  providedIn: 'root'
})

export class LanguageService {

  private LANGUAGE: string = this.languageCode || 'en';
  private currentLng: BehaviorSubject<string>;

  constructor(@Inject(DOCUMENT) private document: Document) {
    if (localStorage.getItem('LANGUAGE')) {
      this.LANGUAGE = JSON.parse(localStorage.getItem('LANGUAGE'));
    }

    this.currentLng = new BehaviorSubject(this.LANGUAGE);
    this.changeLanguage(this.LANGUAGE);
  }

  /**
   * change language
   * 
   */
  changeLanguage(lng) {
    const codes = ['ar', 'Ar', 'Ur', 'ur'];
    this.currentLng.next(lng);

    if (codes.includes(lng)) {
      this.document.body.style.direction = "rtl";
    } else {
      this.document.body.style.direction = "ltr";
    }
  }

  /**
   * selected language
   * 
   */
  get language(): Observable<string> {
    return this.currentLng.asObservable()
  }

  /**
   * 
   * @param lng 
   */
  lngIntoLocalStorage(lng: string) {
    return localStorage.setItem('LANGUAGE', JSON.stringify(lng.toLowerCase()));
  }

  /**
   * language id from local storage
   */
  get languageId(): number {
    if (localStorage.getItem('AlghanimCurrentUser')) {
      return JSON.parse(localStorage.getItem('AlghanimCurrentUser')).data.languageId;
    }
  }

  /**
   * language code from local storage
   * 
   */
  get languageCode(): string {
    if (localStorage.getItem('AlghanimCurrentUser')) {
      return JSON.parse(localStorage.getItem('AlghanimCurrentUser')).data.languageCode.toLowerCase();
    }
  }

  /**
   * set new language id & code to local storage
   * 
   * @param id 
   * @param code
   *
   */
  refershLanguageId(id: number, code: string): void {
    if (localStorage.getItem('AlghanimCurrentUser')) {
      let user = JSON.parse(localStorage.getItem('AlghanimCurrentUser'));

      user.data.languageId = id;
      user.data.languageCode = code.toLowerCase();

      return localStorage.setItem('AlghanimCurrentUser', JSON.stringify(user));
    }
  }
}
