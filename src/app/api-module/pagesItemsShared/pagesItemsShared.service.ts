import { Injectable, OnInit } from '@angular/core';


@Injectable()
export class pagesItemSharedService {

    public actionHideMe: any = null;

    constructor() {

    }

    getData() {
        if (JSON.parse(localStorage.getItem('hideMe'))) {
            this.actionHideMe = JSON.parse(localStorage.getItem('hideMe'));
        } else {
            this.actionHideMe = {
                masterData: false,
                usersManagement: false,
                teamManagement: false,
                dispatcherSetting: false,
                orderManagement:false,
                assignTechnicians: false,
                uploadResources: false,
                inventory: false,
                masterDataArea: false,
                masterDataAttendanceStatus: false,
                masterDataAvailabilty: false,
                masterDataBuildingTypes: false,
                masterDataActionStatus: false,
                masterDataContractType: false,
                masterDataOrdrType: false,
                masterDataOrdrPriority: false,
                masterDataOrdrStatus: false,
                masterDataOrdrProblems: false,
                masterDataOrdrProgress: false
            }
        }
    }

    
    public setItemsValues(arr) {
        localStorage.setItem('hideMe', JSON.stringify(this.actionHideMe));
        if (localStorage.getItem('AlghanimCurrentUser')) {
            let userAllRoles = JSON.parse(localStorage.getItem('AlghanimCurrentUser')).roles;
            if (userAllRoles) {
                for (let i = 0; i < arr.length; i++) {
                    const element = arr[i];
                    this[element] = true;
                }

            }
        }
    }

    public getItemsValues() {
        this.getData();
        //  this.actionHideMe = JSON.parse(localStorage.getItem('hideMe'));
    }
}