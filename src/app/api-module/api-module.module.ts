import { LookupService } from './services/lookup-services/lookup.service';
import { AuthGuardGuard } from './guards/auth-guard.guard';
import { userService } from './services/userService/userService.service'
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoleGuard } from './guards/role-guard';

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [AuthGuardGuard,LookupService,userService,RoleGuard],

  declarations: []
})
export class ApiModuleModule { }
