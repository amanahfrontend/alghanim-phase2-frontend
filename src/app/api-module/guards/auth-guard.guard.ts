import { AlertServiceService } from './../services/alertservice/alert-service.service';
import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Location } from '@angular/common';

@Injectable()
export class AuthGuardGuard implements CanActivate {
  public roles: any;

  constructor(private router: Router,
    private _location: Location,
    private alertService: AlertServiceService) { }

  canActivate(route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot) {
    this.roles = route.data['roles'] as Array<string>;
    if (localStorage.getItem('AlghanimCurrentUser')) {
      let UserAllRoles = JSON.parse(localStorage.getItem('AlghanimCurrentUser')).data.roles;
      if (this.checkRoles(this.roles)) {
        if (state.url == '/') {
          if (UserAllRoles.indexOf('Admin') > -1) {
            this.router.navigate(['/admin']);
            return false;
          }
          else if ((UserAllRoles.indexOf("Supervisor") > -1)) {
            this.router.navigate(['/supervisor']);
            return false;
          }
          else if ((UserAllRoles.indexOf('Dispatcher') > -1 || UserAllRoles.indexOf('MovementController') > -1)) {
            this.router.navigate(['/dispatcher']);
            return false;
          }
        }
        return true;
      }
      else {
        if (state.url == '/') {
          if (UserAllRoles.indexOf('Admin') > -1) {
            this.router.navigate(['/admin']);
            return false;
          }
          else if ((UserAllRoles.indexOf("Supervisor") > -1)) {
            this.router.navigate(['/supervisor']);
            return false;
          }
          else if ((UserAllRoles.indexOf('Dispatcher') > -1 || UserAllRoles.indexOf('MovementController') > -1)) {
            this.router.navigate(['/dispatcher']);
            return false;
          }
        }
        else {
          this.alertService.error('You don\'t have permission to Access');
          this.router.navigate(['/']);
          return false;
        }
      }
    }

    // not logged in so redirect to login page with the return url
    this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
    return false;
  }

  checkRoles(listRoles: Array<string>) {
    let UserAllRoles = JSON.parse(localStorage.getItem('AlghanimCurrentUser')).data.roles;
    for (var i = 0; i < this.roles.length; i++) {
      for (var j = 0; j < UserAllRoles.length; j++) {
        if (this.roles[i].toLowerCase() == UserAllRoles[j].toLowerCase()) {
          return true;
        }
      }
    }
    return false;
  }
}
