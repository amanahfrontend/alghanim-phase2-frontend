import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Location } from '@angular/common';

@Injectable()
export class RoleGuard implements CanActivate {
  public roles: any;

  constructor(private router: Router,
    private _location: Location) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this.roles = route.data['roles'] as Array<string>;
    if (localStorage.getItem('AlghanimCurrentUser')) {
      let UserAllRoles = JSON.parse(localStorage.getItem('AlghanimCurrentUser')).data.roles;
      if (UserAllRoles.indexOf('Admin') > -1) {
        return true;
      }
      else if (UserAllRoles.indexOf('Dispatcher') > -1) {
        this.router.navigate(['/dispatcher']);
        return false;
      }
      else if (UserAllRoles.indexOf("Supervisor") > -1) {
        this.router.navigate(['/supervisor']);
        return false;
      }
    }
  }

  checkRoles(listRoles: Array<string>) {
    let UserAllRoles = JSON.parse(localStorage.getItem('AlghanimCurrentUser')).data.roles;
    for (var i = 0; i < this.roles.length; i++) {
      for (var j = 0; j < UserAllRoles.length; j++) {
        if (this.roles[i].toLowerCase() == UserAllRoles[j].toLowerCase()) {
          return true;
        }
      }
    }
    return false;
  }
}
