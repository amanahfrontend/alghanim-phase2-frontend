'use strict';

export const MainPath = 'http://sprintto-001-site1.itempurl.com';

// export const API_Key = 'AIzaSyDJiuQ2hD0H4plorriRdU_VDaenjNsl_7E';
// export const BaseUrl = 'assets/jsonMocks/';
// export const BaseUrl = 'http://192.168.2.102/';
// export const BaseUrl = 'http://159.65.196.91:9000/';
//  export const BaseUrl = 'http://192.168.1.18:9000/'
export const BaseUrl = 'http://157.230.52.100:9000/';


//!Refactoring 
// Uat
// const bashost = 'https://dispatcher-order-dev-api.chevrolet-autline-dev.p.azurewebsites.net/';

//Test
// const bashost = 'https://order-test-amanah.azurewebsites.net/';

//production
// const bashost = 'https://order-test-amanah1.alghanim-prod-ase.p.azurewebsites.net/';

//Staging
// const bashost = 'https://dispatcher-order-dev-api-staging.chevrolet-autline-dev.p.azurewebsites.net/';

// iis
// const bashost = 'http://37.34.189.202:8040/ELGhanimDevBackend/';
const bashost = 'http://localhost:49846/';
// const bashost = 'https://el-ghanim-engineering-api.azurewebsites.net/'
export const BaseUrlIdentity = `${bashost}api/`;
export const BaseUrlDataManagement = BaseUrlIdentity;
export const BaseUrlUserManagementNew = BaseUrlIdentity;
export const BaseUrlOrderManagementNew = BaseUrlIdentity;
export const BaseUrlLocation = BaseUrlIdentity;
export const BaseUrlDropout = bashost;


// // // /////////////////////local Api//////////////////////////////
// export const BaseUrlIdentity = 'http://localhost:49846/api/';
// export const BaseUrlDataManagement = 'http://localhost:51816/api/';
// export const BaseUrlUserManagementNew = 'http://localhost:54042/api/';
// export const BaseUrlOrderManagementNew = 'http://localhost:57535/api/';
// export const BaseUrlLocation = 'https://dispatcher-location-dev-api-staging.chevrolet-autline-dev.p.azurewebsites.net/api/';
// export const BaseUrlDropout = 'https://dispatcher-dropout-dev-api-staging.chevrolet-autline-dev.p.azurewebsites.net/';

/////////////////////Testing Api//////////////////////////////
// export const BaseUrlIdentity = 'https://identity-test-amanah.azurewebsites.net/api/';
// export const BaseUrlDataManagement = 'https://datamgmt-test-amanah.azurewebsites.net/api/';
// export const BaseUrlUserManagementNew = 'https://usermgmt-test-amanah.azurewebsites.net/api/';
// export const BaseUrlOrderManagementNew = 'https://order-test-amanah.azurewebsites.net/api/';
// export const BaseUrlLocation = 'https://location-test-amanah.azurewebsites.net/api/';
// export const BaseUrlDropout = 'https://dropout-test-amanah.azurewebsites.net/';

// ///////////////////UAT Api//////////////////////////////
// export const BaseUrlIdentity = 'https://dispatcher-identiy-dev-api.chevrolet-autline-dev.p.azurewebsites.net/api/';
// export const BaseUrlDataManagement = 'https://dispatcher-datamgmt-dev-api.chevrolet-autline-dev.p.azurewebsites.net/api/';
// export const BaseUrlUserManagementNew = 'https://dispatcher-usermgmt-dev-api.chevrolet-autline-dev.p.azurewebsites.net/api/';
// export const BaseUrlOrderManagementNew = 'https://dispatcher-order-dev-api.chevrolet-autline-dev.p.azurewebsites.net/api/';
// export const BaseUrlLocation = 'https://dispatcher-location-dev-api.chevrolet-autline-dev.p.azurewebsites.net/api/';
// export const BaseUrlDropout = 'https://dispatcher-dropout-dev-api.chevrolet-autline-dev.p.azurewebsites.net/';

/////////////////////Staging Api//////////////////////////////
// export const BaseUrlIdentity = 'https://dispatcher-identiy-dev-api-staging.chevrolet-autline-dev.p.azurewebsites.net/api/';
// export const BaseUrlDataManagement = 'https://dispatcher-datamgmt-dev-api-staging.chevrolet-autline-dev.p.azurewebsites.net/api/';
// export const BaseUrlUserManagementNew = 'https://dispatcher-usermgmt-dev-api-staging.chevrolet-autline-dev.p.azurewebsites.net/api/';
// export const BaseUrlOrderManagementNew = 'https://dispatcher-order-dev-api-staging.chevrolet-autline-dev.p.azurewebsites.net/api/';
// export const BaseUrlLocation = 'https://dispatcher-location-dev-api-staging.chevrolet-autline-dev.p.azurewebsites.net/api/';
// export const BaseUrlDropout = 'https://dispatcher-dropout-dev-api-staging.chevrolet-autline-dev.p.azurewebsites.net/';

// /////////////////////Production Api//////////////////////////////
// export const BaseUrlIdentity = 'https://dispatcher-identiy-prod-api.alghanim-prod-ase.p.azurewebsites.net/api/';
// export const BaseUrlDataManagement = 'https://dispatcher-datamgmt-prod-api.alghanim-prod-ase.p.azurewebsites.net/api/';
// export const BaseUrlUserManagementNew = 'https://dispatcher-usermgmt-prod-api.alghanim-prod-ase.p.azurewebsites.net/api/';
// export const BaseUrlOrderManagementNew = 'https://dispatcher-order-prod-api.alghanim-prod-ase.p.azurewebsites.net/api/';
// export const BaseUrlLocation = 'https://dispatcher-location-prod-api.alghanim-prod-ase.p.azurewebsites.net/api/';
// export const BaseUrlDropout = 'https://dispatcher-dropout-prod-api.alghanim-prod-ase.p.azurewebsites.net/';


export const x = '';

export function testFunc(test) {
  this.x = test;
}

/***************************************/
// SignalR function
/***************************************/

export function vehicleHubUrl(userId, roles, token) {
  return BaseUrl + 'EnmaaVehicle/dispatchingHub?userId=' + userId + '&&roles=' + roles + '&&token=' + token;
}

export function quotationHubUrl(userId, roles, token) {
  return BaseUrl + 'EnmaaEstimationQuotation/quotationHub?userId=' + userId + '&&roles=' + roles + '&&token=' + token;
}

export function callHubUrl(userId, roles, token) {
  return BaseUrl + 'EnmaaCall/callingHub?userId=' + userId + '&&roles=' + roles + '&&token=' + token;
}

export function orderHubUrl(userId, roles, token) {
  return BaseUrl + 'EnmaaOrder/orderingHub?userId=' + userId + '&&roles=' + roles + '&&token=' + token;
}

// http://localhost/EnmaaEstimationQuotation/quotationHub?userId=' + userId + '&&roles=' + roles + '&&token=' + token

/***************************************/
export const orderStatus = BaseUrlOrderManagementNew + 'orderStatus/getAll';
export const orderProgressUrl = 'OrderAction/GetOrderProgress/';
export const addUserDeviceUrl = 'User/AddUserDevice';
export const allProbs = 'Lang_OrderProblem/GetAll';
export const allProbsLocalized = 'Lang_OrderProblem/GetAllLanguages';
export const addProbURL = 'OrderProblem/Add';
export const allAreasDispatcher = BaseUrlDataManagement + 'Areas/GetAll';
export const allOrderProblemsUrl = BaseUrlOrderManagementNew + 'OrderProblem/GetAll';
export const postOrderTypeUrl = BaseUrlOrderManagementNew + 'OrderType/Add'; // Json mocking
export const postOrderStateUrl = BaseUrlOrderManagementNew + 'OrderStatus/Add'; // Json mocking
export const getOrdersByDis = 'Order/GetMapOrdersForDispatcher?dispatcherId=';
// dispatcher map filter api
export const postMapOrderFilterURL = 'Order/OrderFilterMap';
// dispatcher board filter api
export const postBoardOrderFilterURL = 'Order/assignedOrdersBoardFilter';
export const postAllOrdersBoardOrderFilterURL = 'Order/GetAllOrdersForDispatcherFilter';

export const postUnassignedOrdersBoardFilterURL = 'Order/UnassignedOrdersBoardFilter';
// ***********************filter preventive***************
export const postOrderProblems = BaseUrlOrderManagementNew + 'OrderProblem/Add';
export const removeProblemUrl = BaseUrlOrderManagementNew + 'OrderProblem/Delete/';
export const deleteOrderStatusUrl = BaseUrlOrderManagementNew + 'OrderStatus/Delete/'; // Json mocking
export const deleteOrderTypeUrl = BaseUrlOrderManagementNew + 'OrderType/Delete/'; // Json mocking
export const updateOrderStatusUrl = BaseUrlOrderManagementNew + 'OrderStatus/Update';
export const updateOrderTypeUrl = BaseUrlOrderManagementNew + 'OrderType/Update';
export const updateOrderProgressUrl = BaseUrlOrderManagementNew + 'ProgressStatus/Update';
export const updateOrderProblemsUrl = BaseUrlOrderManagementNew + 'OrderProblem/Update';


//*************************************************************
//*************************************************************
//*************************************************************
// -------------------- Customers URLs ------------------------

// export const SearchCustomer = MainPath + '/api/customer/SearchCustomer?searchToken=';
export const CreatenewCustomer = MainPath + '/api/customer/CreateDetailedCustomer';
// export const GetCustomerDetailsByID = MainPath + '/api/customer/CustomerHistory/';
export const EditCustomerInfo = MainPath + '/api/customer/Update';
export const EditContract = MainPath + '/api/contract/Update';
export const AddContract = MainPath + '/api/contract/add';
export const AddNewWorkOrder = MainPath + '/api/WorkOrder/AddWorkOrder';
export const AddLocation = MainPath + '/api/location/add';

export const EditComplain = MainPath + '/api/complain/Update';
export const AddComplain = MainPath + '/api/complain/add';
export const EditWorkOrder = MainPath + '/api/WorkOrder/EditWorkOrder';

//-------------------- Lookups URls -----------------------------
export const allCustomerTypes = MainPath + '/api/CustomerType/getall';
export const allContractType = MainPath + '/api/ContractType/getall';
export const allWorkItems = MainPath + '/api/Item/GetAll';
// export const allLocation = MainPath + '/api/location/getall';
export const allFactories = MainPath + '/api/Factory/GetAll';
// export const allGovernorates = MainPath + '/api/location/GetAllGovernorates';

// export const allAreas = MainPath + '/api/location/GetAreas';
// export const allBlocks = MainPath + '/api/location/GetBlocks';
// export const allStreets = MainPath + '/api/location/GetStreets';
// export const allGetLocationByPaci = MainPath + '/api/location/GetLocationByPaci?paciNumber=';
export const allStatus = MainPath + '/api/Status/getall';


export const editStatus = MainPath + '/api/Status/Update';
export const AddStatus = MainPath + '/api/Status/Add';
export const DeleteStatus = MainPath + '/api/Status/Delete';

export const allRole = MainPath + '/api/Role/getall';
export const editRole = MainPath + '/api/Role/Update';
export const AddRole = MainPath + '/api/Role/Add';
export const DeleteRole = MainPath + '/api/Role/Delete';

export const allPeriorities = MainPath + '/api/Priority/getall';
export const editPeriorities = MainPath + '/api/Priority/Update';
export const AddPeriorities = MainPath + '/api/Priority/Add';
export const DeletePeriorities = MainPath + '/api/Priority/Delete';

export const editCustomerType = MainPath + '/api/CustomerType/Update';
export const AddCustomerType = MainPath + '/api/CustomerType/Add';
export const DeleteCustomerType = MainPath + '/api/CustomerType/Delete';

export const editContractType = MainPath + '/api/ContractType/Update';
export const AddContractType = MainPath + '/api/ContractType/Add';
export const DeleteContractType = MainPath + '/api/ContractType/Delete';

export const allEquipmentType = MainPath + '/api/EquipmentType/getall';
export const editEquipmentType = MainPath + '/api/EquipmentType/Update';
export const AddEquipmentType = MainPath + '/api/EquipmentType/Add';
export const DeleteEquipmentType = MainPath + '/api/EquipmentType/Delete';

export const allEquipment = MainPath + '/api/Equipment/getall';
export const editEquipment = MainPath + '/api/Equipment/Update';
export const AddEquipment = MainPath + '/api/Equipment/Add';
export const DeleteEquipment = MainPath + '/api/Equipment/Delete';

export const editItem = MainPath + '/api/Item/Update';
export const AddItem = MainPath + '/api/Item/Add';
export const DeleteItem = MainPath + '/api/Item/Delete';

export const editFactory = MainPath + '/api/Factory/Update';
export const AddFactory = MainPath + '/api/Factory/Add';
export const DeleteFactory = MainPath + '/api/Factory/Delete';

export const alluser = MainPath + '/api/User/getall';
export const Adduser = MainPath + '/api/User/Add';
export const Deleteuser = MainPath + '/api/User/Delete';

export const DeleteComplain = MainPath + '/api/complain/delete?id=';
export const DeleteContract = MainPath + '/api/contract/delete?id=';
export const DeleteWorkOrder = MainPath + '/api/workorder/delete?id=';

export const UsernameExists = MainPath + '/api/User/UsernameExists?username=';
export const EmailExists = MainPath + '/api/User/EmailExists?email=';
// ---------------------------Work Order Management page -------------------------

export const AllWorkOrdersDispature = MainPath + '/api/WorkOrder/GetAllSuborders';
export const AllWorkOrdersMovementController = MainPath + '/api/WorkOrder/GetByMovementController';
export const AvailableEquipmentInFactory = MainPath + '/api/Equipment/GetAvailableEquipmentInFactory?factoryId=';
export const UpdateAssignmentToWorkOrder = MainPath + '/api/Equipment/UpdateAssignmentToWorkOrder';
export const UpdateSubOrder = MainPath + '/api/WorkOrder/UpdateSubOrder';
export const reassinFactory = MainPath + '/api/WorkOrder/CanAssignToFactory?workorderId=';
export const AssignmentToFactory = MainPath + '/api/WorkOrder/UpdateOrderAssignmentToFactory?orderId=';
export const JobDetails = MainPath + '/api/job/GetByWorkOrder?workorderId=';
export const AvailableVehicleInFactory = MainPath + '/api/vehicle/GetAvailableVehicleInFactory?factoryId=';
export const AddJop = MainPath + '/api/job/add';
export const CancelJop = MainPath + '/api/job/Cancel?jobId=';
export const ReassignJop = MainPath + '/api/job/Reassign?jobId=';
export const AllWorkOrder = MainPath + '/api/WorkOrder/getall';
export const checkCustomerNameExist = MainPath + '/api/customer/IsCustomerNameExist?name=';
export const checkCustomerphoneExist = MainPath + '/api/customer/IsPhoneExist?phone=';
export const AllVehicles = MainPath + '/api/vehicle/getall';


export const NotificationOrders = MainPath + '/api/WorkOrder/getall';
export const RemindOrders = MainPath + '/api/WorkOrder/getall';


export const createUserUrl = 'User/CreateUser';
export const updateUserUrl = 'User/UpdateUser';
export const loginUrl = 'User/Login';
export const loginManagementUrl = 'UserManagementUser/Login';
export const submitLanguageUrlManagement = 'UserManagementUser/SubmitLanguage';
export const submitLanguageUrl = 'User/SubmitLanguage';
export const logoutUrl = 'User/Logout';
export const getuserNameManagementByIdUrl = 'UserManagementUser/GetUserNameById/';
export const getuserNameByIdUrl = 'User/GetUserNameById/';
//////////////////////DataManagement/////////////////////////////////
//////////////Get/////////////
export const areasUrl = 'Lang_Areas/GetAllLanguages';
export const areasPaginatedUrl = 'Areas/GetAllPaginated';
export const attendanceStatusUrl = 'Lang_AttendanceStates/GetAllLanguages';
export const availabilitysUrl = 'Lang_Availability/GetAllLanguages';
export const availabilitysWithoutLangUrl = 'Availability/GetAll';
export const buildingTypesUrl = 'Lang_BuildingTypes/GetAllLanguages';
export const companyCodeUrl = 'Lang_CompanyCode/GetAllLanguages';
export const contractTypesUrl = 'Lang_ContractTypes/GetAllLanguages';
export const costCenterUrl = 'CostCenter/GetAll';
export const divisionUrl = 'Lang_Division/GetAllLanguages';
export const divisionEnUrl = 'Division/GetAll';
export const governoratesUrl = 'Lang_Governorates/GetAllLanguages';
export const shiftUrl = 'Shift/GetAll';
export const languagesUrl = 'SupportedLanguages/GetAll';
export const rolesUrl = 'Role/GetAll';
export const adminUserUrl = 'User/GetAdminUsers';
/////////////////// with block /////////////////
export const areasUrlWithBlocked = 'Lang_Areas/GetAllLanguagesWithBlocked';
export const attendanceStatusUrlWithBlock = 'Lang_AttendanceStates/GetAllLanguagesWithBlocked';
export const availabilitysUrlWithBlock = 'Lang_Availability/GetAllLanguagesWithBlocked';
export const buildingTypesUrlWithBlock = 'Lang_BuildingTypes/GetAllLanguagesWithBlocked';
export const companyCodeUrlWithBlock = 'Lang_CompanyCode/GetAllLanguagesWithBlocked';
export const contractTypesUrlWithBlock = 'Lang_ContractTypes/GetAllLanguagesWithBlocked';
export const costCenterUrlWithBlock = 'CostCenter/GetAllWithBlocked';
export const divisionUrlWithBlock = 'Lang_Division/GetAllLanguagesWithBlocked';
export const governoratesUrlWithBlock = 'Lang_Governorates/GetAllLanguagesWithBlocked';
export const shiftUrlWithBlock = 'Shift/GetAllWithBlocked';
export const languagesUrlWithBlock = 'SupportedLanguages/GetAllWithBlocked';
export const allProbsLocalizedWithBlock = 'Lang_OrderProblem/GetAllLanguagesWithBlocked';
//export const getWorkingTypesWithBlock = 'WorkingType/GetAllGroupedWithBlocked';
export const getWorkingTypesWithBlock = 'Lang_WorkingType/GetAllGroupedWithBlocked';
export const getOrderStatusWithBlock = 'Lang_OrderStatus/GetAllOrderStatusWithBlocked';
//export const orderSubStatusWithBlock = 'OrderSubStatus/GetByStatusIdWithBlocked?id=';
export const orderSubStatusWithBlock = 'Lang_OrderSubStatus/GetByStatusIdWithBlocked?id=';

/////////GetById///////////////
export const areaLanguageByIdUrl = 'Lang_Areas/GetAllLanguagesByAreaId/';
export const attendanceStatesByIdUrl = 'Lang_AttendanceStates/GetAllLanguagesByAttendanceStatesId/';
export const availabilityByIdUrl = 'Lang_Availability/GetAllLanguagesByAvailabilityId/';
export const buildingTypesByIdUrl = 'Lang_BuildingTypes/GetAllLanguagesByBuildingTypesId/';
export const companyCodeByIdUrl = 'Lang_CompanyCode/GetAllLanguagesByCompanyCodeId/';
export const contractTypesByIdUrl = 'Lang_ContractTypes/GetAllLanguagesByContractTypesId/';
export const CostCentersByIdUrl = 'Lang_CostCenter/GetAllLanguagesByCostCenterId/';
export const divisionByIdUrl = 'Lang_Division/GetAllLanguagesByDivisionId/';
export const governoratesByIdUrl = 'Lang_Governorates/GetAllLanguagesByGovernorateId/';
export const ShiftByIdUrl = 'Lang_Shift/GetAllLanguagesByShiftId/';
//OrderStatus
export const OrderStatusByIdUrl = 'Lang_OrderStatus/GetAllLanguagesByOrderStatusId/';
//////////////Post/////////////
export const postPoblemUrl = 'Lang_OrderProblem/Update';
export const postAreasUrl = 'Areas/Add';
export const postAttendanceStates = 'AttendanceStates/Add';
export const postAvailability = 'Availability/Add';
export const postBuildingTypes = 'BuildingTypes/Add';
export const postCompanyCode = 'CompanyCode/Add';
export const postContractTypes = 'ContractTypes/Add';
export const postCostCenter = 'CostCenter/Add';
export const postDivision = 'Division/Add';
export const postGovernorates = 'Governorates/Add';
export const postShift = 'Shift/Add';
export const postLanguages = 'SupportedLanguages/Add';

export const postAreaLanguage = 'Lang_Areas/AddMulti';
export const postAttendanceStatesLanguage = 'Lang_AttendanceStates/AddMulti';
//OrderStatus
export const postOrderStatusLanguage = 'Lang_OrderStatus/AddMulti';
export const postAvailabilityLanguage = 'Lang_Availability/AddMulti';
export const postBuildingTypesLanguage = 'Lang_BuildingTypes/AddMulti';
export const postCompanyCodeLanguage = 'Lang_CompanyCode/AddMulti';
export const postContractTypesLanguage = 'Lang_ContractTypes/AddMulti';
export const postCostCenterLanguage = 'Lang_CostCenter/AddMulti';
export const postDivisionLanguage = 'Lang_Division/AddMulti';
export const postGovernoratesLanguage = 'Lang_Governorates/AddMulti';
export const postShiftLanguage = 'Lang_Shift/AddMulti';

export const postOrdersManagementReport = 'Order/GetOrderManagementReportPaginatedRefactored';
export const exportOrdersManagementReport = 'Order/GetOrderManagementReportExportRefactored';


//////////////put///////////
export const putAreasUrl = 'Areas/Update';
export const putAttendanceStatesUrl = 'AttendanceStates/Update';
export const putAvailabilityUrl = 'Availability/Update';
export const putBuildingTypesUrl = 'BuildingTypes/Update';
export const putCompanyCodeUrl = 'CompanyCode/Update';
export const putContractTypesUrl = 'ContractTypes/Update';
export const putCostCenterUrl = 'CostCenter/Update';
export const putDivisionUrl = 'Division/Update';
export const putGovernoratesUrl = 'Governorates/Update';
export const putShiftUrl = 'Shift/Update';
export const putLanguagesUrl = 'SupportedLanguages/Update';

export const putAreaLanguageUrl = 'Lang_Areas/UpdateAllLanguages';
export const putAttendanceStatesLanguageUrl = 'Lang_AttendanceStates/UpdateAllLanguages';
export const putWorkingTypeLanguageUrl = 'Lang_WorkingType/UpdateAllLanguages';
export const putOrderSubStatusLanguageUrl = 'Lang_OrderSubStatus/UpdateAllLanguages';
//OrderStatus
export const putOrderStatusLanguageUrl = 'Lang_OrderStatus/UpdateAllLanguages';
export const putAvailabilityLanguageUrl = 'Lang_Availability/UpdateAllLanguages';
export const putBuildingTypesLanguageUrl = 'Lang_BuildingTypes/UpdateAllLanguages';
export const putCompanyCodeLanguageUrl = 'Lang_CompanyCode/UpdateAllLanguages';
export const putContractTypesLanguageUrl = 'Lang_ContractTypes/UpdateAllLanguages';
export const putLang_CostCenterLanguageUrl = 'CostCenter/Update';
export const putDivisionLanguageUrl = 'Lang_Division/UpdateAllLanguages';
export const putGovernoratesLanguageUrl = 'Lang_Governorates/UpdateAllLanguages';
export const putShiftLanguageUrl = 'Lang_Shift/UpdateAllLanguages';
export const putProblemLanguageUrl = 'Lang_OrderProblem/UpdateAllLanguages';

/////////////delete////////////////
export const deleteAreaUrl = 'Areas/Delete/';
export const deleteAttendanceStatesUrl = 'AttendanceStates/Delete/';
export const deleteAvailabilityUrl = 'Availability/Delete/';
export const deleteBuildingTypesUrl = 'BuildingTypes/Delete/';
export const deleteCompanyCodeUrl = 'CompanyCode/Delete/';
export const deleteContractTypesUrl = 'ContractTypes/Delete/';
export const deleteCostCenterUrl = 'CostCenter/Delete/';
export const deleteDivisionUrl = 'Division/Delete/';
export const deleteGovernoratesUrl = 'Governorates/Delete/';
export const deleteShiftUrl = 'Shift/Delete/';
export const deleteLanguagesUrl = 'SupportedLanguages/Delete/';

export const deleteAreaLanguagesUrl = 'Lang_Areas/Delete/';
export const deleteAttendanceStatesLanguagesUrl = 'Lang_AttendanceStates/Delete/';
//OrderStatus
export const deleteOrderStatusLanguagesUrl = 'Lang_OrderStatus/Delete/';

////////////// delete logical (block) ////////////////
export const blockArea = 'Areas/LogicalDelete/';
export const blockAttendanceStatus = 'AttendanceStates/LogicalDelete/';
export const blockAvailability = 'Availability/LogicalDelete/';
export const blockBuildingType = 'BuildingTypes/LogicalDelete/';
export const blockCompanyCode = 'CompanyCode/LogicalDelete/';
export const blockContractType = 'ContractTypes/LogicalDelete/';
export const blockCostCenter = 'CostCenter/LogicalDelete/';
export const blockDivision = 'Division/LogicalDelete/';
export const blockGovernorate = 'Governorates/LogicalDelete/';
export const blockShift = 'Shift/LogicalDelete/';
export const blockLanguage = 'SupportedLanguages/LogicalDelete/';
export const blockProblem = 'OrderProblem/LogicalDelete/';
export const blockWorkingType = 'WorkingType/LogicalDelete/';
export const blockSubstatus = 'OrderSubStatus/LogicalDelete/';
//OrderStatus
export const blockOrderStatus = 'OrderStatus/LogicalDelete/';


///////////////////User Managent////////////////////
////////Get//////////
export const areasEnUrl = 'Areas/GetAll';
export const areasWithblocked = 'Areas/GetAllWithblocked';
export const getDispatcherUrl = 'Dispatcher/GetAll'
export const getDispatcherWithBlockedUrl = 'Dispatcher/GetAllWithblocked'
export const getDispatcherBySuperIdUrl = 'Dispatcher/GetDispatchersDataBySupervisorId/'
export const getDriverUrl = 'Driver/GetAll'
export const getICAgentUrl = '';
export const getSuperVisorUrl = 'Supervisor/GetAll'
export const getSuperVisorByDivisionIdUrl = 'Supervisor/GetSupervisorsDataByDivisionId/'
export const getEngineerUrl = 'Engineer/GetAll'
export const getEngineersByDivisionIdUrl = 'Engineer/GetEngineersByDivisionId /'
export const getTechnicianUrl = 'Technician/GetAll'
export const getAssignedTechnicianUrl = 'Technician/GetAllAssignedTechnicians'
export const getForemanUrl = 'Foreman/GetAll'
export const getManagerUrl = 'Manager/GetAll'
export const getMaterialUrl = 'Material/GetAll';
export const getTeamsForForemen = 'Team/GetByFormanId'
export const getTeamsWithBlockedForForemen = 'Team/GetByFormansIdWithBlocked'


/////// get with block ///////
export const getDispatcherUrlWithBlock = 'Dispatcher/GetAllWithBlocked';
export const getSupervisorUrlWithBlock = 'Supervisor/GetAllWithBlocked';
export const getEngineerUrlWithBlock = 'Engineer/GetAllWithBlocked';
export const getForemanUrlWithBlock = 'Foreman/GetAllWithBlocked';
export const getTechnicianUrlWithBlock = 'Technician/GetAllWithBlocked';
export const getManagerUrlWithBlock = 'Manager/GetAllWithBlocked';
export const getMaterialUrlWithBlock = 'Material/GetAllWithBlocked';

////// block /////////
export const blockDispatcher = 'Dispatcher/Block/';
export const blockSupervisor = 'Supervisor/Block/';
export const blockEngineer = 'Engineer/Block/';
export const blockForeman = 'Foreman/Block/';
export const blockTechnician = 'Technician/Block/';
export const blockManager = 'Manager/Block/';
export const blockMaterialController = 'Material/Block/';


/////////Post/////////////
export const postUserUrl = 'User/Add'
export const postUserDispatcherUrl = 'Dispatcher/Add'
export const postSupervisorUrl = 'Supervisor/Add'
export const postDriverUrl = 'Driver/Add'
export const postEngineerUrl = 'Engineer/Add'
export const postTechnicianUrl = 'Technician/Add'
export const postForemanUrl = 'Foreman/Add'
export const postManagerUrl = 'Manager/Add';
export const postMaterialControllerUrl = 'Material/Add';
export const resetPassword = 'User/ResetPassword';
export const addWorkingType = 'WorkingType/Add';


//////////Update//////////////
export const putUserUrl = 'User/Update'
export const putUserDispatcherUrl = 'Dispatcher/Update'
export const putUserSupervisorUrl = 'Supervisor/Update'
export const putUserDriverUrl = 'Driver/Update'
export const putUserMangerUrl = 'Manager/Update';
export const putUserMaterialControllerUrl = 'Material/Update';
export const putUserEngineerUrl = 'Engineer/Update'
export const putUserTechnicianUrl = 'Technician/Update'
export const putUserForemanUrl = 'Foreman/Update'


//////////////GetById///////////////
export const getDispatcherUserById = 'Dispatcher/GetDispatcherUser/';
export const getSupervisorUserById = 'Supervisor/GetSupervisorUser/';
export const getSupervisorUser = "Supervisor/Get/";
export const getSupervisorUserByUserId = 'Supervisor/GetSupervisorByUserId/';
export const getDriverUserById = 'Driver/GetDriverUser/'
export const getEngineerUserById = 'Engineer/GetEngineerUser/'
export const getTechnicianUserById = 'Technician/GetTechnicianUser/'
export const getForemanUserById = 'Foreman/GetForemanUser/'
export const getMangerUserById = 'Manager/GetManagerUser/';
export const getMateialControllerUserById = 'Material/GetMaterialControllerUser/';
///////////GetByAnotherRole
export const getSupervisorUserBydivison = 'Supervisor/GetSupervisorsByDivisionId/';
export const getEngineerUserBydivison = 'Engineer/GetEngineerUsersByDivisionId/';
export const getDriverUserBydivison = 'Driver/GetDriverUsersByDivisionId/';
export const getDispatchersUserBySuper = 'Dispatcher/GetDispatcherUsersBySupervisorId/';
export const getFormanUserByDispatcher = 'Foreman/GetForemansByDispatcherId/';


////////////////Identity/////////////////
export const getrUserById = 'User/Get?id=';
export const checkUserNameExist = 'User/IsUsernameExist';
export const checkUserPhoneExist = 'User/IsPhoneExist';
export const checkUserEmailExist = 'User/IsEmailExist';
export const deactivateUserByUsername = 'User/ToggleActivation?username=';

///////////////////Team Management////////////////////////
/////////Get/////////
export const addVehicleURL = "Vehicle/Add"
export const updateVehicleURL = 'Vehicle/Update';
export const getVehicleAllUrl = 'Vehicle/GetAll';
export const getVehicleByPlateNoUrl = 'TeamMember/SearchVec?word=';
export const getTechnicianByPFUrl = 'TeamMember/SearchTech?word=';
export const getTeamByNameUrl = 'Team/SearchByTeamName?word=';
export const getUnassignedVehiclesUrl = 'TeamMember/GetUnassignedVehicleMember';
export const getUnassignedMempersUrl = 'TeamMember/GetUnassignedMember';
export const getUnassignedMempersByDivisionIdUrl = 'TeamMember/GetUnassignedMemberByDivisionId/';
export const getByDispatcherByIdUrl = 'Team/GetByDispatcherId/';
export const getTeams = 'Team/GetAllWithDispatcher';
export const getAllTeams = 'Team/GetAll';
export const getMembersByTeamIdUrl = 'TeamMember/GetMembersByTeamId/';

////////////Post///////////////
export const postTeam = 'Team/Add';
export const updateTeam = 'Team/Update';
export const assignTeamMember = 'TeamMember/assignedMemberToTeam';
export const assignTeamMembers = 'TeamMember/assignedMembersToTeam/';
export const reassignTeamToDispatcher = 'Team/ReassignTeamToDispatcher';
export const unassignedMember = 'TeamMember/UnassignedMember';
export const unassignedMembersURL = 'TeamMember/UnassignedMembers';
export const postProblemArea = 'DispatcherSettings/Add'

////////////delete/////////////////////
export const deleteTeamURL = 'Team/Delete?teamId={id}';


////////////////////////Dispatcher Setting////////////
export const OrderProblem = 'OrderProblem/GetAll';
export const OrderProblemWithBlocked = 'OrderProblem/GetAllWithblocked ';
export const disSettingUrl = 'DispatcherSettings/GetAll';
export const GetMultiAreasAndProblemsbyDispatcherId = 'DispatcherSettings/GetMultiAreasAndProblemsbyDispatcherId/{dispatcherId}';
export const getDispatcherSettingById = 'DispatcherSettings/GetbyDispatcherId/{0}';
export const deleteDispatcherSetting = 'DispatcherSettings/Delete/{0}';
export const disSettingUrls = 'DispatcherSettings/Search';
export const postDispatcherAllProblemsandArea = 'DispatcherSettings/AddWithMultiAreasAndOrderProblems';
export const UpdateMulitWithAreasAndOrderProblemsUrl = 'DispatcherSettings/UpdateMulitWithAreasAndOrderProblems';
export const AddMulitWithAreasAndOrderProblemsUrl = 'DispatcherSettings/AddWithMultiAreasAndOrderProblems  ';
export const getALLDispatcherSettingsListUrl = "DispatcherSettings/GetAllDispatchers";
export const getALLDispatcherSettingsbyGroupIdUrl = 'DispatcherSettings/GetbyGroupId/{GroupId}';


//////////////////////OrderManagement/////////////////////////////////
//////////////Get/////////////
export const unAssignedOrdersForDispatcherUrl = 'Order/GetUnAssignedOrdersForDispatcher?dispatcherId=';
export const unAssignedOrdersForDispatcherPaginatedUrl = 'Order/GetUnAssignedOrdersForDispatcherPaginated';
export const unAssignedOrdersForDispatcherPaginatedFilterUrl = 'Order/GetUnAssignedOrdersForDispatcherFilter';
export const assignedOrdersForDispatcherUrl = 'Order/GetAssignedOrdersForDispatcher?dispatcherId=';
export const unAssignedOrdersForSupervisorUrl = 'Order/GetUnAssignedOrdersForSupervisor?supervisrId=';
export const unAssignedOrdersForSupervisorFilteredPaginatedUrl = 'Order/GetUnAssignedOrdersForSupervisorFilter';
export const assignedOrdersForSupervisorFilteredPaginatedUrl = 'Order/GetAssignedOrdersForDispatchersBySupervisorFilter';
export const unAssignedOrdersForSupervisorPaginatedUrl = 'Order/GetUnAssignedOrdersForSupervisorPaginated';
export const assignedOrdersForSupervisorUrl = 'Order/GetAssignedOrdersForDispatchersBySupervisor?supervisorId=';
export const assigndOrdersToDispatcherUrl = 'Order/AssignDispatcher';
export const unAssigndOrdersFromDispatcherUrl = 'Order/UnAssignDispatcher';
export const assigndOrdersUrl = 'Order/Assign';
export const unAssigndOrdersUrl = 'Order/UnAssign';
export const rankTeamOrdersUrl = 'Order/ChangeTeamRank';
export const rankTeamOrderskByDragUrl = 'Order/ChangeTeamRankByDrag/true';
export const rankTeamOrdersWithoutNitificationUrl = 'Order/ChangeOrderRankWithoutNotification';
export const orderDetailsUrl = 'Order/Get/{ID}';
export const updateOrderUrl = 'Order/UpdateAsync';
export const getAllPriorityUrl = 'OrderPriority/GetAll';
export const getAllStatusUrl = 'OrderStatus/GetAll';
export const getAllStatusWithBlockedUrl = 'OrderStatus/GetAllWithblocked';
export const getCompanyCodeUrl = 'CompanyCode/GetAll';
export const allDivisionsUrl = 'Division/GetAll';
export const allDivisionsWithBlockedUrl = 'Division/GetAllWithblocked';
export const allOrderTypesUrl = 'OrderType/GetAll';
export const allOrderTypesWithBlockedUrl = 'OrderType/GetAllWithblocked';
export const getPaciLocationUrl = 'Paci/GetLocationByPaci?paciNumber=';
export const filterMapOrdersURL = 'Order/OrderFilter';
export const orderSubStatus = 'OrderSubStatus/GetByStatusId?id=';
export const updateOrderSubStatus = 'OrderSubStatus/Update';
export const addOrderSubStatus = 'OrderSubStatus/Add';
export const orderChangeStatus = 'Order/ChangeStatus';
export const getFilteredOrdersForPrint = 'Order/PrintOut'
export const getOrderByCode = 'Order/GetOrderByCode?code='
export const orderBulkAssign = 'Order/BulkAssign'


////////////////////// time sheet //////////////////
export const TimeSheetUrlgetdate = 'OrderAction/GetTimeSheet';
export const exportTimeSheetResultsUrl = 'OrderAction/GetTimeSheetExcelExport';



//////////////////NotificationCenter //////////////////////

export const PostNotificationCenterURL = 'NotificationCenter/GetByRecieverIdAndIsRead/';
export const GetNotificationCenterByIdURL = 'NotificationCenter/Get/{id}';
export const GetNotificationCenterCountURL = 'NotificationCenter/Count/';
export const PostNotificationCenterGetByIdsURL = 'NotificationCenter/GetByIds';
export const GetAllNotificationCenterURL = 'NotificationCenter/GetAll';
export const PutNotificationCenterItemURL = 'NotificationCenter/Update/';
export const ChangeReadFlagNotificationCenterItemURL = 'NotificationCenter/ChangeReadFlag/';
export const ChangeActionFlagNotificationCenterItemURL = 'NotificationCenter/ChangeActionFlag/';
export const PostAddNotificationCenterURL = 'NotificationCenter/Add';
export const PostAddMultiNotificationCenterURL = 'NotificationCenter/AddMulti';
export const GetAllPaginatedNotificationCenterURL = 'NotificationCenter/GetAllPaginated';
export const GetAllPaginatedByPredicateNotificationCenterURL = 'NotificationCenter/GetAllPaginatedByPredicate';
export const DeleteNotificationCenterURL = 'NotificationCenter/Delete/{id}';
export const GetNotificationCenterCountByRecieverIdURL = 'NotificationCenter/CountByRecieverId/{recieverId}';
export const MarkAllAsReadByRecieverIdURL = 'NotificationCenter/MarkAllAsRead/';
export const PutNotificationCenterChangeTeamOrderRankURL = 'Order/ChangeOrderRank/{acceptChange}';
export const PostNotificationCenterFilteredByDate = 'NotificationCenter/FilterByDate';
export const getAllNotificationCenterByTechAndOrderCode = 'NotificationCenter/FilterByTechAndOrderCode';


///////////////map settings
export const getMapSettingsURL = 'MapAdminRelated/Get/';
export const putMapSettingsURL = 'MapAdminRelated/Update';

///////////////Working types
export const getWorkingTypes = 'WorkingType/GetAllGrouped';
export const updateWorkingType = 'WorkingType/Update';


//////////delete all
export const deleteAllOrders = 'Order/DeleteAllOrders';
export const deleteAllDispatcherSettings = 'DispatcherSettings/DeleteAllDispatcherSettings';
// important note about below api, when deleting all users from database,
// delete them from both user management api and identity api followed,
// by the below link
export const deleteAllManagementUsers = 'UserManagementUser/DeleteAllUsers';
export const deleteAllUsers = 'User/DeleteAllUsers';
