
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import * as myGlobals from '../globalPath';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class userService {

  constructor(private http: HttpClient) { }

  checkUserNameExist(name: any): Observable<any> {
    return this.http.get(myGlobals.BaseUrlIdentity + myGlobals.checkUserNameExist + '?userName=' + `${name}`);
  }

  checkUserPhoneExist(phone: any): Observable<any> {
    return this.http.get(myGlobals.BaseUrlIdentity + myGlobals.checkUserPhoneExist + '?phone=' + `${phone}`);
  }


  checkUserEmailExist(email: any): Observable<any> {
    return this.http.get(myGlobals.BaseUrlIdentity + myGlobals.checkUserEmailExist + '?email=' + `${email}`);
  }

  // private helper methods

  // private jwt() {
  //   // create authorization header with jwt token
  //   let currentUser = JSON.parse(localStorage.getItem('AlghanimCurrentUser'));
  //   if (currentUser && currentUser.data.token) {
  //     let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data.token });
  //     headers.append('Content-Type', 'application/json');
  //     return new RequestOptions({ headers: headers });
  //   }
  // }
}
