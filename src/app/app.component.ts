import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { AuthenticationServicesService } from './api-module/services/authentication/authentication-services.service';
import { UtilitiesService } from './api-module/services/utilities/utilities.service';
import { LookupService } from './api-module/services/lookup-services/lookup.service';
import { NotificationsService } from './dispatcher/notifications.service';
import { MessageService } from 'primeng/api';
import { Growl } from 'primeng/primeng';
import { Router } from '@angular/router';
import { Subscription, timer } from 'rxjs';
import { Observable } from "rxjs/Observable";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AppState } from './store/refresh-rate.reducer';
import { Store } from '@ngrx/store';
import { RefreshRateService } from './services/refresh-rate.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  title = 'app';
  menuItemToggle: boolean;
  roles: string[];
  toggleFullWidth: any;
  growlMessage = [];
  @ViewChild('growl', { static: false }) growl: Growl;


  /////// reload upon web connected
  // public isOnline: boolean;
  // public showConnectionStatus: boolean;
  // private showConnectionStatusSub: Subscription;
  // private showConnectionStatusTimer: Observable<any>;


  // @HostListener('window:offline', ['$event']) onOffline() {
  //   this.isOnline = false;
  //   this.showConnectionStatus = true;
  //   if (this.showConnectionStatusSub) {
  //     this.showConnectionStatusSub.unsubscribe();
  //   }
  // }


  // @HostListener('window:online', ['$event']) onOnline() {
  // this.isOnline = true;
  // this.showConnectionStatus = true;
  // this.showConnectionStatusSub = this.showConnectionStatusTimer.subscribe(() => {
  // this.showConnectionStatus = false;
  // this.showConnectionStatusSub.unsubscribe();
  // window.location.reload();
  // });
  // }

  ///////////////////////////////////////

  constructor(private authService: AuthenticationServicesService,
    private utilities: UtilitiesService,
    private lookupService: LookupService,
    private notificationsService: NotificationsService,
    private messageService: MessageService,
    private router: Router,
    private modalService: NgbModal,
    private refreshRateService: RefreshRateService,
    private store: Store<AppState>,
  ) {
    /////// reload upon web connected
    // this.showConnectionStatusTimer = timer(5000);
    ///////////////////////////
  }

  ngOnInit() {
    this.messageService.clearObserver.subscribe((res) => {
      if (res != 'blockItem') {
        this.growl.value = null;
      }
    });
    this.messageService.messageObserver.subscribe((m: any) => {
      if (m && m.key && m.key == 'blockItem') {
        this.growl.value = null;
      } else {
        this.growl.value = [m];
      }
    });
    this.roles = [];
    this.authService.userRoles.subscribe((roles) => {
      this.roles = roles || [];
    });
    this.subscribeToGrowlMessage();
    this.refreshRateService.setSettings();
    this.subscribeToNotification();
  }

  subscribeToNotification() {
    this.notificationsService.receiveMessage()
      .subscribe(
        (msg: any) => {
          this.utilities.setGrowlMessage({
            severity: 'success',
            summary: 'Message',
            detail: msg.notification.body
          });
        }, err => console.error(err)
      );


    window.addEventListener('storage', (event) => {
      if (event.storageArea == localStorage) {
        let token = localStorage.getItem('AlghanimCurrentUser');
        if (token == undefined) {
          this.modalService.dismissAll();
          setTimeout(() => {
            this.growl.value = null;
          }, 0);
          this.router.navigate(['login']);
        }
      }
    });

  }

  subscribeToGrowlMessage() {
    this.utilities.growlMessage.subscribe((message) => {
      this.growlMessage.push(message);
    });
  }

}