import {
    HttpInterceptor,
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpHeaders,
    HttpResponse,
    HttpErrorResponse
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})

export class TokenInterceptor implements HttpInterceptor {

    constructor(
        private router: Router
    ) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let currentUser = JSON.parse(localStorage.getItem('AlghanimCurrentUser'));
        let authReq = null;
        if (currentUser && currentUser.data.token) {
            authReq = req.clone(
                {
                    headers: new HttpHeaders(
                        {
                            'Authorization': "bearer " + currentUser.data.token,
                            'Content-Type': 'application/json',
                            'Access-Control-Allow-Origin': '*',
                            'LanguageId': '' + (currentUser.data.languageId || 1)
                        }
                    )
                }
            );

        } else {
            authReq = req.clone(
                {
                    headers: new HttpHeaders(
                        {
                            'Content-Type': 'application/json',
                            'Access-Control-Allow-Origin': '*'
                        }
                    )
                }
            );
        }

        return next.handle(authReq).pipe(
            tap((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                }
            }, (err: any) => {
                if (err instanceof HttpErrorResponse) {
                    console.error("Inerceort Error ", err);
                    if (err.status == 401 || err.status == 402 || err.status == 403) {
                        localStorage.clear();
                        this.router.navigate(['/login']);
                        console.error(err);
                    }
                }
            })
        );

    }
}

// import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
// import { Observable } from 'rxjs';
// import { Injectable } from '@angular/core';
// import { tap } from 'rxjs/operators';
// import { Router } from '@angular/router';

// @Injectable()
// export class TokenInterceptor implements HttpInterceptor {

//     constructor(
//         private router: Router
//     ) { }

//     intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
//         let copiedReq = null;
//         let currentUser = JSON.parse(localStorage.getItem('AlghanimCurrentUser'));
//         if (currentUser && currentUser.data.token) {
//             copiedReq = req.clone({
//                 setHeaders: {
//                     'Authorization': `Bearer ${currentUser.data.token}`,
//                     'Content-Type': 'application/json',
//                     'Access-Control-Allow-Origin': '*'
//                 }
//             });
//         } else {
//             copiedReq = req.clone({
//                 setHeaders: {
//                     'Content-Type': 'application/json',
//                     'Access-Control-Allow-Origin': '*'
//                 }
//             });
//         }
//         console.log(copiedReq);
//         return next.handle(copiedReq).pipe(
//             tap((ev: HttpEvent<any>) => { console.log(ev) },
//                 error => {
//                     if (error.status == '401' || error.status == '402' || error.status == '403') {
//                         localStorage.clear();
//                         this.router.navigate(['/login']);
//                         console.error(error);
//                     }
//                 }
//             ));
//     }
// }

