import { Component, OnInit, Input, ViewEncapsulation, OnDestroy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LookupService } from "../../../api-module/services/lookup-services/lookup.service";
import { Subscription, Observable } from "rxjs";
import { UtilitiesService } from "../../../api-module/services/utilities/utilities.service";
import { MessageService } from 'primeng/components/common/messageservice';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-languageModel',
  templateUrl: './languageModel.component.html',
  styleUrls: ['./languageModel.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class languagesComponent implements OnInit, OnDestroy {
  @Input() header;
  @Input() type;
  @Input() data: any;
  statesSubscriptions: Subscription;
  allLanguages: any = [];
  langName: string = '';
  fK_SupportedLanguages_ID: any;
  langData: any = [];
  startDate: any = [Date];
  endDate: any = [Date];
  day: any = [];
  areaLangObj: any = {};
  toggleLoading: boolean;


  constructor(public activeModal: NgbActiveModal,
    private lookup: LookupService,
    private messageService: MessageService,
    private utilities: UtilitiesService,
    private translateService: TranslateService) { }

  ngOnInit() {
    this.getAllLanguages();

  }

  ngOnDestroy() {
    this.statesSubscriptions && this.statesSubscriptions.unsubscribe();
  }

  ngAfterContentInit() {
    this.getAllAreaLanguages();
  }

  getAllLanguages() {
    this.lookup.getlanguages()
      .subscribe(
        result => {
          this.allLanguages = result.data;
        }, error => { }
      );
  }

  getAllAreaLanguages() {
    this.toggleLoading = true;
    if (this.type == 'Area') {
      this.lookup.getAreaLangById(this.data.id)
        .subscribe(
          result => {
            this.areaLangObj = result.data;
            this.langData = [];
            this.initiateLanguages(result.data, this.data.id);
            this.toggleLoading = false;
          }, error => {
          }
        );
    }
    if (this.type == 'Attendance Status') {
      this.lookup.getAttendanceStatesById(this.data.id)
        .subscribe(
          result => {
            this.areaLangObj = result.data;
            this.langData = [];
            this.initiateLanguages(result.data, this.data.id);
            this.toggleLoading = false;
          }, error => {
          }
        );
    }
    //new 
    if (this.type == 'Order Status') {
      this.lookup.getOrderStatusById(this.data.id)
        .subscribe(
          result => {
            this.areaLangObj = result.data;
            this.langData = [];
            this.initiateLanguages(result.data, this.data.id);
            this.toggleLoading = false;
          }, error => {
          }
        );
    }
    //
    if (this.type == 'Availability') {
      this.lookup.getAvailabilityById(this.data.id)
        .subscribe(
          result => {
            this.areaLangObj = result.data;
            this.langData = [];
            this.initiateLanguages(result.data, this.data.id);
            this.toggleLoading = false;
          }, error => {
          }
        );
    }
    if (this.type == 'Building Types') {
      this.lookup.getBuildingTypesById(this.data.id)
        .subscribe(
          result => {
            this.areaLangObj = result.data;
            this.langData = [];
            this.initiateLanguages(result.data, this.data.id);
            this.toggleLoading = false;
          },
          error => {
          }
        );
    }
    if (this.type == 'Company Code') {
      this.lookup.getCompanyCodeById(this.data.id)
        .subscribe(
          result => {
            this.areaLangObj = result.data;
            this.langData = [];
            this.initiateLanguages(result.data, this.data.id);
            this.toggleLoading = false;
          },
          error => {
          }
        );
    }
    if (this.type == 'Contract Type') {
      this.lookup.getContractTypeById(this.data.id)
        .subscribe(
          result => {
            this.areaLangObj = result.data;
            this.langData = [];
            this.initiateLanguages(result.data, this.data.id);
            this.toggleLoading = false;
          },
          error => {
          }
        );
    }
    if (this.type == 'Cost Center') {
      this.lookup.getCostCenterById(this.data.id)
        .subscribe(
          result => {
            this.areaLangObj = result.data;
            this.langData = [];
            this.initiateLanguages(result.data, this.data.id);
            this.toggleLoading = false;
          },
          error => {
          }
        );
    }
    if (this.type == 'Division') {
      this.lookup.getDivisionById(this.data.id)
        .subscribe(
          result => {
            this.areaLangObj = result.data;
            this.langData = [];
            this.initiateLanguages(result.data, this.data.id);
            this.toggleLoading = false;
          },
          error => {
          }
        );
    }
    if (this.type == 'Governorates') {
      this.lookup.getGovernoratesById(this.data.id)
        .subscribe(
          result => {
            this.areaLangObj = result.data;
            this.langData = [];
            this.initiateLanguages(result.data, this.data.id);
            this.toggleLoading = false;
          }, error => {
          }
        );
    }
    if (this.type == 'Shift') {
      this.lookup.getShiftById(this.data.id)
        .subscribe(
          result => {
            this.areaLangObj = result.data;
            this.langData = [];
            this.initiateLanguages(result.data, this.data.id);
            this.toggleLoading = false;
          },
          error => {
          }
        );
    }

  }

  initiateLanguages(objs, objId) {
    for (let i = 0; i < this.allLanguages.length; i++) {
      if (this.type == 'Area') {
        this.langData.push({
          fK_Area_ID: objId,
          fK_SupportedLanguages_ID: this.allLanguages[i].id,
          id: "",
          name: ""
        });
      }
      if (this.type == 'Attendance Status') {
        this.langData.push({
          fK_AttendanceStates_ID: objId,
          fK_SupportedLanguages_ID: this.allLanguages[i].id,
          id: "",
          name: ""
        });
      }
      //OrderStatus
      if (this.type == 'Order Status') {
        this.langData.push({
          FK_OrderStatus_ID: objId,
          fK_SupportedLanguages_ID: this.allLanguages[i].id,
          id: "",
          name: ""
        });
      }
      //
      if (this.type == 'Availability') {
        this.langData.push({
          fK_Availability_ID: objId,
          fK_SupportedLanguages_ID: this.allLanguages[i].id,
          id: "",
          name: ""
        });
      }
      if (this.type == 'Building Types') {
        this.langData.push({
          fK_BuildingTypes_ID: objId,
          fK_SupportedLanguages_ID: this.allLanguages[i].id,
          id: "",
          name: ""
        });
      }
      if (this.type == 'Company Code') {
        this.langData.push({
          fK_CompanyCode_ID: objId,
          fK_SupportedLanguages_ID: this.allLanguages[i].id,
          id: "",
          name: ""
        });
      }
      if (this.type == 'Contract Type') {
        this.langData.push({
          fK_ContractTypes_ID: objId,
          fK_SupportedLanguages_ID: this.allLanguages[i].id,
          id: "",
          name: ""
        });
      }
      if (this.type == 'Cost Center') {
        this.langData.push({
          fK_CostCenter_ID: objId,
          fK_SupportedLanguages_ID: this.allLanguages[i].id,
          id: "",
          name: ""
        });
      }
      if (this.type == 'Division') {
        this.langData.push({
          fK_CostCenter_ID: objId,
          fK_SupportedLanguages_ID: this.allLanguages[i].id,
          id: "",
          name: ""
        });
      }
      if (this.type == 'Governorates') {
        this.langData.push({
          fK_Governorates_ID: objId,
          fK_SupportedLanguages_ID: this.allLanguages[i].id,
          id: "",
          name: ""
        });
      }
      if (this.type == 'Shift') {
        this.langData.push({
          fK_Shift_ID: objId,
          fK_SupportedLanguages_ID: this.allLanguages[i].id,
          day: "",
        });
      }
    }
    if (objs && objs.length) {
      for (let i = 0; i < objs.length; i++) {
        for (let j = 0; j < this.langData.length; j++) {
          if (this.langData[j].fK_SupportedLanguages_ID == objs[i].fK_SupportedLanguages_ID) {
            this.langData[j].id = objs[i].id;
            this.langData[j].name = objs[i].name;
            if (this.type == 'Shift') {
              this.langData[j].day = objs[i].day
            }
          }
        }
      }
    }

  }

  close() {
    let editedLungsObj = [];
    let addedLangsObj = [];
    for (let i = 0; i < this.langData.length; i++) {
      if (this.type == 'Area') {
        if (this.langData[i].id) {
          editedLungsObj.push({
            fK_Area_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            id: this.langData[i].id,
            name: this.langData[i].name,
          });
        }
        else {
          addedLangsObj.push({
            fK_Area_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            name: this.langData[i].name
          });
        }
      }
      if (this.type == 'Attendance Status') {
        if (this.langData[i].id) {
          editedLungsObj.push({
            fK_AttendanceStates_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            id: this.langData[i].id,
            name: this.langData[i].name,
          });
        }
        else {
          addedLangsObj.push({
            fK_AttendanceStates_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            name: this.langData[i].name
          });
        }
      }
      ///OrderStatus
      if (this.type == 'Order Status') {
        if (this.langData[i].id) {
          editedLungsObj.push({
            FK_OrderStatus_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            id: this.langData[i].id,
            name: this.langData[i].name,
          });
        }
        else {
          addedLangsObj.push({
            FK_OrderStatus_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            name: this.langData[i].name
          });
        }
      }
      if (this.type == 'Availability') {
        if (this.langData[i].id) {
          editedLungsObj.push({
            fK_Availability_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            id: this.langData[i].id,
            name: this.langData[i].name,
          });
        }
        else {
          addedLangsObj.push({
            fK_Availability_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            name: this.langData[i].name
          });
        }
      }
      if (this.type == 'Building Types') {
        if (this.langData[i].id) {
          editedLungsObj.push({
            fK_BuildingTypes_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            id: this.langData[i].id,
            name: this.langData[i].name,
          });
        }
        else {
          addedLangsObj.push({
            fK_BuildingTypes_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            name: this.langData[i].name
          });
        }
      }
      if (this.type == 'Company Code') {
        if (this.langData[i].id) {
          editedLungsObj.push({
            fK_CompanyCode_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            id: this.langData[i].id,
            name: this.langData[i].name,
          });
        }
        else {
          addedLangsObj.push({
            fK_CompanyCode_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            name: this.langData[i].name
          });
        }
      }
      if (this.type == 'Contract Type') {
        if (this.langData[i].id) {
          editedLungsObj.push({
            fK_ContractTypes_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            id: this.langData[i].id,
            name: this.langData[i].name,
          });
        }
        else {
          addedLangsObj.push({
            fK_ContractTypes_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            name: this.langData[i].name
          });
        }
      }
      if (this.type == 'Cost Center') {
        if (this.langData[i].id) {
          editedLungsObj.push({
            fK_CostCenter_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            id: this.langData[i].id,
            name: this.langData[i].name,
          });
        }
        else {
          addedLangsObj.push({
            fK_CostCenter_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            name: this.langData[i].name
          });
        }
      }
      if (this.type == 'Division') {
        if (this.langData[i].id) {
          editedLungsObj.push({
            fK_Division_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            id: this.langData[i].id,
            name: this.langData[i].name,
          });
        }
        else {
          addedLangsObj.push({
            fK_Division_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            name: this.langData[i].name
          });
        }
      }
      if (this.type == 'Governorates') {
        if (this.langData[i].id) {
          editedLungsObj.push({
            fK_Governorates_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            id: this.langData[i].id,
            name: this.langData[i].name
          });
        }
        else {
          addedLangsObj.push({
            fK_Governorates_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            name: this.langData[i].name
          });
        }
      }
      if (this.type == 'Shift') {
        if (this.langData[i].id) {
          editedLungsObj.push({
            fK_Shift_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            id: this.langData[i].id,
            day: this.langData[i].day,

          });
        }
        else {
          addedLangsObj.push({
            fK_Shift_ID: this.data.id,
            fK_SupportedLanguages_ID: this.langData[i].fK_SupportedLanguages_ID,
            day: this.langData[i].day
          });
        }
      }
    }

    let request: Observable<any>;

    if (this.type == 'Area') {
      if (editedLungsObj && editedLungsObj.length) {
        request = this.lookup.editAreaLanguages(editedLungsObj);
      } else if (addedLangsObj && addedLangsObj.length) {
        request = this.lookup.postAreaLanguages(addedLangsObj);
      }
    }
    if (this.type == 'Attendance Status') {
      if (editedLungsObj && editedLungsObj.length) {
        request = this.lookup.editAttendanceStatesLanguages(editedLungsObj)

      }
      else if (addedLangsObj && addedLangsObj.length) {
        request = this.lookup.postAttendanceStatesLanguages(addedLangsObj);
      }
    }



    //OrderStatus
    if (this.type == 'Order Status') {
      if (editedLungsObj && editedLungsObj.length) {
        request = this.lookup.editOrderStatusLanguages(editedLungsObj)
      }
      ///AddOrderStatusLanguage
      else if (addedLangsObj && addedLangsObj.length) {
        request = this.lookup.postOrderStatusLanguages(addedLangsObj)
      }
    }
    if (this.type == 'Availability') {
      if (editedLungsObj && editedLungsObj.length) {
        request = this.lookup.editAvailabilityLanguages(editedLungsObj)

      } else if (addedLangsObj && addedLangsObj.length) {
        request = this.lookup.postAvailabilityLanguage(addedLangsObj)
      }
    }
    if (this.type == 'Building Types') {
      if (editedLungsObj && editedLungsObj.length) {
        request = this.lookup.editBuildingTypesLanguages(editedLungsObj)
      } else if (addedLangsObj && addedLangsObj.length) {
        request = this.lookup.postBuildingTypesLanguage(addedLangsObj)
      }
    }
    if (this.type == 'Company Code') {
      if (editedLungsObj && editedLungsObj.length) {
        request = this.lookup.editCompanyCodeLanguages(editedLungsObj)

      } else if (addedLangsObj && addedLangsObj.length) {
        request = this.lookup.postCompanyCodeLanguage(addedLangsObj)
      }
    }
    if (this.type == 'Contract Type') {
      if (editedLungsObj && editedLungsObj.length) {
        request = this.lookup.editContractTypesLanguages(editedLungsObj)

      } else if (addedLangsObj && addedLangsObj.length) {
        request = this.lookup.postContractTypesLanguage(addedLangsObj)

      }
    }
    if (this.type == 'Cost Center') {
      if (editedLungsObj && editedLungsObj.length) {
        request = this.lookup.editCostCenterLanguages(editedLungsObj)

      } else if (addedLangsObj && addedLangsObj.length) {
        request = this.lookup.postCostCenterLanguage(addedLangsObj)

      }
    }
    if (this.type == 'Division') {
      if (editedLungsObj && editedLungsObj.length) {
        request = this.lookup.editDivisionLanguages(editedLungsObj)

      } else if (addedLangsObj && addedLangsObj.length) {
        request = this.lookup.postDivisionLanguage(addedLangsObj)

      }
    }
    if (this.type == 'Governorates') {
      if (editedLungsObj && editedLungsObj.length) {
        request = this.lookup.editGovernoratesLanguages(editedLungsObj)

      } else if (addedLangsObj && addedLangsObj.length) {
        request = this.lookup.postGovernoratesLanguage(addedLangsObj)

      }
    }
    if (this.type == 'Shift') {
      if (editedLungsObj && editedLungsObj.length) {
        request = this.lookup.editShiftLanguages(editedLungsObj)

      } else if (addedLangsObj && addedLangsObj.length) {
        request = this.lookup.postShiftLanguage(addedLangsObj)
      }
    }

    request.subscribe(
      (data) => {
        this.savedSuccessfully();
      },
      err => {
        err.status == 401 && this.utilities.unauthrizedAction();
        this.networkError();
      }
    );




    this.activeModal.close();
  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  networkError() {
    this.messageService.add({
      severity: 'error',
      summary: this.translateService.instant('Failed!'),
      detail: this.translateService.instant("Failed to update due to network error")
    });
  }
  unValidDataError() {
    this.messageService.add({
      severity: 'error',
      summary: this.translateService.instant('Failed!'),
      detail: this.translateService.instant("Un valid Input Data")
    });
  }

  savedSuccessfully() {
    this.messageService.add({
      severity: 'success',
      summary: this.translateService.instant('Successful!'),
      detail: this.translateService.instant("New value saved Successfully! ")
    });
  }
}
