import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { LookupService } from '../../../api-module/services/lookup-services/lookup.service';
import { MessageService } from 'primeng/primeng';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NotificationModalComponent } from '../notification-modal/notification-modal.component';
import { SharedNotificationService } from '../shared-notification.service';
import { Subscription } from 'rxjs';
import { DataView } from 'primeng/dataview';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-notification-list',
  templateUrl: './notification-list.component.html',
  styleUrls: ['./notification-list.component.css']
})
export class NotificationListComponent implements OnInit, OnDestroy {
  i: any;
  seletetdTecs: any;
  totalRecords: number = 0;
  isLoading: boolean = false;
  notifications: any = [];
  notificationObj: any = {};
  filterObj: any = {
    startDate: null,
    endDate: null
  };
  userId: string = "";
  allTecs = [];
  sub: Subscription;
  seletetdTecsNew: Array<any> = [];
  selectedTechsView: Array<any> = [];
  @ViewChild('dataView', { static: false }) dataView: DataView;

  constructor(
    public sharedNotificationService: SharedNotificationService,
    private lookupService: LookupService,
    private messageService: MessageService,
    private modalService: NgbModal,
    private translateService: TranslateService) { }

  ngOnInit() {
    let AlghanimCurrentUser = localStorage.getItem("AlghanimCurrentUser");
    if (AlghanimCurrentUser) {
      this.userId = JSON.parse(AlghanimCurrentUser).data.userId;
    }
    this.notificationObj = {
      recieverId: this.userId,
      isRead: null,
      pageInfo: {
        pageNo: 1,
        pageSize: 75
      }
    };
    this.showNotificationbetweenDates();
    this.sub = this.sharedNotificationService.notificationCountSubject
      .subscribe(
        (count) => this.showNotificationbetweenDates()
      );
    this.getAllTecs();
  }

  onServerFailed(err) {
    this.isLoading = false;
    console.error(err);
    this.messageService.add({
      severity: 'error',
      summary: this.translateService.instant('error'),
      detail: this.translateService.instant('Failed due to server error!')
    });
  }

  getAllTecs() {
    //Added By Shaimaa Sayed  
    //
    //this.lookupService.getAllTechnicians()
    this.lookupService.getAllAssignedTechnicians()
      .subscribe(
        (res: any) => {
          this.allTecs = res.data;
          console.log(this.allTecs.length)
        },
        err => {
          this.onServerFailed(err);
        }
      );

  }

  // changeTecs(tecs) {
  //   //Added By Shaimaa Sayed  
  //   this.seletetdTecsNew.push(tecs.itemValue.pf);

  // }
  // changeTecs(techsArray) {
  //   console.log(techsArray.itemValue);
  //   this.filterObj.teamId = [];
  //   this.filterObj.techs = [];
  //   techsArray.itemValue.map((techObj: any) => {
  //     this.filterObj.techs.push(techsArray.itemValue.pf);
  //     if (techObj.pf && this.filterObj.pf.indexOf(techObj.pf) < 0) {
  //       this.filterObj.teamId.push(techObj.pf);
  //     }
  //   });
  //   console.log(this.filterObj);
  // }
  changeTecs(techsArray) {
    console.log(techsArray.value);
    this.filterObj.teamId = [];
    this.filterObj.techs = [];
    this.filterObj.pf = [];
    techsArray.value.map((techObj: any) => {
      this.filterObj.techs.push("" + techObj.id);
      if (techObj.pf && this.filterObj.pf.indexOf(techObj.pf) < 0) {
        this.filterObj.pf.push(techObj.pf);
      }
    });
    console.log(this.filterObj);
  }

  loadData(e) {
    this.notificationObj.pageInfo.pageNo = (e.first / 75) + 1;
    this.showNotificationbetweenDates();
  }

  showNotificationbetweenDates() {
    // debugger;
    this.isLoading = true;
    //Added By Shaimaa Sayed
    let variance = 60 * 60 * 1000 * 9;
    let start = new Date((new Date(this.filterObj.startDate)).getTime());
    let end = new Date((new Date(this.filterObj.endDate)).getTime());
    // let Obj: any = Object.assign({}, this.filterObj);
    // var timeZoneShift = new Date().getTimezoneOffset() / (-60);
    // var dt = new Date(Obj.endDate);
    // dt.setHours(dt.getHours() + timeZoneShift);
    // Obj.endDate = dt;

    // var df = new Date(Obj.startDate);
    // df.setHours(df.getHours() + timeZoneShift);
    // Obj.startDate = df;
    this.notificationObj.tecs = this.filterObj.pf;
    this.notificationObj.orderCode = this.filterObj.OrderCode;
    this.notificationObj.startDate = start;
    this.notificationObj.endDate = end;
    this.notificationObj.recieverId = this.userId;
    if (this.notificationObj.startDate.getFullYear() == 1970 && this.notificationObj.endDate.getFullYear() == 1970) {
      this.notificationObj.startDate = null;
      this.notificationObj.endDate = null;
    }

    this.lookupService.postNotificationCenterFilteredByDate(this.notificationObj)
      .subscribe(
        (response) => {
          if (response && response.isSucceeded) {
            console.log(response.data.data);
            this.notifications = response.data.data;
            this.totalRecords = response.data.count;
            this.isLoading = false;
          } else {
            this.onServerFailed(response);
          }
        },
        err => {
          this.onServerFailed(err);
        }
      );
    console.log(this.notificationObj);
  }

  showNotificationbetweenDatesAndResetPaging() {

    this.notificationObj.pageInfo.pageNo = 1;
    if (new Date(this.filterObj.startDate).getTime() <= new Date(this.filterObj.endDate).getTime()) {
      //if (this.filterObj.startDate >=this.filterObj.endDate) {
      this.showNotificationbetweenDates();
    } else {
      this.messageService.add({
        severity: 'error',
        summary: this.translateService.instant('invalid dates!'),
        detail: this.translateService.instant('Start Date should be before End Date!')
      });
    }
  }

  onResetDates() {
    this.filterObj.startDate = null;
    this.filterObj.endDate = null;
    this.filterObj.OrderCode = "";
    this.seletetdTecsNew = [];
    this.selectedTechsView = [];
    this.filterObj.pf = []
    this.filterObj.teamId = [];
    this.filterObj.techs = [];
    this.filterObj.pf = [];
    this.onResetPagination();
  }

  onResetPagination() {
    this.dataView.paginate(
      {
        first: 0,
        rows: 75,
        page: 1,
        // pageCount: 1 
      }
    );
  }

  onNotificationClick(notification) {
    if (notification.isRead) {
      let modalRef = this.modalService.open(NotificationModalComponent);
      modalRef.componentInstance.data = notification;
    } else {
      this.isLoading = true;
      let obj = notification;
      obj.isRead = true;
      this.lookupService.ChangeReadFlagNotificationCenterItem(obj.id)
        .subscribe(
          (response) => {
            this.sharedNotificationService.notificationCountSubject
              .next((this.sharedNotificationService.notificationCount - 1));
            if (response && response.isSucceeded) {
              let modalRef = this.modalService.open(NotificationModalComponent);
              modalRef.componentInstance.data = notification;
              this.isLoading = false;
            } else {
              this.onServerFailed(response);
            }
          },
          err => {
            this.onServerFailed(err);
          }
        );
    }
  }


  uponApprovalAction(obj, index, accept) {
    this.lookupService.ChangeActionFlagNotificationCenterItem(obj.id)
      .subscribe(
        (ChangeActionFlagNotificationCenterItemResponse) => {
          this.notifications[index].isActionTaken = true;
          if (ChangeActionFlagNotificationCenterItemResponse && ChangeActionFlagNotificationCenterItemResponse.isSucceeded) {
            this.isLoading = false;
            if (accept) {
              this.sharedNotificationService.acceptChangeRankSubject.next();
            }
            this.sharedNotificationService.notificationCountSubject
              .next((this.sharedNotificationService.notificationCount - 1));
          } else {
            this.onServerFailed(ChangeActionFlagNotificationCenterItemResponse);
          }
        },
        err => {
          this.onServerFailed(err);
        }
      );
  }

  onApproveRequest(e, notification: any, index: number, acceptChangeValue: boolean) {
    e.stopPropagation();
    this.isLoading = true;
    let orders = JSON.parse(notification.data);
    var accept = acceptChangeValue;
    this.lookupService.putNotificationCenterChangeTeamOrderRank(acceptChangeValue, orders)
      .subscribe(
        (response) => {
          if (response && response.isSucceeded) {
            if (!notification.isRead) {
              let obj = notification;
              obj.isRead = true;
              this.uponApprovalAction(obj, index, accept);
            } else {
              this.isLoading = false;
            }
          } else {
            if (response && response.status && response.status.code && !response.isSucceeded && response.status.code == 6) {
              this.isLoading = false;
              console.error(response);
              this.notifications[index].isActionTaken = true;
              this.notifications[index].isRead = true;
              this.messageService.add({
                severity: 'error',
                summary: this.translateService.instant('error'),
                detail: this.translateService.instant('Unable to change rank! Please ask the technnician to resend the request.')
              });
              let obj = notification;
              this.uponApprovalAction(obj, index, accept);
            } else {
              this.onServerFailed(response);
            }
          }
        },
        err => {
          this.onServerFailed(err);
        }
      );
  }

  onDeleteNotification(e: any, notification: any, index: number) {
    e.stopPropagation();
    this.isLoading = true;
    this.lookupService.deleteNotificationCenter(notification.id)
      .subscribe(
        (response) => {
          this.sharedNotificationService.notificationCountSubject
            .next((this.sharedNotificationService.notificationCount - 1));
          if (response && response.isSucceeded) {
            this.messageService.add({
              severity: 'success',
              summary: this.translateService.instant('deleted successfully!'),
              detail: this.translateService.instant('deleted successfully!')
            });
            this.notifications.splice(index, 1);
            this.isLoading = false;
          } else {
            this.onServerFailed(response);
          }
        },
        err => {
          this.onServerFailed(err);
        }
      );
  }

  onUpdateNotification(e: any, notification: any, index: number) {
    e.stopPropagation();
    this.isLoading = true;
    let obj = Object.assign({}, notification);
    obj.isRead = true;
    this.lookupService.ChangeReadFlagNotificationCenterItem(obj.id)
      .subscribe(
        (response) => {
          this.sharedNotificationService.notificationCountSubject
            .next((this.sharedNotificationService.notificationCount - 1));
          if (response && response.isSucceeded) {
            this.messageService.add({
              severity: 'success',
              summary: this.translateService.instant('updated successfully!'),
              detail: this.translateService.instant('updated successfully!')
            });
            notification.isRead = !notification.isRead;
            this.isLoading = false;
          } else {
            this.onServerFailed(response);
          }
        }
        , err => {
          this.onServerFailed(err);
        }
      );
  }


  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
