import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AlertServiceService } from './../api-module/services/alertservice/alert-service.service';
import { AlertComponentComponent } from './shared/alert-component/alert-component.component';
import { FooterComponentComponent } from './footer-component/footer-component.component';
import { MainHeaderComponentComponent } from './main-header-component/main-header-component.component';
import { ApiModuleModule } from './../api-module/api-module.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModuleWithProviders, Type } from '@angular/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DataViewModule } from 'primeng/dataview';
import { SplitButtonModule } from 'primeng/components/splitbutton/splitbutton';
import { CalendarModule } from 'primeng/components/calendar/calendar';
import { AccordionModule } from 'primeng/components/accordion/accordion';
import { MessagesModule } from 'primeng/components/messages/messages';
import { MessageModule } from 'primeng/components/message/message';
import { FileUploadModule } from 'primeng/components/fileupload/fileupload';
import { ProgressSpinnerModule } from 'primeng/components/progressspinner/progressspinner';
import { TooltipModule } from 'primeng/components/tooltip/tooltip';
import { InputSwitchModule } from 'primeng/components/inputswitch/inputswitch';
import { DropdownModule } from 'primeng/components/dropdown/dropdown';
import { MultiSelectModule } from 'primeng/components/multiselect/multiselect';
import { SelectButtonModule } from 'primeng/components/selectbutton/selectbutton';
import { DragulaModule } from "ng2-dragula";
import { DataTableModule } from 'primeng/components/datatable/datatable';
import { SharedModule } from 'primeng/components/common/shared';
import { DateFromToComponent } from './date-from-to/date-from-to.component';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { TabViewModule } from 'primeng/tabview';
import { NotificationModalComponent } from './shared/notification-modal/notification-modal.component';
import { NotificationListComponent } from './shared/notification-list/notification-list.component';
import { ListboxModule } from 'primeng/listbox';
import { AngularFireModule } from '@angular/fire';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { environment } from '../../environments/environment';
import { PageTitleComponent } from './shared/page-title/page-title.component';
import { HoursToHoursMinutesSecondsPipe } from './shared/hours-to-hours-minutes-seconds.pipe';
import { ToastModule } from 'primeng/toast';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { KeyFilterModule } from 'primeng/keyfilter';
import { InputRestrictionDirective } from './input-restriction.directive';
import { SidebarModule, ScheduleModule, CheckboxModule, RadioButtonModule } from 'primeng/primeng';
import { GrowlModule } from 'primeng/primeng';
import { OrderFilterComponent } from '../dispatcher/order-filter/order-filter.component';
import { TranslateModule } from '@ngx-translate/core';
import { MatMenuModule } from '@angular/material/menu';
import { ConfirmationDialog } from './confirm-dialog-component/confirm-dialog-component';
import { NoSpacesAllowedDirective } from './no-space-allowed.directive';
import { PreventClickByEnterDirective } from './prevent-click-by-enter.directive';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    DragulaModule,
    ApiModuleModule,
    FormsModule,
    NgxDatatableModule,
    TooltipModule,
    SplitButtonModule,
    CalendarModule,
    GrowlModule,
    AccordionModule,
    MessagesModule,
    MessageModule,
    FileUploadModule,
    ProgressSpinnerModule,
    InputSwitchModule,
    DropdownModule,
    DataTableModule, SharedModule,
    MultiSelectModule,
    SelectButtonModule,
    ToggleButtonModule,
    TabViewModule,
    DataViewModule,
    ListboxModule,
    ToastModule,
    CheckboxModule,
    RadioButtonModule,
    NgbModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireMessagingModule,
    ScrollingModule,
    InfiniteScrollModule,
    KeyFilterModule,
    SidebarModule,
    ScheduleModule,
    TranslateModule,
    MatMenuModule
  ],
  declarations: [
    MainHeaderComponentComponent,
    FooterComponentComponent,
    AlertComponentComponent,
    DateFromToComponent,
    ConfirmationDialog,
    NotificationModalComponent,
    NotificationListComponent,
    PageTitleComponent,
    HoursToHoursMinutesSecondsPipe,
    InputRestrictionDirective,
    OrderFilterComponent,
    NoSpacesAllowedDirective,
    PreventClickByEnterDirective,
  ],
  exports: [
    CommonModule,
    ReactiveFormsModule,
    ListboxModule,
    ToggleButtonModule,
    MainHeaderComponentComponent,
    FooterComponentComponent,
    AlertComponentComponent,
    NgxDatatableModule,
    NgbModule,
    ApiModuleModule,
    TooltipModule,
    SplitButtonModule,
    CalendarModule,
    GrowlModule,
    AccordionModule,
    MessagesModule,
    MessageModule,
    FileUploadModule,
    ProgressSpinnerModule,
    DragulaModule,
    FormsModule,
    InputSwitchModule,
    DataTableModule, SharedModule,
    DropdownModule,
    MultiSelectModule,
    DateFromToComponent,
    ConfirmationDialog,
    SelectButtonModule,
    NotificationModalComponent,
    DataViewModule,
    HoursToHoursMinutesSecondsPipe,
    ToastModule,
    CheckboxModule,
    RadioButtonModule,
    AngularFireModule,
    AngularFireMessagingModule,
    PageTitleComponent,
    ScrollingModule,
    InfiniteScrollModule,
    KeyFilterModule,
    InputRestrictionDirective,
    SidebarModule,
    ScheduleModule,
    OrderFilterComponent,
    NoSpacesAllowedDirective,
    PreventClickByEnterDirective,
  ],
  entryComponents: [
    NotificationModalComponent,
    ConfirmationDialog,
  ]
})

export class SharedModuleModule {

  static forRoot(entryComponents?: Array<Type<any> | any[]>): ModuleWithProviders {
    return {
      ngModule: SharedModuleModule,
      providers: [
        AlertServiceService,
      ]
    };
  }
}

export const rootShared: ModuleWithProviders = SharedModuleModule.forRoot();

