import { Router } from '@angular/router';
import { AuthenticationServicesService } from './../../api-module/services/authentication/authentication-services.service';
import { Component, OnInit, ChangeDetectorRef, AfterViewInit, AfterViewChecked } from '@angular/core';
import { Subscription } from "rxjs";
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import { LookupService } from '../../api-module/services/lookup-services/lookup.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NotificationModalComponent } from '../shared/notification-modal/notification-modal.component';
import { MessageService } from 'primeng/primeng';
import { SharedNotificationService } from '../shared/shared-notification.service';
import { delay } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-main-header-component',
  templateUrl: './main-header-component.component.html',
  styleUrls: ['./main-header-component.component.css']
})

export class MainHeaderComponentComponent implements OnInit, AfterViewInit, AfterViewChecked {
  isLoggedin: boolean = false;
  isloading: boolean = false;
  toggleNotification: boolean;
  newNotificationToggle: boolean;
  notificationBackgroundClass: any[];
  CurentUser: any = {};
  toggelingNotificationIconInterval: any;
  quotationSubscription: Subscription;
  callSubscription: Subscription;
  orderSubscription: Subscription;
  RoleSubscription: Subscription;
  notifications: any[] = [];
  toggelingNotificationIconClass: string[] = [];
  userId = "";
  role: any = "";

  finishLoggingOut: boolean;
  items: MenuItem[];

  constructor(
    public sharedNotificationService: SharedNotificationService,
    private lookupService: LookupService,
    private messageService: MessageService,
    private cdRef: ChangeDetectorRef,
    public authenticationService: AuthenticationServicesService,
    private router: Router,
    private utilities: UtilitiesService,
    private modalService: NgbModal,
    public translateService: TranslateService) {
  }

  ngOnInit() {
    this.getNotifications();
    this.authenticationService.loadNotification
      .subscribe(
        res => {
          this.getNotifications();
        }
      );

    this.sharedNotificationService.notificationCountSubject
      .subscribe(
        (count) => {
          this.isloading=true;
          this.sharedNotificationService.notificationCount = count;
          this.lookupService.GetNotificationCenterCountByRecieverId(this.userId)
            .subscribe(
              (response) => {
                if (response && response.isSucceeded) {
                  this.sharedNotificationService.notificationCount = response.data;
                  this.getNotifications();
                }
              }
            );
        }
      );

    this.authenticationService.isLoggedin
      .subscribe(
        loggedIn => {
          this.isLoggedin = loggedIn;
        }
      );
    this.CurentUser = JSON.parse(localStorage.getItem('AlghanimCurrentUser'));
    this.notificationBackgroundClass = ['hide-notification-transparent-background'];
    this.quotationSubscription = this.authenticationService.quotaionSignalR
      .subscribe(
        (quotation) => {
          quotation.type = 'quotation';
          this.notifications.push(quotation);
          this.newNotificationToggle = true;
          this.toggelingNotificationIcon();
        },
        (err) => {
          console.log(err)
        }
      );

    this.callSubscription = this.authenticationService.callSignalR
      .subscribe(
        (call) => {
          call.type = 'call';
          this.notifications.push(call);
          this.newNotificationToggle = true;
          this.toggelingNotificationIcon();
        },
        (err) => {
          console.log(err)
        }
      );

    this.RoleSubscription = this.authenticationService.userRoles.pipe(
      delay(0)
    ).subscribe(
      (role) => {
        this.role = role[0];
        if (this.role === "Admin") {
          this.sharedNotificationService.notificationCount = 0;
        }
      }, (err) => {
        console.log(err);
      }
    );

    this.orderSubscription = this.authenticationService.orderSignalR
      .subscribe(
        (order) => {
          this.notifications.push(order);
          this.newNotificationToggle = true;
          this.toggelingNotificationIcon();
        },
        (err) => {
          console.log(err);
        });

    window.addEventListener('storage', (event) => {
      if (event.storageArea == localStorage) {
        this.authenticationService.CurrentUser();
      }
    });
  }


  onServerFailed(err) {
    console.error(err);
    this.messageService.add({
      severity: 'error',
      summary: this.translateService.instant('error'),
      detail: this.translateService.instant('Failed due to server error!')
    });
  }

  profile() {
    this.router.navigate(['profile']);
  }

  getNotifications() {
    this.notifications = [];
    let AlghanimCurrentUser = localStorage.getItem("AlghanimCurrentUser");
    if (AlghanimCurrentUser) {
      this.userId = JSON.parse(AlghanimCurrentUser).data.userId;
    }

    if (this.userId) {
      this.isloading=true;
      //console.log("test",this.isloading);
      
      this.lookupService.GetNotificationCenterCountByRecieverId(this.userId)
        .subscribe(
          (response) => {
            if (response && response.isSucceeded) {
              this.sharedNotificationService.notificationCount = response.data;
              let notificationObj = {
                recieverId: this.userId,
                isRead: false,
                pageInfo: {
                  pageNo: 1,
                  pageSize: 5
                }
              };
      this.isloading=true;
//console.log("Test2",this.isloading);

              this.lookupService.postNotificationCenter(notificationObj)
                .subscribe(
                  (response) => {
                    this.notifications = response.data.data;
                    this.isloading=false;
                  }, err => this.onServerFailed(err)
                );
            } else {
              this.onServerFailed(response)
            }
          }, err => this.onServerFailed(err));
    }
  }


  uponApprovalAction(obj, index, accept) {
    this.lookupService.ChangeActionFlagNotificationCenterItem(obj.id)
      .subscribe(
        (ChangeActionFlagNotificationCenterItemResponse) => {
          this.notifications[index].isActionTaken = true;
          if (ChangeActionFlagNotificationCenterItemResponse && ChangeActionFlagNotificationCenterItemResponse.isSucceeded) {
            if (accept) {
              this.sharedNotificationService.acceptChangeRankSubject.next();
            }
            this.sharedNotificationService.notificationCountSubject
              .next((this.sharedNotificationService.notificationCount - 1));
          } else {
            this.onServerFailed(ChangeActionFlagNotificationCenterItemResponse);
          }
        },
        err => {
          this.onServerFailed(err);
        }
      );
  }

  onApproveRequest(e, notification: any, index: number, acceptChangeValue: boolean) {
    e.stopPropagation();
    var accept = acceptChangeValue;
    this.notifications[index].className = "wait";
    let orders = JSON.parse(notification.data);
    this.lookupService.putNotificationCenterChangeTeamOrderRank(acceptChangeValue, orders)
      .subscribe(
        (response) => {
          if (response && response.isSucceeded) {
            let obj = Object.assign({}, notification);
            obj.isRead = true;
            this.uponApprovalAction(obj, index, accept);
          } else {
            if (response && response.status && response.status.code && !response.isSucceeded && response.status.code == 6) {
              console.error(response);
              this.notifications[index].isActionTaken = true;
              this.notifications[index].isRead = true;
              this.messageService.add({
                severity: 'error',
                summary: this.translateService.instant('error'),
                detail: this.translateService.instant('Unable to change rank! Please ask the technnician to resend the request.')
              });
              let obj = notification;
              this.uponApprovalAction(obj, index, accept);
            } else {
              this.onServerFailed(response);
            }
          }
        }, err => this.onServerFailed(err)
      );
  }
  onNotificationClick(notificationData, index: number) {
    this.isloading = true;
    let obj = notificationData;
    obj.isRead = true;
    // this.lookupService.updateNotificationCenterItem(obj)
    this.lookupService.ChangeReadFlagNotificationCenterItem(obj.id)
      .subscribe(
        (response) => {
          if (response && response.isSucceeded) {
            this.sharedNotificationService.notificationCountSubject
              .next((this.sharedNotificationService.notificationCount));
            let modalRef = this.modalService.open(NotificationModalComponent);
            modalRef.componentInstance.data = notificationData;
            this.sharedNotificationService.notificationCount--;
            this.notifications.splice(index, 1);
            this.getNotifications();
            this.isloading = false;
            modalRef.result
              .then(
                () => {
                  this.toggleNotificationBody();
                }
              );
          }
        }
      );
  }

  onMarkAllAsRead() {
    //debugger
    this.isloading=true;
    this.notifications = [];
    let AlghanimCurrentUser = localStorage.getItem("AlghanimCurrentUser");
    if (AlghanimCurrentUser) {
      this.userId = JSON.parse(AlghanimCurrentUser).data.userId;
    }
    this.lookupService.MarkAllAsReadByRecieverIdURL(this.userId)
      .subscribe(
        (response) => {
          if (response && response.isSucceeded) {
            this.notifications = [];
            this.sharedNotificationService.notificationCount = 0;
            this.sharedNotificationService.notificationCountSubject.next(0);
            this.messageService.add({
              severity: 'success',
              summary: this.translateService.instant('Successful!'),
              detail: this.translateService.instant(`All marked as read`)
              
            })
           // console.log("hhh",this.notifications)
          }
        }
      );
  }
  // seriveCalled() {
  //   this.isloading = true;
  //   //this.numberOfServicesCalled++;
  // }

  LogoutAll() {
    if (!this.finishLoggingOut) {
      let userId = JSON.parse(localStorage.getItem("AlghanimCurrentUser")).data.userId;
      let deviceId = localStorage.getItem("deviceId")
      let obj = {
        userId: userId,
        deviceId: deviceId
      };
      this.finishLoggingOut = true;
      this.lookupService.logout(obj)
        .subscribe(
          (res: any) => {
            this.notifications = [];
            this.newNotificationToggle = false;
            localStorage.removeItem("deviceId");
            localStorage.removeItem("AlghanimCurrentUserId");
            this.toggelingNotificationIconClass = ['show-notification-icon'];
            this.stopIntervalNotificationIconToggeling();
            this.router.navigate(['/login']);
            this.finishLoggingOut = false;
          }, err => {
            console.error(err);
            this.messageService.add({
              severity: 'error',
              summary: this.translateService.instant('Failed'),
              detail: this.translateService.instant(`Failed to load data due to server error.`)
            });
            this.notifications = [];
            this.newNotificationToggle = false;
            localStorage.removeItem("deviceId");
            localStorage.removeItem("AlghanimCurrentUserId");
            this.toggelingNotificationIconClass = ['show-notification-icon'];
            this.stopIntervalNotificationIconToggeling();
            this.router.navigate(['/login']);
            this.finishLoggingOut = false;
          }
        );
    }
  }

  toggleNotificationBody() {
    this.toggleNotification = !this.toggleNotification;
    this.newNotificationToggle = false;
    this.toggelingNotificationIconClass = ['show-notification-icon'];
    this.stopIntervalNotificationIconToggeling();
    if (this.notificationBackgroundClass.toString() == ['show-notification-transparent-background'].toString()) {
      this.notificationBackgroundClass = ['hide-notification-transparent-background'];
    } else {
      this.notificationBackgroundClass = ['show-notification-transparent-background'];
    }
  }

  toggelingNotificationIcon() {
    this.toggelingNotificationIconInterval = setInterval(
      () => {
        if (this.newNotificationToggle) {
          if (this.toggelingNotificationIconClass.toString() == ['show-notification-icon'].toString()) {
            this.toggelingNotificationIconClass = ['hide-notification-icon'];
          } else {
            this.toggelingNotificationIconClass = ['show-notification-icon'];
          }
        } else {
          this.toggelingNotificationIconClass = ['show-notification-icon'];
        }
      }
      , 1000)
  }

  stopIntervalNotificationIconToggeling() {
    clearInterval(this.toggelingNotificationIconInterval);
  }

  ngAfterViewInit() {
    this.cdRef.detectChanges();
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges()
  }
}
