import { Component, OnInit, Input } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { LookupService } from "../../../../api-module/services/lookup-services/lookup.service";

import { SettingsDispatchersService } from "../settings-dispatchers.service";
import { MessageService } from "primeng/primeng";
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-edit-dis-and-prob-modal',
  templateUrl: './edit-dis-and-prob-modal.component.html',
  styleUrls: ['./edit-dis-and-prob-modal.component.css']
})
export class EditDisAndProbModalComponent implements OnInit {
  @Input() data;
  problems: any[];
  dispatcherDetailsFormated: any[] = [];
  areas: any[];
  governorates: any[];
  toggleLoading: boolean;
  newProblems: any[] = [];
  cornerMessage: any[];
  objToPost: any = {};
  objectdispatcher: any = [];
  dispatcherList: any = [];
  dispatcherObj: any = {};
  numberOfServicesCalled: number = 0;
  numberOfServicesFinished: number = 0;

  constructor(private activeModal: NgbActiveModal,
    private _lookup: LookupService,
    private settingsDispatchersService: SettingsDispatchersService,
    private messagseService: MessageService,
    private translateService: TranslateService) { }

  ngOnInit() {
    this.getAllProblems();
    this.getAllAreas();
    this.getDispatcherList();
    if (this.data) {
      this.dispatcherObj.id = this.data.dispatcherId;
      this.dispatcherObj.name = this.data.dispatcherName;
      if (this.data.area != '-') {
        this.getDispatcherAllProblems(this.dispatcherObj.id);
      }
    }
  }

  seriveCalled() {
    this.toggleLoading = true;
    this.numberOfServicesCalled++;
  }

  checkIfServicesFinished() {
    this.numberOfServicesFinished++;
    if (this.numberOfServicesFinished == this.numberOfServicesCalled) {
      this.toggleLoading = false;
    }
  }

  getDispatcherList() {
    this.seriveCalled();
    this._lookup.getAllDispatchers().subscribe(
      (data) => {
        this.checkIfServicesFinished();
        this.dispatcherList = data.data;
        this.dispatcherList.map(
          (dis, i) => {
            this.dispatcherList[i] = {
              label: dis.fullName,
              name: dis.name,
              id: dis.id
            }
          }
        );
      },
      err => {
        console.log(err)
      }
    );
  }

  addNewProblem() {
    this.newProblems.push({
      area: '',
      problems: []
    });
  }

  removeNewProblem(item) {
    let removedFlag: boolean;
    this.newProblems.map(
      (problem) => {
        if (JSON.stringify(item) == JSON.stringify(problem) && !removedFlag) {
          this.newProblems.splice(this.newProblems.indexOf(item), 1)
        }
      }
    );
  }

  removeProblem(item, index) {
    this.toggleLoading = true;
    item.areas = [];
    item.area = [];
    item.problems = [];
    item.orderProblems = [];
    let arrayToPost = [item];
    this._lookup.UpdateMulitWithAreasAndOrderProblems(arrayToPost)
      .subscribe(
        (res: any) => {
          this.toggleLoading = false;
          this.dispatcherDetailsFormated.splice(index, 1);
          this.settingsDispatchersService.closeModal.next();
        }
      );
  }

  getAllAreas() {
    this.seriveCalled();
    this._lookup.getALLareasEnUrl()
      .subscribe(
        (data) => {
          this.checkIfServicesFinished();
          this.areas = data.data;
          this.areas.map(
            (are, i) => {
              this.areas[i] = {
                label: are.name,
                value: are.name,
                id: are.id
              }
            }
          );
        },
        err => {
          console.log(err);
        }
      );
  }

  getAreas(item) {
    let usedItem = item;
    this._lookup.getAllAreasDataManagement()
      .subscribe(
        (data) => {
          this.toggleLoading = false;
          this.areas = data.data;
        },
        err => {
          console.log(err);
        }
      );
  }

  selectId(name, list) {
    return list.filter(
      (item) => {
        return item.name == name;
      }
    )[0].id;
  }

  getAllProblems() {
    this.seriveCalled();
    this._lookup.getAllOrderProblem()
      .subscribe(
        (problems) => {
          this.checkIfServicesFinished();
          this.problems = problems.data;
          this.problems.map(
            (prob, i) => {
              this.problems[i] = {
                label: prob.name,
                value: prob.name,
                id: prob.id
              }
            }
          );
        },
        err => {
          console.log(err);
        }
      );
  }

  getDispatcherAllProblems(id) {
    this.seriveCalled();
    this._lookup.GetMultiAreasAndProblemsbyDispatcherId(id).subscribe(
      (response) => {
        let results = response;
        this.dispatcherDetailsFormated = results;
        results.map(
          (item, i) => {
            this.dispatcherDetailsFormated[i].problems = [];
            item.orderProblems.map(
              (prob) => {
                this.dispatcherDetailsFormated[i].problems.push(prob.problemName)
              }
            );
            this.dispatcherDetailsFormated[i].area = [];
            item.areas.map(
              (area) => {
                this.dispatcherDetailsFormated[i].area.push(area.areaName);
              }
            );
          }
        );
        this.checkIfServicesFinished();
      }
    );
  }

  onSelectDispatecher(dis) {
    this.dispatcherObj = dis.value;
  }


  applyEdits() {
    this.toggleLoading = true;
    let allViewProblemsAndAreas = this.dispatcherDetailsFormated.concat(this.newProblems);
    if (this.data) {
      this.objToPost.dispatcherId = this.data.dispatcherId
      this.objToPost.dispatcherName = this.data.dispatcherName
      this.objToPost.groupId = this.data.groupId
    } else if (this.dispatcherObj) {
      this.objToPost.dispatcherId = this.dispatcherObj.id;
      this.objToPost.dispatcherName = this.dispatcherObj.name;
    } else {
      throw ("invalid dispatcher");
    }
    this.objToPost.orderProblems = [];
    this.objToPost.areas = [];

    allViewProblemsAndAreas.map(
      prob => {
        let objToPost: any = {};
        objToPost.dispatcherName = this.objToPost.dispatcherName;
        objToPost.dispatcherId = this.objToPost.dispatcherId;
        objToPost.areas = [];
        objToPost.orderProblems = [];
        objToPost.groupId = prob.groupId;
        if (prob.area && prob.area.length > 0) {
          prob.area.map(
            probString => {
              this.areas.map(
                area => {
                  if (area.label == probString) {
                    let obj = { areaId: area.id, areaName: area.value };
                    objToPost.areas.push(obj);
                  }
                }
              );
            }
          );
        }
        prob.problems.map(
          probString => {
            this.problems.map(
              problem => {
                if (problem.label == probString) {
                  let obj = { problemId: problem.id, problemName: problem.value };
                  objToPost.orderProblems.push(obj);
                }
              }
            );
          }
        );
        this.objectdispatcher.push(objToPost)
      }
    );
    if (this.data) {
      this._lookup.UpdateMulitWithAreasAndOrderProblems(this.objectdispatcher)
        .subscribe(
          (resposnse) => {
            const msg = this.translateService.instant("Updated Successfully!");
            this.closeAndReload(msg);
          }, err => {
            console.log(err);
          }
        );
    } else {
      this._lookup.AddMulitWithAreasAndOrderProblems(this.objectdispatcher)
        .subscribe(
          (resposnse) => {
            const msg = this.translateService.instant("Added Successfully!");
            this.closeAndReload(msg);
          }, err => {
            console.log(err);
          }
        );
    }
  }


  closeAndReload(msg: string) {
    const success = this.translateService.instant("Success!");

    this.toggleLoading = false;
    this.messagseService.add({
      severity: 'success',
      summary: success,
      detail: (msg || 'Done!')
    });
    this.activeModal.close();
    this.settingsDispatchersService.closeModal.next();
  }

  close() {
    this.activeModal.dismiss();
  }
}