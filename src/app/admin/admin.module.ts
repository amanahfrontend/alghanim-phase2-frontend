import { RouterModule } from '@angular/router';
import { SharedModuleModule } from '../shared-module/shared-module.module';
import { NgModule } from '@angular/core';
import { adminRoutes } from './admin-routes';
import { languagesComponent } from '../shared-module/shared/languageModel/languageModel.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    SharedModuleModule,
    RouterModule.forChild(adminRoutes),
    TranslateModule
  ],
  declarations: [
    languagesComponent,
  ],
  entryComponents: [
    languagesComponent,
  ]
})

export class AdminModule {
}
