import { Component, OnInit } from '@angular/core';
import { LookupService } from '../../../api-module/services/lookup-services/lookup.service';

@Component({
  selector: 'app-user-management-container',
  templateUrl: './user-management-container.component.html',
  styleUrls: ['./user-management-container.component.css']
})

export class UserManagementContainerComponent implements OnInit {
  itemsRoles: any[];
  getDataForRole: string;
  items: any[];
  isLoading: boolean = true;

  constructor(private lookupS: LookupService) { }

  ngOnInit() {
    this.isLoading = true;
    this.getRoles();
  }

  getRoles() {
    this.lookupS.getAllRoles().subscribe(result => {
      this.itemsRoles = result.data
      this.isLoading = false;
      this.getUserData(this.itemsRoles[0].name);
    },
      error => {
        console.log(error)
      }
    );
  }


  getUserData(role) {
    this.getDataForRole = role;
  }


  applyDefaultActiveClass(item) {
    if (this.items.indexOf(item) == 0) {
      return 'uk-open'
    }
  }

  applyActiveTab(role) {
    if (this.itemsRoles.indexOf(role) == 0) {
      return 'uk-active'
    }
  }

}
