import { Component, OnInit, Input, OnChanges, OnDestroy } from '@angular/core';
import { Subscription } from "rxjs";
import { LookupService } from "../../../api-module/services/lookup-services/lookup.service";
import { MessageService } from 'primeng/components/common/messageservice';
import { userManagementModelComponent } from '../userManageModel/userManageModel.component';
import { languagesComponent } from '../../../shared-module/shared/languageModel/languageModel.component'
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { ConfirmationDialog } from 'src/app/shared-module/confirm-dialog-component/confirm-dialog-component';
import { MatDialog } from '@angular/material';


@Component({
  selector: 'app-userManagment',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.css']
})
export class userManagementComponent implements OnInit, OnChanges, OnDestroy {
  @Input() role: string;
  @Input() getUserData: string;
  type: any;
  dataSubscription: Subscription;
  deleteDataSubscription: Subscription;
  modalRef: any;
  rows: any[];
  toggleLoading: boolean;
  itemsRoles: any = [];
  editing = {};

  constructor(
    private dialog: MatDialog,
    private lookUp: LookupService,
    private messageService: MessageService,
    private modalService: NgbModal,
    private config: NgbModalConfig,
    private translateService: TranslateService) {
    config.backdrop = 'static';
  }

  ngOnInit() {
    this.rows = [];
  }

  ngOnChanges() {
    this.toggleLoading = true;
    !this.rows && (this.rows = []);
    if (this.role == 'Admin' && this.getUserData == 'Admin') {
      this.dataSubscription = this.lookUp.getAllUserAdmin()
        .subscribe(
          (data) => {
            this.toggleLoading = false;
            this.rows = data.data;
          }, err => {
            console.log(err);
            this.messageService.add({
              severity: 'error',
              summary: this.translateService.instant('Failed!'),
              detail: this.translateService.instant("Failed to update due to network error")
            });
            this.toggleLoading = false;
          }
        );
    }
    else if (this.role == 'Dispatcher' && this.getUserData == 'Dispatcher') {
      this.dataSubscription = this.lookUp.getAllDispatchersWithBlock()
        .subscribe(
          (data) => {
            this.toggleLoading = false;
            this.rows = data.data;
          }, err => {
            console.log(err);
            this.messageService.add({
              severity: 'error',
              summary: this.translateService.instant('Failed!'),
              detail: this.translateService.instant("Failed to update due to network error")
            });
            this.toggleLoading = false;
          }
        );
    }
    else if (this.role == 'Supervisor' && this.getUserData == 'Supervisor') {
      this.dataSubscription = this.lookUp.getAllSuperVisorWithBlock()
        .subscribe(
          (data) => {
            this.toggleLoading = false;
            this.rows = data.data;
          }, err => {
            console.log(err);
            this.messageService.add({
              severity: 'error',
              summary: this.translateService.instant('Failed!'),
              detail: this.translateService.instant("Failed to update due to network error")
            });
            this.toggleLoading = false;
          }
        );
    }
    else if (this.role == 'Engineer' && this.getUserData == 'Engineer') {
      this.dataSubscription = this.lookUp.getAllEngineersWithBlock()
        .subscribe(
          (data) => {
            this.toggleLoading = false;
            this.rows = data.data;
          }, err => {
            console.log(err);
            this.messageService.add({
              severity: 'error',
              summary: this.translateService.instant('Failed!'),
              detail: this.translateService.instant("Failed to update due to network error")
            });
            this.toggleLoading = false;
          }
        );
    }
    else if (this.role == 'Foreman' && this.getUserData == 'Foreman') {
      this.dataSubscription = this.lookUp.getAllForemansWithBlock()
        .subscribe(
          (data) => {
            this.toggleLoading = false;
            this.rows = data.data;
          }, err => {
            console.log(err);
            this.messageService.add({
              severity: 'error',
              summary: this.translateService.instant('Failed!'),
              detail: this.translateService.instant("Failed to update due to network error")
            });
            this.toggleLoading = false;
          }
        );
    }
    else if (this.role == 'Technician' && this.getUserData == 'Technician') {
      this.dataSubscription = this.lookUp.getAllTechniciansWithBlockedList()
        .subscribe(
          (data) => {
            this.toggleLoading = false;
            this.rows = data.data;
          }, err => {
            console.log(err);
            this.messageService.add({
              severity: 'error',
              summary: this.translateService.instant('Failed!'),
              detail: this.translateService.instant("Failed to update due to network error")
            });
            this.toggleLoading = false;
          }
        );
    }
    else if (this.role == 'Manager' && this.getUserData == 'Manager') {
      this.dataSubscription = this.lookUp.getAllMangaerWithBlock()
        .subscribe(
          (data) => {
            this.toggleLoading = false;
            this.rows = data.data;
          }, err => {
            console.log(err);
            this.messageService.add({
              severity: 'error',
              summary: this.translateService.instant('Failed!'),
              detail: this.translateService.instant("faild to update due to network error")
            });
            this.toggleLoading = false;
          }
        );
    }
    else if (this.role == 'Material Controller' && this.getUserData == 'Material Controller') {
      this.dataSubscription = this.lookUp.getAllMaterialWithBlock()
        .subscribe(
          (data) => {
            this.toggleLoading = false;
            this.rows = data.data;
          }, err => {
            console.log(err);
            this.messageService.add({
              severity: 'error',
              summary: this.translateService.instant('Failed!'),
              detail: this.translateService.instant("faild to update due to network error")
            });
            this.toggleLoading = false;
          }
        );
    }
  }

  ngOnDestroy() {
    this.dataSubscription && this.dataSubscription.unsubscribe();
    this.deleteDataSubscription && this.deleteDataSubscription.unsubscribe();
  }

  add() {
    this.openModal({}, `Add ${this.role}`, this.role);
    this.modalRef.result.then((newValue) => {
      if (this.role == 'Admin') {
        this.ngOnChanges();
      }
      else if (this.role == 'Dispatcher') {
        this.ngOnChanges();
      }
      else if (this.role == 'Supervisor') {
        this.ngOnChanges();
      }
      else if (this.role == 'Engineer') {
        this.ngOnChanges();
      }
      else if (this.role == 'Driver') {
        this.ngOnChanges();
      }
      else if (this.role == 'Foreman') {
        this.ngOnChanges();
      }
      else if (this.role == 'Technician') {
        this.ngOnChanges();
      }
      else if (this.role == 'Manager') {
        this.ngOnChanges();
      }
      else if (this.role == 'Material Controller') {
        this.ngOnChanges();
      }

    })
      .catch((result) => {
        this.messageService.add({
          severity: 'info',
          summary: this.translateService.instant('Nothing Added!'),
          detail: this.translateService.instant("You didn't saved new value.")
        });
      });
  }

  edit(row) {
    this.openModal(Object.assign({}, row), 'Edit', this.role);
    this.modalRef.result.then((editedValue) => {
      if (this.role == 'Admin') {
        this.ngOnChanges();
      }
      else if (this.role == 'Dispatcher') {
        this.ngOnChanges();
      }
      else if (this.role == 'Engineer') {
        this.ngOnChanges()
      }
      else if (this.role == 'Supervisor') {
        this.ngOnChanges()
      }
      else if (this.role == 'Driver') {
        this.ngOnChanges();
      }
      else if (this.role == 'Foreman') {
        this.ngOnChanges();
      }
      else if (this.role == 'Technician') {
        this.ngOnChanges();
      }
      else if (this.role == 'Manager') {
        this.ngOnChanges();
      }
      else if (this.role == 'MaterialController') {
        this.ngOnChanges();
      }
    })
      .catch(
        (result) => {
          this.messageService.add({
            severity: 'info',
            summary: this.translateService.instant('Nothing Edited!'),
            detail: this.translateService.instant("You didn't change the old value")
          });
        }
      );
  }

  addEditLang(row) {
    this.openModalLanguage(Object.assign({}, row), 'language', this.role);
    this.modalRef.result.then(
      (newValue) => {
        this.ngOnChanges();
      }
    ).catch(
      (result) => {
        this.messageService.add({
          severity: 'info',
          summary: this.translateService.instant('Nothing Edited!'),
          detail: this.translateService.instant("You didn't change the old value")
        });
      }
    );
  }

  openModal(data, header, role?) {
    this.modalRef = this.modalService.open(userManagementModelComponent);
    this.modalRef.componentInstance.header = header;
    this.modalRef.componentInstance.role = role;
    this.modalRef.componentInstance.data = data;
  }

  openModalLanguage(data, header, role?) {
    this.modalRef = this.modalService.open(languagesComponent);
    this.modalRef.componentInstance.header = header;
    this.modalRef.componentInstance.role = role;
    this.modalRef.componentInstance.data = data;
  }


  openConfirmationBlock(item, operation, index) {
    console.log(item);

    let confirmbutontxt = item.isDeleted ?
      this.translateService.instant("Confirm UnBlocking") :
      this.translateService.instant("Confirm Blocking");


    const dialogRef = this.dialog.open(ConfirmationDialog, {
      data: {
        message: `Are you sure want to Block (  ${item.fullName} - ${item.name} ) ?`,
        cancelButtonText: "Cancel",
        confirmButtonText: confirmbutontxt,
      }
    });

    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      if (confirmed) {
        console.log(item, operation, index)
        this.block(item, operation, index);
      }
      else {
        //if false or undegiend
      }
    });


  }





  block(item, operation, index) {
    this.editing[index + '-isDeleted'] = true;
    if (this.role == 'Dispatcher') {
      this.lookUp.blockDispatcher(item.id).subscribe(
        res => {
          if (res.isSucceeded == true) {
            if (operation == 'block') {
              this.messageService.add({
                severity: 'success',
                summary: this.translateService.instant('Successful!'),
                detail: this.translateService.instant("Blocked Successfully!")
              });
              this.updateValue(true, 'isDeleted', index);
            } else {
              this.messageService.add({
                severity: 'success',
                summary: this.translateService.instant('Successful!'),
                detail: this.translateService.instant("unblocked Successfully!")
              });
              this.updateValue(false, 'isDeleted', index);
            }
          } else {
            console.log(res);
            if (operation == 'block') {
              this.messageService.add({
                severity: 'error',
                summary: this.translateService.instant('Failed!'),
                detail: this.translateService.instant("Failed to block item!")
              });
            } else {
              this.messageService.add({
                severity: 'error',
                summary: this.translateService.instant('Failed!'),
                detail: this.translateService.instant("Failed to unblock item!")
              });
            }
          }
        }, err => {
          console.log(err);
          if (operation == 'block') {
            this.messageService.add({
              severity: 'error',
              summary: this.translateService.instant('Failed!'),
              detail: this.translateService.instant("Failed to block item!")
            });
          } else {
            this.messageService.add({
              severity: 'error',
              summary: this.translateService.instant('Failed!'),
              detail: this.translateService.instant("Failed to unblock item!")
            });
          }
        }
      );
    }
    else if (this.role == 'Supervisor') {
      this.lookUp.blockSupervisor(item.id).subscribe(
        res => {
          if (res.isSucceeded == true) {
            if (operation == 'block') {
              this.messageService.add({
                severity: 'success',
                summary: this.translateService.instant('Successful!'),
                detail: this.translateService.instant("Blocked Successfully!")
              });
              this.updateValue(true, 'isDeleted', index);
            } else {
              this.messageService.add({
                severity: 'success',
                summary: this.translateService.instant('Successful!'),
                detail: this.translateService.instant("unblocked Successfully!")
              });
              this.updateValue(false, 'isDeleted', index);
            }
          } else {
            if (operation == 'block') {
              this.messageService.add({
                severity: 'error',
                summary: this.translateService.instant('Failed!'),
                detail: this.translateService.instant("Failed to block item!")
              });
            } else {
              this.messageService.add({
                severity: 'error',
                summary: this.translateService.instant('Failed!'),
                detail: this.translateService.instant("Failed to unblock item!")
              });
            }
          }
        }, err => {
          console.log(err);
          if (operation == 'block') {
            this.messageService.add({
              severity: 'error',
              summary: this.translateService.instant('Failed!'),
              detail: this.translateService.instant("Failed to block item!")
            });
          } else {
            this.messageService.add({
              severity: 'error',
              summary: this.translateService.instant('Failed!'),
              detail: this.translateService.instant("Failed to unblock item!")
            });
          }
        }
      );
    }
    else if (this.role == 'Engineer') {
      this.lookUp.blockEngineer(item.id).subscribe(
        res => {
          if (res.isSucceeded == true) {
            if (operation == 'block') {
              this.messageService.add({
                severity: 'success',
                summary: this.translateService.instant('Successful!'),
                detail: this.translateService.instant("Blocked Successfully!")
              });
              this.updateValue(true, 'isDeleted', index);
            } else {
              this.messageService.add({
                severity: 'success',
                summary: this.translateService.instant('Successful!'),
                detail: this.translateService.instant("unblocked Successfully!")
              });
              this.updateValue(false, 'isDeleted', index);
            }
          } else {
            if (operation == 'block') {
              this.messageService.add({
                severity: 'error',
                summary: this.translateService.instant('Failed!'),
                detail: this.translateService.instant("Failed to block item!")
              });
            } else {
              this.messageService.add({
                severity: 'error',
                summary: this.translateService.instant('Failed!'),
                detail: this.translateService.instant("Failed to unblock item!")
              });
            }
          }
        }, err => {
          console.log(err);
          if (operation == 'block') {
            this.messageService.add({
              severity: 'error',
              summary: this.translateService.instant('Failed!'),
              detail: this.translateService.instant("Failed to block item!")
            });
          } else {
            this.messageService.add({
              severity: 'error',
              summary: this.translateService.instant('Failed!'),
              detail: this.translateService.instant("Failed to unblock item!")
            });
          }
        }
      );
    }
    else if (this.role == 'Foreman') {
      this.lookUp.blockForeman(item.id).subscribe(
        res => {
          if (res.isSucceeded == true) {
            if (operation == 'block') {
              this.messageService.add({
                severity: 'success',
                summary: this.translateService.instant('Successful!'),
                detail: this.translateService.instant("Blocked Successfully!")
              });
              this.updateValue(true, 'isDeleted', index);
            } else {
              this.messageService.add({
                severity: 'success',
                summary: this.translateService.instant('Successful!'),
                detail: this.translateService.instant("unblocked Successfully!")
              });
              this.updateValue(false, 'isDeleted', index);
            }
          } else {
            if (operation == 'block') {
              this.messageService.add({
                severity: 'error',
                summary: this.translateService.instant('Failed!'),
                detail: this.translateService.instant("Failed to block item!")
              });
            } else {
              this.messageService.add({
                severity: 'error',
                summary: this.translateService.instant('Failed!'),
                detail: this.translateService.instant("Failed to unblock item!")
              });
            }
          }
        }, err => {
          console.log(err);
          if (operation == 'block') {
            this.messageService.add({
              severity: 'error',
              summary: this.translateService.instant('Failed!'),
              detail: this.translateService.instant("Failed to block item!")
            });
          } else {
            this.messageService.add({
              severity: 'error',
              summary: this.translateService.instant('Failed!'),
              detail: this.translateService.instant("Failed to unblock item!")
            });
          }
        }
      );
    }
    else if (this.role == 'Technician') {
      this.lookUp.blockTechnician(item.id).subscribe(
        res => {
          if (res.isSucceeded == true) {
            if (operation == 'block') {
              this.messageService.add({
                severity: 'success',
                summary: this.translateService.instant('Successful!'),
                detail: this.translateService.instant("Blocked Successfully!")
              });
              this.updateValue(true, 'isDeleted', index);
            } else {
              this.messageService.add({
                severity: 'success',
                summary: this.translateService.instant('Successful!'),
                detail: this.translateService.instant("unblocked Successfully!")
              });
              this.updateValue(false, 'isDeleted', index);
            }
          } else {
            if (operation == 'block') {
              this.messageService.add({
                severity: 'error',
                summary: this.translateService.instant('Failed!'),
                detail: this.translateService.instant("Failed to block item!")
              });
            } else {
              this.messageService.add({
                severity: 'error',
                summary: this.translateService.instant('Failed!'),
                detail: this.translateService.instant("Failed to unblock item!")
              });
            }
          }
        }, err => {
          console.log(err);
          if (operation == 'block') {
            this.messageService.add({
              severity: 'error',
              summary: this.translateService.instant('Failed!'),
              detail: this.translateService.instant("Failed to block item!")
            });
          } else {
            this.messageService.add({
              severity: 'error',
              summary: this.translateService.instant('Failed!'),
              detail: this.translateService.instant("Failed to unblock item!")
            });
          }
        }
      );
    }
    else if (this.role == 'Manager') {
      this.lookUp.blockManager(item.id).subscribe(
        res => {
          if (res.isSucceeded == true) {
            if (operation == 'block') {
              this.messageService.add({
                severity: 'success',
                summary: this.translateService.instant('Successful!'),
                detail: this.translateService.instant("Blocked Successfully!")
              });
              this.updateValue(true, 'isDeleted', index);
            } else {
              this.messageService.add({
                severity: 'success',
                summary: this.translateService.instant('Successful!'),
                detail: this.translateService.instant("unblocked Successfully!")
              });
              this.updateValue(false, 'isDeleted', index);
            }
          } else {
            if (operation == 'block') {
              this.messageService.add({
                severity: 'error',
                summary: this.translateService.instant('Failed!'),
                detail: this.translateService.instant("Failed to block item!")
              });
            } else {
              this.messageService.add({
                severity: 'error',
                summary: this.translateService.instant('Failed!'),
                detail: this.translateService.instant("Failed to unblock item!")
              });
            }
          }
        }, err => {
          console.log(err);
          if (operation == 'block') {
            this.messageService.add({
              severity: 'error',
              summary: this.translateService.instant('Failed!'),
              detail: this.translateService.instant("Failed to block item!")
            });
          } else {
            this.messageService.add({
              severity: 'error',
              summary: this.translateService.instant('Failed!'),
              detail: this.translateService.instant("Failed to unblock item!")
            });
          }
        }
      );
    }
    else if (this.role == 'Material Controller') {
      this.lookUp.blockMaterialController(item.id).subscribe(
        res => {
          if (res.isSucceeded == true) {
            if (operation == 'block') {
              this.messageService.add({
                severity: 'success',
                summary: this.translateService.instant('Successful!'),
                detail: this.translateService.instant("Blocked Successfully!")
              });
              this.updateValue(true, 'isDeleted', index);
            } else {
              this.messageService.add({
                severity: 'success',
                summary: this.translateService.instant('Successful!'),
                detail: this.translateService.instant("unblocked Successfully!")
              });
              this.updateValue(false, 'isDeleted', index);
            }
          } else {
            if (operation == 'block') {
              this.messageService.add({
                severity: 'error',
                summary: this.translateService.instant('Failed!'),
                detail: this.translateService.instant("Failed to block item!")
              });
            } else {
              this.messageService.add({
                severity: 'error',
                summary: this.translateService.instant('Failed!'),
                detail: this.translateService.instant("Failed to unblock item!")
              });
            }
          }
        }, err => {
          console.log(err);
          if (operation == 'block') {
            this.messageService.add({
              severity: 'error',
              summary: this.translateService.instant('Failed!'),
              detail: this.translateService.instant("Failed to block item!")
            });
          } else {
            this.messageService.add({
              severity: 'error',
              summary: this.translateService.instant('Failed!'),
              detail: this.translateService.instant("Failed to unblock item!")
            });
          }
        }
      );
    }
  }

  updateValue(event, cell, rowIndex) {
    this.editing[rowIndex + '-' + cell] = false;
    if (event == true || event == false) {
      this.rows[rowIndex][cell] = event;
    } else {
      this.rows[rowIndex][cell] = event.target.value;
    }
    this.rows = [...this.rows];
  }

}
