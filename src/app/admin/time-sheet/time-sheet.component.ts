import { Component, OnInit } from '@angular/core';
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { MessageService } from 'primeng/primeng';
import { BaseUrlOrderManagementNew } from '../../api-module/services/globalPath';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-time-sheet',
  templateUrl: './time-sheet.component.html',
  styleUrls: ['./time-sheet.component.css']
})
export class TimeSheetComponent implements OnInit {
  costCenters: any = [];
  timeObj: any = {
    dateFrom: '',
    dateTo: '',
    costCenter: '',
    pfNo: '',
  };
  SelectedCostCenter: any;
  rows = [];
  columns = [
    { prop: 'dayDate' },
    { name: 'name' },
    { name: 'pfNo' },
    { name: 'hours' },
    { name: 'workType' },
  ];

  loadingIndicator: boolean;

  constructor(
    private lookUp: LookupService,
    private MessageService: MessageService,
    // private ToExcelService:ToExcelService,
    private translateService: TranslateService
  ) { }


  ngOnInit() {
    this.lookUp.getCostCenter().subscribe((res: any) => {
      this.costCenters = res.data;
      console.log(res);
    },
      err => {
        console.error(err);
        this.MessageService.add({
          severity: 'error',
          summary: this.translateService.instant('Failed'),
          detail: this.translateService.instant(`Failed to get Cost Centers !`)
        });
      })

  }

  onExport(timeObj) {
    // return this.ToExcelService.exportAsExcelFile(this.rows, 'Used Items Report');
    // this.lookUp.exportTimeSheetResults(this.rows)
    if (timeObj.dateFrom == '' || timeObj.dateTo == '') {
      this.MessageService.add({
        severity: 'error',
        summary: this.translateService.instant('Failed'),
        detail: this.translateService.instant(`Enter Valid Dates.`)
      });
    } else {
      this.lookUp.exportTimeSheetResults(timeObj)
        .subscribe(
          (results) => {
            let link = document.createElement("a");
            link.download = "TimeSheet";
            const baseurl = BaseUrlOrderManagementNew.substring(0, BaseUrlOrderManagementNew.length - 4);
            const excelURL = results.data.substring(9);
            // link.href = baseurl + excelURL;
            // document.body.appendChild(link);
            // link.click();
            // document.body.removeChild(link);
            window.open(baseurl + excelURL, "_blank");
          }, err => {
            console.error(err);
            this.MessageService.add({
              severity: 'error',
              summary: this.translateService.instant('Failed'),
              detail: this.translateService.instant(`Failed to get time sheet !`)
            });
          }
        );
    }
  }

  onSelectCostCenter(e: any) {
    this.timeObj.costCenter = e.value.name;
  }

  onReset() {
    this.timeObj = {
      dateFrom: '',
      dateTo: '',
      costCenter: '',
      pfNo: '',
    };
    this.SelectedCostCenter = "";
    this.MessageService.clear();
  }

  ShowTime(timeObj) {
    if (timeObj.dateFrom == '' || timeObj.dateTo == '') {
      this.MessageService.add({
        severity: 'error',
        summary: this.translateService.instant('Failed'),
        detail: this.translateService.instant(`Enter Valid Dates.`)
      });
    } else {
      this.loadingIndicator = true;
      this.lookUp.GetTimeSheet(this.timeObj)
        .subscribe(
          (Timesheetobj) => {
            this.loadingIndicator = false;
            // Timesheetobj.data.map((item) => {
            //   // item.activity_Type = "FLDLBR";
            //   // item.attendance_or_activity_type = "ATRH";
            //   //     item.time = item.time.toFixed(2);
            //   //     if (item.time[7] == ":") {
            //   //       item.time = item.time.substring(0, item.time.length - 1);
            //   //     }
            //   //     item.key_Date = item.key_Date.slice(0, 10);
            // });
            this.rows = Timesheetobj.data;
          }, err => {
            console.error(err);
            this.loadingIndicator = false;
            this.MessageService.add({
              severity: 'error',
              summary: this.translateService.instant('Failed'),
              detail: this.translateService.instant(`Failed to get time sheet !`)
            });
          }
        );
    }
  }
}


