import { NgModule } from '@angular/core';
import { TimeSheetComponent } from './time-sheet.component';
import { SharedModuleModule } from '../../shared-module/shared-module.module';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';


const routes: Routes = [
  { path: "", component: TimeSheetComponent }
];

@NgModule({
  declarations: [
    TimeSheetComponent,
  ],
  imports: [
    SharedModuleModule,
    RouterModule.forChild(routes),
    TranslateModule
  ]
})
export class TimeSheetModule { }
