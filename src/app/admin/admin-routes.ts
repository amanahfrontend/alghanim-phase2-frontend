import { Routes } from "@angular/router";
import { AuthGuardGuard } from "../api-module/guards/auth-guard.guard";

export const adminRoutes: Routes = [
    {
        path: "",
        canActivate: [AuthGuardGuard],
        data: { roles: ['Admin'] },
        children: [
            {
                path: '',
                redirectTo: 'master-data',
                pathMatch: 'full'
            },
            {
                path: 'master-data',
                loadChildren: "./master-data/master-data.module#MasterDataModule",
                data: { title: 'Master Data' }
            },
            {
                path: 'user-management',
                loadChildren: "./user-management/user-management.module#UserManagementModule",
                data: { title: 'User Management' }
            },
            {
                path: 'team-management',
                loadChildren: './team-management/team.module#TeamModule'
            },
            {
                path: 'dispatcher-settings',
                loadChildren: "./dispatcher-settings/dispatcher-settings.module#DispatcherSettingsModule",
                data: { title: 'Dispatcher Settings' }
            },
            {
                path: 'timesheet',
                loadChildren: "./time-sheet/time-sheet.module#TimeSheetModule",
                data: { title: 'Time Sheet' }
            },
            {
                path: 'map-settings',
                loadChildren: "./map-settings/map-settings.module#MapSettingsModule",
                data: { title: 'Map Settings' }
            }
        ]
    }
];