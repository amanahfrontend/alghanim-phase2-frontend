import { Component, OnInit, Input, OnChanges, OnDestroy } from '@angular/core';
import { Observable, Subscription, Subject } from "rxjs";
import { LookupService } from "../../../api-module/services/lookup-services/lookup.service";
import { MessageService } from 'primeng/components/common/messageservice';
import { MasterDataPromptComponent } from '../master-data-prompt/master-data-prompt.component.';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UtilitiesService } from "../../../api-module/services/utilities/utilities.service";
import { TranslateService } from '@ngx-translate/core';
import { MasterDataService } from '../master-data.service';

@Component({
  selector: 'app-master-data',
  templateUrl: './master-data.component.html',
  styleUrls: ['./master-data.component.css']
})
export class MasterDataComponent implements OnInit, OnChanges, OnDestroy {
  @Input() type: string;
  @Input() getData: string;

  modalRef: any;
  rows: any[] = [];
  toggleLoading: boolean;
  editing = {};
  saving = {};
  isBlocking: boolean;

  constructor(private lookUp: LookupService,
    private messageService: MessageService,
    private modalService: NgbModal,
    private utilities: UtilitiesService,
    public masterDataService: MasterDataService,
    public translateService: TranslateService) {
  }

  ngOnInit() {
  }

  /**
   * @param err : error sent by server
   */
  onServiceFailure(err) {
    console.error(err);
    this.showErrorMsg();
    this.toggleLoading = false;
  }

  /**
   * ? Used to generate columns by the number of languages sent by server
   */
  generateColumns() {
    this.rows.forEach((item, index) => {
      this.rows[index].languagesDictionaries.forEach((langObj) => {
        item[langObj.key] = langObj.value;
      });
    });
  }

  /**
   * ? create the langs by the sent values in the result for each table. 
   */
  createLangArray() {
    if (this.rows && this.rows[0] && this.rows[0].languagesDictionaries) {
      this.masterDataService.languageArray = [];
      this.rows[0].languagesDictionaries.forEach((langObj) => {
        this.masterDataService.languageArray.push(langObj);
      });
    }
  }

  /**
   * ? update the value without sending it the the Backend on blur event on the edited input
   * 
   * @param event : html input element
   * @param langIndex : The index of language list generated in createLangArray function
   * @param rowIndex : Index of the data table
   * 
   */
  updateLanguageValue(event, langIndex, rowIndex) {
    if (this.rows[rowIndex].languagesDictionaries[langIndex].value != event.target.value) {
      if (this.rows[rowIndex].isDeleted) {
        this.messageService.add({
          severity: 'error',
          summary: this.translateService.instant('error!'),
          detail: this.translateService.instant("This Item is blocked!")
        });
        this.resetValue(rowIndex);
      } else {
        this.rows[rowIndex].updated = true;
        this.rows[rowIndex].languagesDictionaries[langIndex].value = event.target.value;
        this.rows = [...this.rows];
        this.generateColumns();
        this.messageService.add({
          severity: 'info',
          summary: this.translateService.instant('Nothing Saved!'),
          detail: this.translateService.instant("Click on save button to submit the new value")
        });
      }
    }
  }


  /**
   * ? update the value without sending it the the Backend on blur event on the edited input
   * 
   * @param event : html input element
   * @param rowIndex : index of data table
   */
  updateOrderStatusCode(event, rowIndex) {
    if (this.rows[rowIndex].code != event.target.value) {
      this.rows[rowIndex].code = event.target.value;
      this.messageService.add({
        severity: 'info',
        summary: this.translateService.instant('Nothing Edited!'),
        detail: this.translateService.instant("Click on save button to submit the new value")
      });
      this.rows[rowIndex].updated = true;
    } else if (this.rows[rowIndex].isDeleted) {
      this.messageService.add({
        severity: 'error',
        summary: this.translateService.instant('error!'),
        detail: this.translateService.instant("This Item is blocked!")
      });
      this.resetValue(rowIndex);
    }
  }



  /**
   * ? this function is used to set the localization to each data table upon getting data from the server
   */
  setLoacalizedData() {
    this.masterDataService.languageArray = [];
    this.generateColumns();
    this.createLangArray();
    this.toggleLoading = false;
  }


  updateCashedLists() {
    if (this.type == 'Area') {
      this.masterDataService.AreasWithBlocked = JSON.parse(JSON.stringify(this.rows));
    } else if (this.type == 'Attendance Status') {
      this.masterDataService.AttendanceStatesWithBlocked = JSON.parse(JSON.stringify(this.rows));
    } else if (this.type == 'Order Status') {
      this.masterDataService.OrderStatusWithBlocked = JSON.parse(JSON.stringify(this.rows));
    } else if (this.type == 'Contract Type') {
      this.masterDataService.ContractTypesWithBlocked = JSON.parse(JSON.stringify(this.rows));
    } else if (this.type == 'Cost Center') {
      this.masterDataService.CostCenterWithBlocked = JSON.parse(JSON.stringify(this.rows));
    } else if (this.type == 'Division') {
      this.masterDataService.DivisionWithBlocked = JSON.parse(JSON.stringify(this.rows));
    } else if (this.type == 'Shift') {
      this.masterDataService.ShiftWithBlocked = JSON.parse(JSON.stringify(this.rows));
    } else if (this.type == 'Languages') {
      this.masterDataService.languagesWithBlocked = JSON.parse(JSON.stringify(this.rows));
    } else if (this.type == 'Problems') {
      this.masterDataService.ProblemsLocalizedWithBlocked = JSON.parse(JSON.stringify(this.rows));
    }
    //  else if (this.type == 'Idle Types' || this.type == 'Break Types') {
    //   this.masterDataService.WorkingTypesWithBlocked = JSON.parse(JSON.stringify(this.rows));
    // }
    else if (this.type == 'Idle Types') {
      this.masterDataService.IdleTypes = JSON.parse(JSON.stringify(this.rows));
    }
    else if (this.type == 'Break Types') {
      this.masterDataService.BreakTypes = JSON.parse(JSON.stringify(this.rows));
    }
    else if (this.type == 'On-Hold Sub-Status Types') {
      this.masterDataService.OnHoldSubStatusWithBlocked = JSON.parse(JSON.stringify(this.rows));
    } else if (this.type == 'Order Cancellation Reasons') {
      this.masterDataService.CancellationSubStatusWithBlocked = JSON.parse(JSON.stringify(this.rows));
    } else if (this.type == 'Complete Sub-Status Types') {
      this.masterDataService.CompleteSubStatusWithBlocked = JSON.parse(JSON.stringify(this.rows));
    }
  }

  resetCashedListsAndUpdateLanguage() {
    this.masterDataService.AreasWithBlocked = [];
    this.masterDataService.AttendanceStatesWithBlocked = [];
    this.masterDataService.OrderStatusWithBlocked = [];
    this.masterDataService.ContractTypesWithBlocked = [];
    this.masterDataService.CostCenterWithBlocked = [];
    this.masterDataService.DivisionWithBlocked = [];
    this.masterDataService.ShiftWithBlocked = [];
    this.masterDataService.ProblemsLocalizedWithBlocked = [];
    // this.masterDataService.WorkingTypesWithBlocked = [];
    this.masterDataService.IdleTypes = []
    this.masterDataService.BreakTypes = [];
    this.masterDataService.OnHoldSubStatusWithBlocked = [];
    this.masterDataService.CancellationSubStatusWithBlocked = [];
    this.masterDataService.CompleteSubStatusWithBlocked = [];
    this.masterDataService.languagesWithBlocked = JSON.parse(JSON.stringify(this.rows));
  }

  assignOldRowsValueAndLocalizeData(cashedListName: string, localizationNeeded: boolean) {

    this.rows = JSON.parse(JSON.stringify(this.masterDataService[cashedListName]));
    if (localizationNeeded) {
      setTimeout(() => {
        this.setLoacalizedData();
      }, 0);
    }
    this.toggleLoading = false;

    return;








    // if (cashedListName == "ShiftWithBlocked") {
    //   setTimeout(() => {
    //     this.rows = this.masterDataService.ShiftWithBlocked;
    //     this.masterDataService[cashedListName].map((s) => {
    //       s.fromTime = new Date((s.fromTime));
    //       s.toTime = new Date((s.toTime));
    //     });
    //   }, 0);
    //   this.toggleLoading = false;
    //   // } else if (this.type == 'Idle Types') {
    //   //   
    //   //   this.rows = [];
    //   //   for (var i = 0; i < this.masterDataService[cashedListName].length; i++) {
    //   //     if (this.masterDataService[cashedListName][i].code == "Idle Time hours") {
    //   //       this.rows.push(JSON.parse(JSON.stringify(this.masterDataService[cashedListName][i])));
    //   //     }
    //   //   }
    //   //   this.setLoacalizedData();
    //   //   this.toggleLoading = false;
    //   // } else if (this.type == 'Break Types') {
    //   //   
    //   //   this.rows = [];
    //   //   for (var i = 0; i < this.masterDataService[cashedListName].length; i++) {
    //   //     if (this.masterDataService[cashedListName][i].code == "Break hours") {
    //   //       this.rows.push(JSON.parse(JSON.stringify(this.masterDataService[cashedListName][i])));
    //   //     }
    //   //   }
    //   // this.setLoacalizedData();
    //   // this.toggleLoading = false;
    // } else {
    //   this.rows = JSON.parse(JSON.stringify(this.masterDataService[cashedListName]));
    //   if (localizationNeeded) {
    //     setTimeout(() => {
    //       this.setLoacalizedData();
    //     }, 0);
    //   }
    //   this.toggleLoading = false;
    // }
  }

  restoreSavedValue() {
    if (this.type == 'Area') {
      this.assignOldRowsValueAndLocalizeData('AreasWithBlocked', true);
    } else if (this.type == 'Attendance Status') {
      this.assignOldRowsValueAndLocalizeData('AttendanceStatesWithBlocked', true);
    } else if (this.type == 'Order Status') {
      this.assignOldRowsValueAndLocalizeData('OrderStatusWithBlocked', true);
    } else if (this.type == 'Contract Type') {
      this.assignOldRowsValueAndLocalizeData('ContractTypesWithBlocked', true);
    } else if (this.type == 'Cost Center') {
      this.assignOldRowsValueAndLocalizeData('CostCenterWithBlocked', false);
    } else if (this.type == 'Division') {
      this.assignOldRowsValueAndLocalizeData('DivisionWithBlocked', true);
    } else if (this.type == 'Shift') {
      this.assignOldRowsValueAndLocalizeData('ShiftWithBlocked', false);
    } else if (this.type == 'Languages') {
      this.assignOldRowsValueAndLocalizeData('languagesWithBlocked', false);
    } else if (this.type == 'Problems') {
      this.assignOldRowsValueAndLocalizeData('ProblemsLocalizedWithBlocked', true);
    } else if (this.type == 'Idle Types') {
      this.assignOldRowsValueAndLocalizeData('IdleTypes', true);
    }
    else if (this.type == 'Break Types') {
      this.assignOldRowsValueAndLocalizeData('BreakTypes', true);
    }
    else if (this.type == 'On-Hold Sub-Status Types') {
      this.assignOldRowsValueAndLocalizeData('OnHoldSubStatusWithBlocked', true);
    } else if (this.type == 'Order Cancellation Reasons') {
      this.assignOldRowsValueAndLocalizeData('CancellationSubStatusWithBlocked', true);
    } else if (this.type == 'Complete Sub-Status Types') {
      this.assignOldRowsValueAndLocalizeData('CompleteSubStatusWithBlocked', true);
    }
  }


  onChangeType(cashedListName: string, getAllRequest: Observable<any>, localizationNeeded: boolean) {

    if (this.masterDataService[cashedListName].length > 0) {
      this.assignOldRowsValueAndLocalizeData(cashedListName, localizationNeeded);
    } else {
      if (this.getData == this.type) {
        this.toggleLoading = true;
      } else {
        this.toggleLoading = false;
      }
      getAllRequest
        .subscribe(
          (res: any) => {
            if (this.getData == this.type) {
              if (res && res.isSucceeded) {
                if (this.type == 'Idle Types') {
                  this.rows = [];
                  for (var i = 0; i < res.data.length; i++) {
                    if (res.data[i].code == "Idle Time hours") {
                      this.rows.push(res.data[i]);
                    }
                  }
                  this.masterDataService.IdleTypes = JSON.parse(JSON.stringify(this.rows));
                  //this.masterDataService.WorkingTypesWithBlocked = JSON.parse(JSON.stringify(res.data));
                  this.setLoacalizedData();
                  this.toggleLoading = false;
                } else if (this.type == 'Break Types') {
                  this.rows = [];
                  for (var i = 0; i < res.data.length; i++) {
                    if (res.data[i].code == "Break hours") {
                      this.rows.push(res.data[i]);
                    }
                  }
                  this.masterDataService.BreakTypes = JSON.parse(JSON.stringify(this.rows));
                  //this.masterDataService.WorkingTypesWithBlocked = JSON.parse(JSON.stringify(res.data));
                  this.setLoacalizedData();
                  this.toggleLoading = false;
                } else if (this.type == 'Shift') {
                  this.rows = res.data
                  this.rows.map((s) => {
                    s.fromTime = new Date((s.fromTime));
                    s.toTime = new Date((s.toTime));
                  });
                  this.masterDataService[cashedListName] = JSON.parse(JSON.stringify(this.rows));
                  this.toggleLoading = false;
                } else {
                  this.rows = res.data;
                  this.masterDataService[cashedListName] = JSON.parse(JSON.stringify(this.rows));
                  if (localizationNeeded) {
                    this.setLoacalizedData();
                  } else {
                    this.toggleLoading = false;
                  }
                }
              } else {
                this.restoreSavedValue();
                this.onServiceFailure(res);
              }
            }
          }, (err: any) => this.onServiceFailure(err));
    }
  }

  /**
   *  
   * ? 1- open loader 
   * ? 2- as per the type get the data and localize it if there is a localization for the data loaded from server
   * ? 4- close loader
   * ? 3- cache it to load it when expand without loading data from server
   */
  ngOnChanges() {

    this.masterDataService.languageArray = [];
    this.toggleLoading = true;
    !this.rows && (this.rows = []);
    if (this.type == 'Area' && this.getData == 'Area') {
      this.onChangeType('AreasWithBlocked', this.lookUp.getAllAreasWithBlocked(), true);
    } else if (this.type == 'Attendance Status' && this.getData == 'Attendance Status') {
      this.onChangeType('AttendanceStatesWithBlocked', this.lookUp.getAllAttendanceStatesWithBlocked(), true);
    } else if (this.type == 'Contract Type' && this.getData == 'Contract Type') {
      this.onChangeType('ContractTypesWithBlocked', this.lookUp.getContractTypesWithBlock(), true);
    } else if (this.type == 'Cost Center' && this.getData == 'Cost Center') {
      this.onChangeType('CostCenterWithBlocked', this.lookUp.getCostCenterWithBlock(), false);
    } else if (this.type == 'Division' && this.getData == 'Division') {
      this.onChangeType('DivisionWithBlocked', this.lookUp.getDivisionWithBlock(), true);
    } else if (this.type == 'Shift' && this.getData == 'Shift') {
      this.onChangeType('ShiftWithBlocked', this.lookUp.getShiftWithBlock(), false);
    } else if (this.type == 'Languages' && this.getData == 'Languages') {
      this.onChangeType('languagesWithBlocked', this.lookUp.getlanguagesWithBlock(), false);
    } else if (this.type == 'Problems' && this.getData == 'Problems') {
      this.onChangeType('ProblemsLocalizedWithBlocked', this.lookUp.getAllProblemsLocalizedWithBlock(), true);
    } else if (this.type == 'Idle Types' && this.getData == 'Idle Types') {
      this.onChangeType('IdleTypes', this.lookUp.getAllWorkingTypesWithBlock(), true);
    } else if (this.type == 'Break Types' && this.getData == 'Break Types') {
      this.onChangeType('BreakTypes', this.lookUp.getAllWorkingTypesWithBlock(), true);
    } else if (this.type == 'Order Status' && this.getData == 'Order Status') {
      this.onChangeType('OrderStatusWithBlocked', this.lookUp.getAllOrderStatusWithBlock(), true);
    } else if (this.type == 'On-Hold Sub-Status Types' && this.getData == 'On-Hold Sub-Status Types') {
      this.onChangeType('OnHoldSubStatusWithBlocked', this.lookUp.getAllSubOrderStatusWithBlock(6), true);
    } else if (this.type == 'Order Cancellation Reasons' && this.getData == 'Order Cancellation Reasons') {
      this.onChangeType('CancellationSubStatusWithBlocked', this.lookUp.getAllSubOrderStatusWithBlock(8), true);
    } else if (this.type == 'Complete Sub-Status Types' && this.getData == 'Complete Sub-Status Types') {
      this.onChangeType('CompleteSubStatusWithBlocked', this.lookUp.getAllSubOrderStatusWithBlock(7), true);
    }
  }

  /**
   * ? unsubscribe to all Subscription
   */
  ngOnDestroy() {
  }


  onAddNewType(request: Observable<any>, cashedListName: string) {
    request
      .subscribe(
        (res: any) => {
          if (res.isSucceeded == true) {
            this.showSuccessMsg("New value saved Successfully!.");
            this.masterDataService[cashedListName] = [];
            this.ngOnChanges();
            if (this.type == 'Languages') {
              this.resetCashedListsAndUpdateLanguage();
            }
          } else {
            this.showErrorMsg(res.status.message);
          }
        },
        (err: any) => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.onServiceFailure(err);
        }
      );
  }

  /**
   * ? 
   * 1- open master data modal and update the backend according to the type.
   * 2- if failed and the user was not authorized logout.
   */
  add() {
    this.openModal({}, `Add ${this.type}`, this.type);
    this.modalRef.result.then(
      (newValue) => {
        if (this.type == 'Area') {
          this.onAddNewType(this.lookUp.postArea(newValue), 'AreasWithBlocked');
        } else if (this.type == 'Problems') {
          this.onAddNewType(this.lookUp.postProblem(newValue), 'ProblemsLocalizedWithBlocked');
        } else if (this.type == 'Attendance Status') {
          this.onAddNewType(this.lookUp.postAttendanceStates(newValue), 'AttendanceStatesWithBlocked');
        } else if (this.type == 'Order Status') {
          this.onAddNewType(this.lookUp.postOrderStatus(newValue), 'OrderStatusWithBlocked');
        } else if (this.type == 'Contract Type') {
          this.onAddNewType(this.lookUp.postContractType(newValue), 'ContractTypesWithBlocked');
        } else if (this.type == 'Cost Center') {
          this.onAddNewType(this.lookUp.postCostCenter(newValue), 'CostCenterWithBlocked');
        } else if (this.type == 'Division') {
          this.onAddNewType(this.lookUp.postDivision(newValue), 'DivisionWithBlocked');
        } else if (this.type == 'Shift') {
          this.onAddNewType(this.lookUp.postShift(newValue), 'ShiftWithBlocked');
        } else if (this.type == 'Languages') {
          this.onAddNewType(this.lookUp.postLanguages(newValue), 'languagesWithBlocked');
        } else if (this.type == 'On-Hold Sub-Status Types' ||
          this.type == 'Complete Sub-Status Types' ||
          this.type == 'Order Cancellation Reasons') {
          this.masterDataService.OnHoldSubStatusWithBlocked = this.masterDataService.CompleteSubStatusWithBlocked = this.masterDataService.CancellationSubStatusWithBlocked = [];
          this.onAddNewType(this.lookUp.addOrderSubStatus(newValue), 'OnHoldSubStatusWithBlocked');
        } else if (this.type == 'Idle Types') {
          this.onAddNewType(this.lookUp.addWorkingType(newValue), 'IdleTypes');
        } else if (this.type == 'Break Types') {
          this.onAddNewType(this.lookUp.addWorkingType(newValue), 'BreakTypes');
        }
      })
      .catch(
        (result: any) => {
          this.messageService.add({
            severity: 'info',
            summary: this.translateService.instant('Nothing Added!'),
            detail: this.translateService.instant("You didn't saved new value.")
          });
        }
      );
  }

  onDeleteItem(request: Observable<any>, id: any, cashedListName: string) {
    request
      .subscribe(
        (res: any) => {
          if (res.isSucceeded) {
            this.rows = this.rows.filter((oneRow) => {
              return oneRow.id != id;
            });
            this.masterDataService[cashedListName] = JSON.parse(JSON.stringify(this.rows));
            this.showSuccessMsg('Item removed successfully!')
          } else {
            this.restoreSavedValue();
            this.onServiceFailure(res);
          }
        },
        (err: any) => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.onServiceFailure(err);
        }
      );
  }

  /**
   * ? delete the selected row then remove it from the table and show success message.
   * 
   * @param row The clicked row to delete
   */
  remove(row) {
    const id = row.id;
    if (this.type == 'Area') {
      this.onDeleteItem(this.lookUp.deleteArea(id), id, 'AreasWithBlocked');
    } else if (this.type == 'Attendance Status') {
      this.onDeleteItem(this.lookUp.deleteAttendanceStates(id), id, 'AreasWithBlocked');
    } else if (this.type == 'Order Status') {
      this.onDeleteItem(this.lookUp.deleteOrderStatus(id), id, 'AreasWithBlocked');
    } else if (this.type == 'Contract Type') {
      this.onDeleteItem(this.lookUp.deleteContractTypes(id), id, 'AreasWithBlocked');
    } else if (this.type == 'Cost Center') {
      this.onDeleteItem(this.lookUp.deleteCostCenter(row.name), id, 'AreasWithBlocked');
    } else if (this.type == 'Division') {
      this.onDeleteItem(this.lookUp.deleteDivision(id), id, 'AreasWithBlocked');
    } else if (this.type == 'Shift') {
      this.onDeleteItem(this.lookUp.deleteDivision(id), id, 'AreasWithBlocked');
    } else if (this.type == 'Languages') {
      this.onDeleteItem(this.lookUp.deleteLanguages(id), id, 'AreasWithBlocked');
    }
  }

  /**
   * ? show the pop up to confirm blocking
   * 
   * @param item : selected row
   * @param operation : to be done
   * @param index : index of the data table
   */
  block(item, operation, index) {
    this.messageService.add({
      key: 'blockItem',
      sticky: true,
      severity: 'error',
      summary: `${this.translateService.instant(`Are you sure you want to`)} ${item.isDeleted ? "unblock" : "block"} ${item.English || item.name} ?`,
      detail: 'Confirm to proceed',
      data: {
        action: "block",
        item: item,
        operation: operation,
        index: index
      }
    });
  }

  /**
   * ? display pop up of updating item
   * @param row : selected row
   * @param index : index of data table
   */
  save(row, index) {
    this.messageService.add({
      key: 'blockItem',
      sticky: true,
      severity: 'error',
      summary: `${this.translateService.instant(`Are you sure you want to update`)} ${row.English || row.name} ?`,
      detail: this.translateService.instant('Confirm to proceed'),
      data: {
        action: "update",
        row: row,
        index: index
      }
    });
  }

  /**
   * ? according to action open the selected pop up
   * 
   * @param data 
   */
  action(data: any) {
    this.isBlocking = true;
    if (data.action == "block") {
      this.blockItem(data.item, data.operation, data.index)
    } else if (data.action == "update") {
      this.saveItem(data.row, data.index)
    } else if (data.action == "deleteAllOrders") {
      this.deleteAllOrders();
    } else if (data.action == "deleteUserManagement") {
      this.deleteUserManagement();
    } else if (data.action == "deleteDispatcherSettings") {
      this.deleteDispatcherSettings();
    }
  }

  /**
   * ? close the pop up.
   */
  onReject() {
    this.messageService.clear('blockItem');
    this.isBlocking = false;
  }


  /**
   * ? send the request to the Back end according to the type
   * 
   * @param item : selected row
   * @param operation : to be done
   * @param index : index of the data table
   */
  blockItem(item, operation, index) {
    let request: Observable<any>;
    if (this.type == 'Area') {
      request = this.lookUp.blockArea(item.id);
    } else if (this.type == 'Attendance Status') {
      request = this.lookUp.blockAttendanceStatus(item.id);
    } else if (this.type == 'Order Status') {
      request = this.lookUp.blockOrderStatus(item.id);
    } else if (this.type == 'Contract Type') {
      request = this.lookUp.blockContractType(item.id);
    } else if (this.type == 'Cost Center') {
      request = this.lookUp.blockCostCenter(item.id);
    } else if (this.type == 'Division') {
      request = this.lookUp.blockDivision(item.id);
    } else if (this.type == 'Shift') {
      request = this.lookUp.blockShift(item.id);
    } else if (this.type == 'Languages') {
      request = this.lookUp.blockLanguage(item.id);
    } else if (this.type == 'Languages') {
      request = this.lookUp.blockLanguage(item.id);
    } else if (this.type == 'Problems') {
      request = this.lookUp.blockProblem(item.id);
    } else if (this.type == 'Idle Types' || this.type == 'Break Types') {
      request = this.lookUp.blockWorkingType(item.id);
    } else if (this.type == 'On-Hold Sub-Status Types' ||
      this.type == 'Order Cancellation Reasons' ||
      this.type == 'Complete Sub-Status Types') {
      request = this.lookUp.blockOrderSubstatus(item.id);
    }
    request.subscribe(
      (res: any) => {
        if (res.isSucceeded == true) {
          if (operation == 'block') {
            this.showSuccessMsg("Blocked Successfully!");
            this.updateValue(true, 'isDeleted', index);
          } else {
            this.showSuccessMsg("Unblocked Successfully!");
            this.updateValue(false, 'isDeleted', index);
          }

          if (this.type == 'Languages') {
            this.resetCashedListsAndUpdateLanguage();
          } else {
            this.updateCashedLists();
          }

        } else {
          this.restoreSavedValue();
          this.showErrorMsg(res.status.message);
        }
        this.onReject();
      }, (err: any) => {
        console.error(err);
        this.showErrorMsg();
        this.restoreSavedValue();
        this.onReject();
      }
    );

    // _________________________________________________
    // if (this.type == 'Area') {
    //   this.lookUp.blockArea(item.id).subscribe(
    //     res => {
    //       if (res.isSucceeded == true) {
    //         if (operation == 'block') {
    //           this.blockedSuccessfully();
    //           this.updateValue(true, 'isDeleted', index);
    //         } else {
    //           this.unblockedSuccessfully();
    //           this.updateValue(false, 'isDeleted', index);
    //         }
    //       } else {
    //         this.failedToBlock();
    //       }
    //       this.onReject();
    //     }, (err: any) => {
    //       console.log(err);
    //       if (operation == 'block') {
    //         this.failedToBlock();
    //       } else {
    //         this.failedToBlock();
    //       }
    //       this.onReject();
    //     }
    //   );
    // }
    // else if (this.type == 'Attendance Status') {
    //   this.lookUp.blockAttendanceStatus(item.id).subscribe(
    //     res => {
    //       if (res.isSucceeded == true) {
    //         if (operation == 'block') {
    //           this.blockedSuccessfully();
    //           this.updateValue(true, 'isDeleted', index);
    //         } else {
    //           this.unblockedSuccessfully();
    //           this.updateValue(false, 'isDeleted', index);
    //         }
    //       } else {
    //         this.failedToBlock();
    //       }
    //       this.onReject();
    //     }, (err: any) => {
    //       console.log(err);
    //       if (operation == 'block') {
    //         this.failedToBlock();
    //       } else {
    //         this.failedToBlock();
    //       }
    //       this.onReject();
    //     }
    //   );
    // }
    // //OrderStatus
    // else if (this.type == 'Order Status') {
    //   this.lookUp.blockOrderStatus(item.id).subscribe(
    //     res => {
    //       if (res.isSucceeded == true) {
    //         if (operation == 'block') {
    //           this.blockedSuccessfully();
    //           this.updateValue(true, 'isDeleted', index);
    //         } else {
    //           this.unblockedSuccessfully();
    //           this.updateValue(false, 'isDeleted', index);
    //         }
    //       } else {
    //         this.failedToBlock();
    //       }
    //       this.onReject();
    //     }, (err: any) => {
    //       console.log(err);
    //       if (operation == 'block') {
    //         this.failedToBlock();
    //       } else {
    //         this.failedToBlock();
    //       }
    //       this.onReject();
    //     }
    //   );
    //   // }
    //   //
    //   // else if (this.type == 'Availability') {
    //   //   this.lookUp.blockAvailabilty(item.id).subscribe(
    //   //     res => {
    //   //       if (res.isSucceeded == true) {
    //   //         if (operation == 'block') {
    //   //           this.blockedSuccessfully();
    //   //           this.updateValue(true, 'isDeleted', index);
    //   //         } else {
    //   //           this.unblockedSuccessfully();
    //   //           this.updateValue(false, 'isDeleted', index);
    //   //         }
    //   //       } else {
    //   //         this.failedToBlock();
    //   //       }
    //   //       this.onReject();
    //   //     }, (err:any) => {
    //   //       console.log(err);
    //   //       if (operation == 'block') {
    //   //         this.failedToBlock();
    //   //       } else {
    //   //         this.failedToBlock();
    //   //       }
    //   //       this.onReject();
    //   //     }
    //   //   );
    //   // } else if (this.type == 'Building Types') {
    //   //   this.lookUp.blockBuildingType(item.id).subscribe(
    //   //     res => {
    //   //       if (res.isSucceeded == true) {
    //   //         if (operation == 'block') {
    //   //           this.blockedSuccessfully();
    //   //           this.updateValue(true, 'isDeleted', index);
    //   //         } else {
    //   //           this.unblockedSuccessfully();
    //   //           this.updateValue(false, 'isDeleted', index);
    //   //         }
    //   //       } else {
    //   //         this.failedToBlock();
    //   //       }
    //   //       this.onReject();
    //   //     }, (err:any) => {
    //   //       console.log(err);
    //   //       if (operation == 'block') {
    //   //         this.failedToBlock();
    //   //       } else {
    //   //         this.failedToBlock();
    //   //       }
    //   //       this.onReject();
    //   //     }
    //   //   );
    //   // } else if (this.type == 'Company Code') {
    //   //   this.lookUp.blockCompanyCode(item.id).subscribe(
    //   //     res => {
    //   //       if (res.isSucceeded == true) {
    //   //         if (operation == 'block') {
    //   //           this.blockedSuccessfully();
    //   //           this.updateValue(true, 'isDeleted', index);
    //   //         } else {
    //   //           this.unblockedSuccessfully();
    //   //           this.updateValue(false, 'isDeleted', index);
    //   //         }
    //   //       } else {
    //   //         this.failedToBlock();
    //   //       }
    //   //       this.onReject();
    //   //     }, (err: any) => {
    //   //       console.log(err);
    //   //       if (operation == 'block') {
    //   //         this.failedToBlock();
    //   //       } else {
    //   //         this.failedToBlock();
    //   //       }
    //   //       this.onReject();
    //   //     }
    //   //   );
    // } else if (this.type == 'Contract Type') {
    //   this.lookUp.blockContractType(item.id).subscribe(
    //     res => {
    //       if (res.isSucceeded == true) {
    //         if (operation == 'block') {
    //           this.blockedSuccessfully();
    //           this.updateValue(true, 'isDeleted', index);
    //         } else {
    //           this.unblockedSuccessfully();
    //           this.updateValue(false, 'isDeleted', index);
    //         }
    //       } else {
    //         this.failedToBlock();
    //       }
    //       this.onReject();
    //     }, (err: any) => {
    //       console.log(err);
    //       if (operation == 'block') {
    //         this.failedToBlock();
    //       } else {
    //         this.failedToBlock();
    //       }
    //       this.onReject();
    //     }
    //   );
    // } else if (this.type == 'Cost Center') {
    //   this.lookUp.blockCostCenter(item.id).subscribe(
    //     res => {
    //       if (res.isSucceeded == true) {
    //         if (operation == 'block') {
    //           this.blockedSuccessfully();
    //           this.updateValue(true, 'isDeleted', index);
    //         } else {
    //           this.unblockedSuccessfully();
    //           this.updateValue(false, 'isDeleted', index);
    //         }
    //       } else {
    //         this.failedToBlock();
    //       }
    //       this.onReject();
    //     }, (err: any) => {
    //       console.log(err);
    //       if (operation == 'block') {
    //         this.failedToBlock();
    //       } else {
    //         this.failedToBlock();
    //       }
    //       this.onReject();
    //     }
    //   );
    // } else if (this.type == 'Division') {
    //   this.lookUp.blockDivision(item.id).subscribe(
    //     res => {
    //       if (res.isSucceeded == true) {
    //         if (operation == 'block') {
    //           this.blockedSuccessfully();
    //           this.updateValue(true, 'isDeleted', index);
    //         } else {
    //           this.unblockedSuccessfully();
    //           this.updateValue(false, 'isDeleted', index);
    //         }
    //       } else {
    //         this.failedToBlock();
    //       }
    //       this.onReject();
    //     }, (err: any) => {
    //       console.log(err);
    //       if (operation == 'block') {
    //         this.failedToBlock();
    //       } else {
    //         this.failedToBlock();
    //       }
    //       this.onReject();
    //     }
    //   );
    //   // } else if (this.type == 'Governorates') {
    //   //   this.lookUp.blockGovernorate(item.id).subscribe(
    //   //     res => {
    //   //       if (res.isSucceeded == true) {
    //   //         if (operation == 'block') {
    //   //           this.blockedSuccessfully();
    //   //           this.updateValue(true, 'isDeleted', index);
    //   //         } else {
    //   //           this.unblockedSuccessfully();
    //   //           this.updateValue(false, 'isDeleted', index);
    //   //         }
    //   //       } else {
    //   //         this.failedToBlock();
    //   //       }
    //   //       this.onReject();
    //   //     }, (err:any) => {
    //   //       console.log(err);
    //   //       if (operation == 'block') {
    //   //         this.failedToBlock();
    //   //       } else {
    //   //         this.failedToBlock();
    //   //       }
    //   //       this.onReject();
    //   //     }
    //   //   );
    // } else if (this.type == 'Shift') {
    //   this.lookUp.blockShift(item.id).subscribe(
    //     res => {
    //       if (res.isSucceeded == true) {
    //         if (operation == 'block') {
    //           this.blockedSuccessfully();
    //           this.updateValue(true, 'isDeleted', index);
    //         } else {
    //           this.unblockedSuccessfully();
    //           this.updateValue(false, 'isDeleted', index);
    //         }
    //       } else {
    //         this.failedToBlock();
    //       }
    //       this.onReject();
    //     }, (err: any) => {
    //       console.log(err);
    //       if (operation == 'block') {
    //         this.failedToBlock();
    //       } else {
    //         this.failedToBlock();
    //       }
    //       this.onReject();
    //     }
    //   );
    // } else if (this.type == 'Languages') {
    //   this.lookUp.blockLanguage(item.id).subscribe(
    //     res => {
    //       if (res.isSucceeded == true) {
    //         if (operation == 'block') {
    //           this.blockedSuccessfully();
    //           this.updateValue(true, 'isDeleted', index);
    //         } else {
    //           this.unblockedSuccessfully();
    //           this.updateValue(false, 'isDeleted', index);
    //         }
    //       } else {
    //         this.failedToBlock();
    //       }
    //       this.onReject();
    //     }, (err: any) => {
    //       console.log(err);
    //       if (operation == 'block') {
    //         this.failedToBlock();
    //       } else {
    //         this.failedToBlock();
    //       }
    //       this.onReject();
    //     }
    //   );
    // } else if (this.type == 'Problems') {
    //   this.lookUp.blockProblem(item.id).subscribe(
    //     res => {
    //       if (res.isSucceeded == true) {
    //         if (operation == 'block') {
    //           this.blockedSuccessfully();
    //           this.updateValue(true, 'isDeleted', index);
    //         } else {
    //           this.unblockedSuccessfully();
    //           this.updateValue(false, 'isDeleted', index);
    //         }
    //       } else {
    //         this.failedToBlock();
    //       }
    //       this.onReject();
    //     }, (err: any) => {
    //       console.log(err);
    //       if (operation == 'block') {
    //         this.failedToBlock();
    //       } else {
    //         this.failedToBlock();
    //       }
    //       this.onReject();
    //     }
    //   );
    // } else if (this.type == 'Idle Types' || this.type == 'Break Types') {
    //   this.lookUp.blockWorkingType(item.id).subscribe(
    //     res => {
    //       if (res.isSucceeded == true) {
    //         if (operation == 'block') {
    //           this.blockedSuccessfully();
    //           this.updateValue(true, 'isDeleted', index);
    //         } else {
    //           this.unblockedSuccessfully();
    //           this.updateValue(false, 'isDeleted', index);
    //         }
    //       } else {
    //         this.failedToBlock();
    //       }
    //       this.onReject();
    //     }, (err: any) => {
    //       console.log(err);
    //       if (operation == 'block') {
    //         this.failedToBlock();
    //       } else {
    //         this.failedToBlock();
    //       }
    //       this.onReject();
    //     }
    //   );
    // } else if (this.type == 'On-Hold Sub-Status Types' ||
    //   this.type == 'Order Cancellation Reasons' ||
    //   this.type == 'Complete Sub-Status Types') {
    //   this.lookUp.blockOrderSubstatus(item.id).subscribe(
    //     res => {
    //       if (res.isSucceeded == true) {
    //         if (operation == 'block') {
    //           this.blockedSuccessfully();
    //           this.updateValue(true, 'isDeleted', index);
    //         } else {
    //           this.unblockedSuccessfully();
    //           this.updateValue(false, 'isDeleted', index);
    //         }
    //       } else {
    //         this.failedToBlock();
    //       }
    //       this.onReject();
    //     }, (err: any) => {
    //       console.log(err);
    //       if (operation == 'block') {
    //         this.failedToBlock();
    //       } else {
    //         this.failedToBlock();
    //       }
    //       this.onReject();
    //     }
    //   );
    // }
  }

  // addEditLang(row) {
  //   this.openModalLanguage(Object.assign({}, row), 'language', this.type);
  //   this.modalRef.result.then(
  //     (newValue) => {
  //       this.ngOnChanges();
  //     }
  //   ).catch(
  //     (result) => {
  //       this.messageService.add({
  //         severity: 'info',
  //         summary: this.translateService.instant('Nothing Edited!'),
  //         detail: this.translateService.instant("You didn't change the old value")
  //       });
  //     }
  //   );
  // }

  /**
   * @param data selected row /// after updating the code the data is not sent and an empty object is sent instead as the update done inline 
   * @param header 
   * @param type 
   */
  openModal(data, header, type?) {
    this.modalRef = this.modalService.open(MasterDataPromptComponent);
    this.modalRef.componentInstance.header = header;
    this.modalRef.componentInstance.type = type;
    this.modalRef.componentInstance.data = data;
  }

  // openModalLanguage(data, header, type?) {
  //   this.modalRef = this.modalService.open(languagesComponent);
  //   this.modalRef.componentInstance.header = header;
  //   this.modalRef.componentInstance.type = type;
  //   this.modalRef.componentInstance.data = data;
  // }

  /**
   * ? update the selected row in backend
   * 
   * @param row selected row
   * @param index index of data table
   */
  saveItem(row, index) {
    this.saving['saving' + index] = true;
    let request: Observable<any>;
    if (this.type == 'Area')
      request = this.lookUp.editAreaLanguages(row);
    else if (this.type == 'Attendance Status')
      request = this.lookUp.editAttendanceStatesLanguages(row);
    else if (this.type == 'Order Status')
      request = this.lookUp.editOrderStatusLanguages(row);
    else if (this.type == 'Idle Types' || this.type == 'Break Types')
      request = this.lookUp.editWorkingTypeLanguages(row);
    // else if (this.type == 'Availability')
    //   request = this.lookUp.editAvailabilityLanguages(row);
    // else if (this.type == 'Building Types')
    //   request = this.lookUp.editBuildingTypesLanguages(row);
    // else if (this.type == 'Company Code')
    //   request = this.lookUp.editCompanyCodeLanguages(row);
    else if (this.type == 'Contract Type')
      request = this.lookUp.editContractTypesLanguages(row);
    else if (this.type == 'Cost Center')
      request = this.lookUp.editCostCenterLanguages(row);
    else if (this.type == 'Division')
      request = this.lookUp.editDivisionLanguages(row);
    // else if (this.type == 'Governorates')
    //   request = this.lookUp.editGovernoratesLanguages(row)

    else if (this.type == 'Shift') {
      let objToPost: any = {};
      objToPost.id = row.id;
      let thours = (row.toTime.getHours() < 10) ? "0" + row.toTime.getHours() : row.toTime.getHours();
      let tmins = (row.toTime.getMinutes() < 10) ? "0" + row.toTime.getMinutes() : row.toTime.getMinutes();
      objToPost.toTime = `0001-01-01T${thours}:${tmins}:00`;
      let fhours = (row.fromTime.getHours() < 10) ? "0" + row.fromTime.getHours() : row.fromTime.getHours();
      let fmins = (row.fromTime.getMinutes() < 10) ? "0" + row.fromTime.getMinutes() : row.fromTime.getMinutes();
      objToPost.fromTime = `0001-01-01T${fhours}:${fmins}:00`;
      if (objToPost.fromTime == objToPost.toTime) {
        this.messageService.add({
          severity: 'error',
          summary: this.translateService.instant('Failed!'),
          detail: this.translateService.instant("From time should not be equal to to time, please select different times!")
        });
        this.saving['saving' + index] = false;
        this.onReject();
        return;
      }
      request = this.lookUp.editShiftLanguages(objToPost);
    }
    else if (this.type == 'Languages')
      request = this.lookUp.editLanguages(row)
    else if (this.type == 'Problems')
      request = this.lookUp.editProblem(row)
    else if (this.type == 'Complete Sub-Status Types' ||
      this.type == 'On-Hold Sub-Status Types' ||
      this.type == 'Order Cancellation Reasons')
      request = this.lookUp.editOrderSubStatusLanguages(row)
    request.subscribe(
      (res: any) => {
        if (res.isSucceeded) {
          row.updated = false;
          this.saving['saving' + index] = false;
          this.showSuccessMsg("Updated Successfully!");
          this.onReject();
          if (this.type == 'Languages') {
            this.resetCashedListsAndUpdateLanguage();
          } else {
            this.updateCashedLists();
          }
        } else if (!res.isSucceeded) {
          this.failedToUpdate(res, index, res.status.message);
          this.onReject();
        }
      }, (err: any) => {
        this.failedToUpdate(err, index);
      }
    );


    //  else if (this.type == 'Idle Types' || this.type == 'Break Types') {
    //   this.lookUp.updateWorkingType(row)
    //     .subscribe(
    //       res => {
    //         this.saving['saving' + index] = false;
    //         this.messageService.add({
    //           severity: 'success',
    //           summary: 'Successful!',
    //           detail: "Updated Successfully!"
    //         });
    //         this.onReject();
    //       }, (err:any) => {
    //         console.log(err);
    //         this.messageService.add({
    //           severity: 'error',
    //           summary: 'Failed!',
    //           detail: "Failed to update item!"
    //         });
    //         this.onReject();
    //       }
    //     );
    // }
  }


  /**
   * ? update the view without sending data to backend
   * 
   * @param event html input element
   * @param cell selected cell
   * @param rowIndex index of data table
   */
  updateValue(event, cell, rowIndex) {
    this.editing[rowIndex + '-' + cell] = false;
    if (event == true || event == false) {
      this.rows[rowIndex][cell] = event;
    } else {
      if (this.rows[rowIndex].isDeleted) {
        this.messageService.add({
          severity: 'error',
          summary: this.translateService.instant('error!'),
          detail: this.translateService.instant("This Item is blocked!")
        });
        this.resetValue(rowIndex);
      } else {
        if (!(!event.target.value && !this.rows[rowIndex][cell]) && this.rows[rowIndex][cell] !== event.target.value) {
          this.rows[rowIndex][cell] = event.target.value;
          this.rows[rowIndex].updated = true;
          this.messageService.add({
            severity: 'info',
            summary: this.translateService.instant('Nothing Saved!'),
            detail: this.translateService.instant("Click on save button to submit the new value")
          });
        }
      }
      this.rows = [...this.rows];
    }
  }


  /**
  * ? update the view without sending data to backend
  * 
  * @param event html input element
  * @param cell selected cell
  * @param rowIndex index of data table
  */
  updateShift(value, cell, rowIndex) {
    this.editing[rowIndex + '-' + cell] = false;
    if (this.rows[rowIndex][cell] !== value) {
      this.rows[rowIndex][cell] = value;
      this.rows[rowIndex].updated = true;
      console.log('in updateShift', value);
    }
    this.rows = [...this.rows];
  }




  // /**
  //  * ? update the selected row in backend
  //  * 
  //  * @param row selected row
  //  * @param index index of data table
  //  */
  // IsExist(row, index) {
  //   this.saving['saving' + index] = true;



  //   // export const IsExistLanguagesUrl = 'SupportedLanguages/IsExist/';


  //   let request: Observable<any>;
  //   if (this.type == 'Area')
  //     request = this.lookUp.AreaIsExiste(row);
  //   else if (this.type == 'Attendance Status')
  //     request = this.lookUp.AttendanceStatesIsExiste(row);
  //   // else if (this.type == 'Order Status')
  //   //   request = this.lookUp.orderStat(row);



  //   else if (this.type == 'Idle Types' || this.type == 'Break Types')
  //     request = this.lookUp.editWorkingTypeLanguages(row);





  //   else if (this.type == 'Availability')
  //     request = this.lookUp.AvailabilityIsExiste(row);
  //   else if (this.type == 'Building Types')
  //     request = this.lookUp.BuildingTypesIsExiste(row);
  //   else if (this.type == 'Company Code')
  //     request = this.lookUp.CompanyCodeIsExiste(row);
  //   else if (this.type == 'Contract Type')
  //     request = this.lookUp.ContractTypesIsExiste(row);
  //   else if (this.type == 'Cost Center')
  //     request = this.lookUp.CostCenterIsExiste(row);
  //   else if (this.type == 'Division')
  //     request = this.lookUp.DivisionIsExiste(row);
  //   else if (this.type == 'Governorates')
  //     request = this.lookUp.GovernoratesIsExiste(row)
  //   else if (this.type == 'Shift')
  //     request = this.lookUp.ShiftIsExiste(row)
  //   else if (this.type == 'Languages')
  //     request = this.lookUp.LanguagesIsExiste(row)
  //   // else if (this.type == 'Problems')
  //   //   request = this.lookUp.Problem(row)
  //   else if (this.type == 'Complete Sub-Status Types' ||
  //     this.type == 'On-Hold Sub-Status Types' ||
  //     this.type == 'Order Cancellation Reasons')
  //     this.lookUp.editOrderSubStatusLanguages(row)

  //   // request.subscribe(
  //   //   res => {
  //   //     this.saving['saving' + index] = false;
  //   //     this.updatedSuccessfully();
  //   //     this.onReject();
  //   //   }, (err:any) => {
  //   //     console.log(err);
  //   //     this.failedToUpdate();
  //   //     this.onReject();
  //   //   }
  //   // );


  //   //  else if (this.type == 'Idle Types' || this.type == 'Break Types') {
  //   //   this.lookUp.updateWorkingType(row)
  //   //     .subscribe(
  //   //       res => {
  //   //         this.saving['saving' + index] = false;
  //   //         this.messageService.add({
  //   //           severity: 'success',
  //   //           summary: 'Successful!',
  //   //           detail: "Updated Successfully!"
  //   //         });
  //   //         this.onReject();
  //   //       }, (err:any) => {
  //   //         console.log(err);
  //   //         this.messageService.add({
  //   //           severity: 'error',
  //   //           summary: 'Failed!',
  //   //           detail: "Failed to update item!"
  //   //         });
  //   //         this.onReject();
  //   //       }
  //   //     );
  //   // }
  // }




  /**
   * ? delete all orders from DB
   */
  deleteAllOrders() {
    this.lookUp.deleteAllOrders()
      .subscribe(
        res => {
          if (res.isSucceeded == true) {
            this.showSuccessMsg('all orders have been deleted successfully');
          } else {
            this.showErrorMsg(res.status.msg);
          }
          this.onReject();
        },
        (err) => {
          this.showErrorMsg();
          console.log(err);
          this.onReject();
        }
      );
  }

  /**
   * delete all users from DB
   */
  deleteUserManagement() {
    this.lookUp.deleteAllUsersFromUserManagement()
      .subscribe(
        (res: any) => {
          if (res.isSucceeded == true) {
            this.lookUp.deleteAllUsersFromIdentity()
              .subscribe(
                res => {
                  if (res.isSucceeded == true) {
                    this.showSuccessMsg('all Users have been deleted successfully');
                  }
                  else {
                    this.messageService.add({
                      severity: 'error',
                      summary: this.translateService.instant('Failed!'),
                      detail: this.translateService.instant(res.status.message)
                    });
                  }
                  //else {
                  //  this.showErrorMsg(res.status.message);
                  //  }
                  this.onReject();
                }, (err: any) => {
                  this.showErrorMsg();
                  console.error(err);
                  this.onReject();
                }
              );
          } else {
            this.messageService.add({
              severity: 'error',
              summary: this.translateService.instant('Failed!'),
              detail: this.translateService.instant(res.status.message)
            });
            this.onReject();
            //this.showErrorMsg();
          }
        },
        err => {
          this.onServiceFailure(err);
        }


        // (err: any) => {
        //   this.showErrorMsg();
        //   console.error(err);
        //   this.onReject();
        // }
      );
  }

  /**
   * ? delete all dispatcher settings from DB
   */
  deleteDispatcherSettings() {
    this.lookUp.deleteDispatcherSettings().subscribe(
      (res: any) => {
        if (res.isSucceeded == true) {
          this.showSuccessMsg('all dispatcher settings have been deleted successfully');
        } else {
          this.showErrorMsg(res.status.msg);
        }
        this.onReject();
      }, (err: any) => {
        this.showErrorMsg();
        console.error(err);
        this.onReject();
      }
    );
  }


  failedToUpdate(err, index, msg = "Failed to update item!") {
    this.resetValue(index);
    console.error(err)
    this.onReject();
    this.showErrorMsg(msg);
  }

  restoreSingleRowValueAndLocalizeData(index: number, cashedListName: string, localizationNeeded: boolean) {
    if (cashedListName == "ShiftWithBlocked") {
      let clonedData = Object.assign({}, this.masterDataService[cashedListName][index]);
      clonedData.fromTime = new Date((clonedData.fromTime))
      clonedData.toTime = new Date((clonedData.toTime))
      this.rows[index] = clonedData;
    } else {
      this.rows[index] = JSON.parse(JSON.stringify(this.masterDataService[cashedListName][index]));
    }
    this.rows = [...this.rows];
    if (localizationNeeded) {
      this.generateColumns();
    }
  }

  resetValue(index: number) {
    this.saving['saving' + index] = false;
    if (this.type == 'Area') {
      this.restoreSingleRowValueAndLocalizeData(index, 'AreasWithBlocked', true);
    } else if (this.type == 'Attendance Status') {
      this.restoreSingleRowValueAndLocalizeData(index, 'AttendanceStatesWithBlocked', true);
    } else if (this.type == 'Order Status') {
      this.restoreSingleRowValueAndLocalizeData(index, 'OrderStatusWithBlocked', true);
    } else if (this.type == 'Contract Type') {
      this.restoreSingleRowValueAndLocalizeData(index, 'ContractTypesWithBlocked', true);
    } else if (this.type == 'Cost Center') {
      this.restoreSingleRowValueAndLocalizeData(index, 'CostCenterWithBlocked', false);
    } else if (this.type == 'Division') {
      this.restoreSingleRowValueAndLocalizeData(index, 'DivisionWithBlocked', true);
    } else if (this.type == 'Shift') {
      this.restoreSingleRowValueAndLocalizeData(index, 'ShiftWithBlocked', false);
    } else if (this.type == 'Languages') {
      this.restoreSingleRowValueAndLocalizeData(index, 'languagesWithBlocked', false);
    } else if (this.type == 'Problems') {
      this.restoreSingleRowValueAndLocalizeData(index, 'ProblemsLocalizedWithBlocked', true);
    } else if (this.type == 'Idle Types') {
      this.restoreSingleRowValueAndLocalizeData(index, 'IdleTypes', true);
    } else if (this.type == 'Break Types') {
      this.restoreSingleRowValueAndLocalizeData(index, 'BreakTypes', true);
    } else if (this.type == 'On-Hold Sub-Status Types') {
      this.restoreSingleRowValueAndLocalizeData(index, 'OnHoldSubStatusWithBlocked', true);
    } else if (this.type == 'Order Cancellation Reasons') {
      this.restoreSingleRowValueAndLocalizeData(index, 'CancellationSubStatusWithBlocked', true);
    } else if (this.type == 'Complete Sub-Status Types') {
      this.restoreSingleRowValueAndLocalizeData(index, 'CompleteSubStatusWithBlocked', true);
    }
  }

  /**
   * show Success messages
   * 
   * 
   * @param msg 
   */
  showSuccessMsg(msg: string = 'Successful!') {
    this.messageService.add({
      severity: 'success',
      summary: this.translateService.instant('Successful!'),
      detail: this.translateService.instant(msg)
    });
  }

  /**
   * show error messages
   * 
   * 
   * @param msg 
   */
  showErrorMsg(msg: string = "Failed due to server error!") {
    this.messageService.add({
      severity: 'error',
      summary: this.translateService.instant('Failed!'),
      detail: this.translateService.instant(msg)
    });
  }

}

