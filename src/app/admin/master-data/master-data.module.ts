import { NgModule } from '@angular/core';
import { SharedModuleModule } from '../../shared-module/shared-module.module';
import { MasterDataContainerComponent } from './master-data-container/master-data-container.component';
import { RouterModule, Routes, } from '@angular/router';
import { MasterDataPromptComponent } from './master-data-prompt/master-data-prompt.component.';
import { MasterDataComponent } from './master-data/master-data.component';
import { TranslateModule } from '@ngx-translate/core';

const routes: Routes = [
  { path: "", component: MasterDataContainerComponent }
];

@NgModule({
  declarations: [
    MasterDataContainerComponent,
    MasterDataComponent,
    MasterDataPromptComponent,
  ],
  imports: [
    SharedModuleModule,
    RouterModule.forChild(routes),
    TranslateModule
  ], entryComponents: [
    MasterDataPromptComponent,
  ]
})
export class MasterDataModule { }
