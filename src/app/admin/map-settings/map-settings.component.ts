import { Component, OnInit } from '@angular/core';
import { LookupService } from '../../api-module/services/lookup-services/lookup.service';
import { MessageService } from 'primeng/primeng';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngrx/store';
import * as fromRefreshRate from 'src/app/store/refresh-rate.reducer';
import * as RefreshRateActions from 'src/app/store/refresh-rate.actions';

@Component({
  selector: 'app-map-settings',
  templateUrl: './map-settings.component.html',
  styleUrls: ['./map-settings.component.css']
})
export class MapSettingsComponent implements OnInit {
  isLoading: boolean = false;
  settings: any = {}
  numberOfServicesFinished: number = 0;
  numberOfServicesCalled: number = 0;
  userId: string = "";

  constructor(private lookupService: LookupService,
    private messageService: MessageService,
    private translateService: TranslateService,
    private store: Store<fromRefreshRate.AppState>,
  ) { }

  ngOnInit() {
    this.isLoading = true;
    this.lookupService.getMapSettings(1).subscribe((response) => {
      if (response && response.isSucceeded) {
        this.isLoading = false;
        this.settings.APIKeyForWeb = response.data.mobile_Api_Key;
        this.settings.APIKeyForMobile = response.data.web_Api_Key;
        this.settings.RefreshRate = response.data.refresh_Rate;
      }
    });
  }



  checkIfServicesFinished() {
    this.numberOfServicesFinished++;
    if (this.numberOfServicesFinished == this.numberOfServicesCalled) {
      this.isLoading = false;
    }
  }

  seriveCalled() {
    this.isLoading = true;
    this.numberOfServicesCalled++;
  }


  onSaveSettings() {
    if (this.settings.RefreshRate >= 1) {
      let obj = {
        mobile_Api_Key: this.settings.APIKeyForMobile,
        web_Api_Key: this.settings.APIKeyForWeb,
        refresh_Rate: this.settings.RefreshRate,
        id: 1,
      };

      this.isLoading = true;
      this.lookupService.putMapSettings(obj).subscribe(
        (response) => {
          if (response && response.isSucceeded) {
            this.isLoading = false;
            this.messageService.add({
              severity: 'success',
              summary: this.translateService.instant('Successful!'),
              detail: this.translateService.instant(`info Edited & Saved Successfully!`)
            });
            this.store.select("refreshRateData").subscribe((storedRefreshRateState) => {
              if (storedRefreshRateState.refreshRate != this.settings.RefreshRate) {
                this.store.dispatch(new RefreshRateActions.UpdateRefreshRate(this.settings.RefreshRate));
              }
            })
          } else {
            this.isLoading = false;
            this.messageService.add({
              severity: 'error',
              summary: this.translateService.instant('invalid values!'),
              detail: response.status.message
            });

          }
        }, (error) => {
          this.isLoading = false;
          this.messageService.add({
            severity: 'error',
            summary: this.translateService.instant('error!'),
            detail: error
          });
        });
    } else {
      this.messageService.add({
        severity: 'error',
        summary: this.translateService.instant('error!'),
        detail: this.translateService.instant('refresh rate must be greated Than 1 minute')
      });
    }

  }


}
