import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-team-management',
  templateUrl: './team-management.component.html',
  styleUrls: ['./team-management.component.css']
})
export class TeamManagementComponent implements OnInit {

  selectedTab: string = "add";
  constructor() { }

  ngOnInit() {
  }

  selectTab(tabName:string){
    this.selectedTab = tabName;
  }


}