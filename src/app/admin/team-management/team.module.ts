import { NgModule } from '@angular/core';
import { SharedModuleModule } from '../../shared-module/shared-module.module';
import { AddTeamComponent } from './add-team/add-team.component';
import { EditTeamComponent } from './edit-team/edit-team.component';
import { VehicleComponent } from './vehicle/vehicle.component';
import { VehicleUpdateModalComponent } from './vehicle/vehicle-update-modal/vehicle-update-modal.component';
import { RouterModule } from '@angular/router';
import { teamRoutes } from './team-routes';
import { TeamManagementComponent } from './team-management.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    SharedModuleModule,
    RouterModule.forChild(teamRoutes),
    TranslateModule
  ],
  declarations: [
    TeamManagementComponent,
    AddTeamComponent,
    EditTeamComponent,
    VehicleComponent,
    VehicleUpdateModalComponent,
  ],
  exports: [
    TeamManagementComponent,
    AddTeamComponent,
    EditTeamComponent,
    VehicleComponent,
    VehicleUpdateModalComponent,
    RouterModule
  ], entryComponents: [
    VehicleUpdateModalComponent
  ]
})
export class TeamModule { }
