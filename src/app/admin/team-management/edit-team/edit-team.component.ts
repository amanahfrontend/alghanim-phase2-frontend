import { Component, OnInit, ViewChild } from '@angular/core';
import { LookupService } from "../../../api-module/services/lookup-services/lookup.service";
import { MessageService } from 'primeng/components/common/messageservice';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material';
import { ConfirmationDialog } from 'src/app/shared-module/confirm-dialog-component/confirm-dialog-component';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-edit-team',
  templateUrl: './edit-team.component.html',
  styleUrls: ['./edit-team.component.css']
})
export class EditTeamComponent implements OnInit {
  @ViewChild('editForm', { 'static': false }) editForm: NgForm;
  userObj: any = {
    name: "",
  };
  divisions: any = [];
  isSuccess: boolean = false;
  drivers: any = [];
  formans: any = [];
  divisionObj: any = {
    id: 0
  };
  driverObj: any = {};
  formanObj: any = {};
  availabilityObj: any = {};
  shiftObj: any = { id: null };

  objToPost: any = {};
  unassignedMembers: any = [];
  shifts: any = [];
  availabilities: any = [];
  memberToPost: any;
  memberTeamToPost: any;
  teamToPost: Array<any> = [];
  teams: any = [];
  cashedSelectedTeam: any = {
    members: [],
    vehicle: {},
    driver: {}
  };
  unassignedVehicles: Array<any> = [];
  currentTeam: any = {
    members: [],
    vehicle: {},
    driver: {}
  };
  numberOfServicesFinished: number = 0;
  isLoading: boolean = false;
  PF: string = "";
  plateNo: string = "";
  teamName: string = "";
  numberOfServicesCalled = 0;
  disableChangeDivision: boolean = false;
  currentlyUnassignedMembers = [];
  curentlyUnassignedVehicles = [];
  allForemen: any = [];
  cashedTeams: any = [];
  // allForemen: any = [];
  // allForemen: any = [];
  reInitCurrentTeamData = function () {
    this.editForm.form.markAsDirty();
    this.userObj = { name: "" };


    this.divisionObj = {
      id: 0
    };
    this.formanObj = {};
    this.shiftObj = { id: null };
    this.availabilityObj = {};

    this.currentTeam = {
      members: [],
      vehicle: {},
      driver: {}
    };
    this.plateNo = "";
    this.teamName = "";
  }





  constructor(
    private dialog: MatDialog,
    private lookUp: LookupService,
    private messageService: MessageService,
    private translateService: TranslateService) { }


  ngOnInit() {
    this.getDivisions();
    this.getUnassignedVehicles();
    this.getTeams();
    this.getAllFormen();
    this.getShifts();
    this.getavailabilities();
  }


  assinTeamName(member) {
    this.editForm.form.markAsDirty();
    this.userObj.name = member.memberPhone ? (member.memberName + " - " + member.memberPhone) : (member.memberName);
  }


  checkIfServicesFinished() {
    this.numberOfServicesFinished++;
    if (this.numberOfServicesFinished == this.numberOfServicesCalled) {
      this.isLoading = false;
    }
  }

  seriveCalled() {
    this.isLoading = true;
    this.numberOfServicesCalled++;
  }

  onServiceFailure(err, msg = "Failed to get data due to network error.") {
    console.error(err);
    this.messageService.add({
      severity: 'error',
      summary: this.translateService.instant('Failed!'),
      detail: this.translateService.instant(msg)
    });
    this.checkIfServicesFinished();
  }

  compareByOptionId(idFist, idSecond) {
    return idFist && idSecond && idFist.id == idSecond.id;
  }

  getavailabilities() {
    this.seriveCalled();
    this.lookUp.getAvailabilities()
      .subscribe(
        result => {
          this.availabilities = result.data;
          this.checkIfServicesFinished();
        }, err => {
          this.onServiceFailure(err);
        }
      );
  }


  getShifts() {
    this.seriveCalled();
    this.lookUp.getShift()
      .subscribe(
        result => {
          this.shifts = result.data;
          this.checkIfServicesFinished();
        }, err => {
          this.onServiceFailure(err);
        }
      );
  }

  getTeams() {
    this.teams = [];
    this.seriveCalled();
    this.lookUp.getAllTeams()
      .subscribe(
        result => {
          this.teams = result.data;
          this.cashedTeams = this.teams;
          this.checkIfServicesFinished();
        }, err => {
          this.onServiceFailure(err);
        }
      );
  }


  getDivisions() {
    this.seriveCalled();
    this.lookUp.getDivisionEnUrl()
      .subscribe(
        result => {
          this.divisions = result.data;
          // this.divisionObj = this.divisions[0]
          this.checkIfServicesFinished();
        }, err => {
          this.onServiceFailure(err);
        }
      );
  }

  getUnassignedVehicles() {
    this.seriveCalled();
    this.lookUp.getAllUnassignedVehicles()
      .subscribe(
        result => {
          this.unassignedVehicles = result.data;
          this.unassignedVehicles.map(
            (v, i) => {
              if (this.currentTeam.vehicle.id && this.currentTeam.vehicle.memberParentName == v.memberParentName) {
                this.unassignedVehicles.splice(i, 1);
              }
            }
          );
          this.curentlyUnassignedVehicles = this.unassignedVehicles;
          this.checkIfServicesFinished();
        }, err => {
          this.onServiceFailure(err);
        }
      );
  }

  getUnassignedMembers(divisionId) {
    this.seriveCalled();
    this.lookUp.getAllUnassignedMempersByDivision(divisionId)
      .subscribe(
        result => {
          this.unassignedMembers = result.data;
          for (let i = (this.unassignedMembers.length - 1); i >= 0; i--) {
            if (this.unassignedMembers[i].isDriver &&
              this.currentTeam.driver.id &&
              this.currentTeam.driver.memberParentId == this.unassignedMembers[i].memberParentId) {
              this.unassignedMembers.splice(i, 1);
            }
            if (!this.unassignedMembers[i].isDriver && this.currentTeam.members.length > 0) {
              this.currentTeam.members.map(
                (member: any) => {
                  if (member.memberParentId == this.unassignedMembers[i].memberParentId) {
                    this.unassignedMembers.splice(i, 1);
                  }
                }
              );
            }
          }
          this.currentlyUnassignedMembers = this.unassignedMembers;
          this.checkIfServicesFinished();
        }, err => {
          this.onServiceFailure(err);
        }
      );
  }

  onChangeDivision(divisionId) {
    this.getUnassignedMembers(divisionId);
    this.divisionObj.divisionName = this.divisions.filter((d) => d.id == divisionId)[0].name;
    this.formans = this.allForemen.filter((f) => f.divisionId == divisionId);
  }

  getAllFormen() {
    this.seriveCalled();
    this.lookUp.getAllForemans()
      .subscribe(
        result => {
          this.formans = result.data;
          this.allForemen = result.data;
          this.checkIfServicesFinished();
        }, err => {
          this.onServiceFailure(err);
        }
      );
  }

  assignVehicle(vehicle: any, index: number) {
    if (this.currentTeam.vehicle.id && this.currentTeam.vehicle.memberParentName) {
      this.unassignedVehicles.push(this.currentTeam.vehicle);
    }
    this.currentTeam.vehicle = vehicle;
    this.unassignedVehicles.splice(index, 1);
    this.curentlyUnassignedVehicles = this.unassignedVehicles;
  }

  addTech(tech: any, i: number) {
    this.disableChangeDivision = true;
    if (tech.isDriver) {
      if (this.currentTeam.driver.id) {
        this.unassignedMembers.push(this.currentTeam.driver);
      }
      this.currentTeam.driver = tech;
    } else {
      this.currentTeam.members.push(tech);
    }
    this.unassignedMembers.splice(i, 1);
    this.currentlyUnassignedMembers = this.unassignedMembers;
  }

  removeDriver(driver) {

    this.unassignedMembers.unshift(driver);
    if (this.userObj.name == driver.memberName + ' - ' + driver.memberPhone) {
      this.userObj.name = "";
    }
    if (this.currentTeam.members.length == 0) {
      this.disableChangeDivision = false;
    }
    this.currentTeam.driver = {};
    this.currentlyUnassignedMembers = this.unassignedMembers;
  }

  removeTeamMember(member: any, index: number) {
    this.unassignedMembers.unshift(member);
    this.currentTeam.members.splice(index, 1);
    if (this.userObj.name == member.memberName + ' - ' + member.memberPhone) {
      this.userObj.name = "";
    }
    if (this.currentTeam.members.length == 0 && !this.currentTeam.driver.id) {
      this.disableChangeDivision = false;
    }
    this.currentlyUnassignedMembers = this.unassignedMembers;
  }

  removeVehicle(vehicle) {
    this.unassignedVehicles.unshift(vehicle);
    this.currentTeam.vehicle = {};
    this.curentlyUnassignedVehicles = this.unassignedVehicles;
  }

  getTeamsBeforeSearch() {
    this.teamName = "";
    this.teams = this.cashedTeams;
  }

  getTechsBeforSearch() {
    this.PF = "";
    this.unassignedMembers = this.currentlyUnassignedMembers;
  }

  getVehiclesBeforeSearch() {
    this.plateNo = "";
    this.unassignedVehicles = this.curentlyUnassignedVehicles;
  }

  onTechSearch() {
    this.unassignedMembers = [];
    this.currentlyUnassignedMembers.map((cashedMember) => {
      if (cashedMember.pf.indexOf(this.PF) > -1) {
        this.unassignedMembers.push(cashedMember);
      }
    });
  }

  onTeamSearch() {
    this.teams = [];
    this.cashedTeams.map((cashedTeam: any) => {
      if (cashedTeam.name.toLowerCase().indexOf(this.teamName.toLowerCase()) > -1) {
        this.teams.push(cashedTeam);
      }
    });
  }

  onDeleteTeam(id) {
    debugger
    this.seriveCalled();
    let membersIdArray = []
    this.lookUp.deleteTeam(id)
      .subscribe(
        (res: any) => {
          this.checkIfServicesFinished();
          if (res.isSucceeded) {
            this.unassignedMembers = [];
            this.reInitCurrentTeamData();
            this.messageService.add({
              severity: 'success',
              summary: this.translateService.instant('Success!'),
              detail: this.translateService.instant('Team Deleted Successfully!')
            });
          } else {
            this.messageService.add({
              severity: 'error',
              summary: this.translateService.instant('Failed!'),
              detail: this.translateService.instant(res.status.message)
            });

          }
          this.getTeams();


        }, err => {
          this.onServiceFailure(err);
        }
      );
  }


  onVehicleSearch() {
    this.unassignedVehicles = [];
    this.curentlyUnassignedVehicles.map((cashedVehicle) => {
      if (cashedVehicle.memberParentName.indexOf(this.plateNo) > -1) {
        this.unassignedVehicles.push(cashedVehicle);
      }
    });
  }

  GetMembersByTeamId(id) {
    this.seriveCalled();
    this.currentTeam.members = [];
    this.currentTeam.driver = {};
    this.currentTeam.vehicle = {};
    this.cashedSelectedTeam.members = [];
    this.cashedSelectedTeam.driver = {};
    this.cashedSelectedTeam.vehicle = {};;
    this.lookUp.getMembersByTeamId(id)
      .subscribe(
        (result) => {
          this.checkIfServicesFinished();
          result.data.map(
            (member) => {
              if (member.memberType == 3) {
                this.currentTeam.vehicle = member;
                this.cashedSelectedTeam.vehicle = member;
              } else if (member.isDriver) {
                this.currentTeam.driver = member;
                this.cashedSelectedTeam.driver = member;
                this.disableChangeDivision = true;
              } else {
                this.currentTeam.members.push(member);
                this.cashedSelectedTeam.members.push(member);
                this.disableChangeDivision = true;
              }
            }
          );
        }, err => {
          this.onServiceFailure(err);
        }
      );
  }

  sameMembers() {
    return JSON.stringify(this.currentTeam.members) === JSON.stringify(this.cashedSelectedTeam.members) && this.cashedSelectedTeam.driver.id == this.currentTeam.driver.id && this.cashedSelectedTeam.vehicle.id == this.currentTeam.vehicle.id;
  }

  onSelectTeam(team) {
    this.disableChangeDivision = false;
    this.divisionObj.name = team.divisionName;
    this.divisionObj.id = team.divisionId;
    this.onChangeDivision(this.divisionObj.id);
    this.shiftObj.id = team.shiftId || this.shifts[0].id;
    if (team.statusId) {
      this.availabilityObj.id = team.statusId;
    }
    this.userObj.name = team.name;
    this.currentTeam.id = team.id;
    this.currentTeam.ordersCount = team.ordersCount
    this.GetMembersByTeamId(team.id);
    this.getUnassignedVehicles();
    this.getUnassignedMembers(team.divisionId);
    setTimeout(() => {
      this.cashedSelectedTeam.foremanId = team.foremanId;
      this.formanObj.id = team.foremanId;
      this.formanObj.name = team.foremanName;
    }, 0);
  }

  onSubmit() {
    if (!this.userObj.name) {
      this.messageService.add({
        severity: 'error',
        summary: this.translateService.instant('Failed!'),
        detail: this.translateService.instant('Select name to submit')
      });
    } else if (!this.divisionObj.id && this.divisionObj.id !== 0) {
      this.messageService.add({
        severity: 'error',
        summary: this.translateService.instant('Failed!'),
        detail: this.translateService.instant('Select Division to submit')
      });
    } else if (!this.formanObj.id && this.formanObj.id !== 0) {
      this.messageService.add({
        severity: 'error',
        summary: this.translateService.instant('Failed!'),
        detail: this.translateService.instant('Select Forman to submit')
      });
    } else if (!this.shiftObj.id) {
      this.messageService.add({
        severity: 'error',
        summary: this.translateService.instant('Failed!'),
        detail: this.translateService.instant('Select Shift to submit')
      });
    } else if (!this.availabilityObj.id) {
      this.messageService.add({
        severity: 'error',
        summary: this.translateService.instant('Failed!'),
        detail: this.translateService.instant('Select Availability to submit')
      });
    } else {
      this.teamToPost = [];
      this.objToPost = {
        divisionId: this.divisionObj.id,
        divisionName: this.divisionObj.name,
        foremanId: this.formanObj.id,
        foremanName: this.formanObj.name,
        name: this.userObj.name,
        id: this.currentTeam.id,
        ShiftId: this.shiftObj.id,
        statusId: this.availabilityObj.id,
        statusName: this.availabilityObj.name,
      }
      if (this.currentTeam.vehicle.id) {
        this.teamToPost.push(this.currentTeam.vehicle.id);
      }
      if (this.currentTeam.driver.id) {
        this.teamToPost.push(this.currentTeam.driver.id);
      }
      this.currentTeam.members.forEach(member => {
        if (this.objToPost.divisionId === member.divisionId) {
          this.teamToPost.push(member.id);
        } else {
          this.messageService.add({
            severity: 'error',
            summary: this.translateService.instant('Failed!'),
            detail: this.translateService.instant('Select Technicians from same division to submit')
          });
          return false;
        }
      });
      if (this.currentTeam.ordersCount && this.formanObj.id != this.cashedSelectedTeam.foremanId) {
        this.messageService.add({
          severity: 'error',
          summary: this.translateService.instant('Failed!'),
          detail: this.translateService.instant('Remove Assigned Orders from team to change foreman')
        });
        return false;
      }
      this.seriveCalled();
      this.lookUp.updateTeam(this.objToPost)
        .subscribe(
          (teamCreationResponse) => {
            if (teamCreationResponse.isSucceeded) {

              let teamIdWithMembers = {
                teamId: this.objToPost.id,
                members: this.teamToPost
              };
              this.lookUp.assginMembersToTeam(teamIdWithMembers)
                .subscribe(
                  (res: any) => {
                    this.checkIfServicesFinished();
                    this.getTeams();
                    this.messageService.add({
                      severity: 'success',
                      summary: this.translateService.instant('Success!'),
                      detail: this.translateService.instant('Team Updated Successfully!')
                    });
                  }, err => {
                    this.onServiceFailure(err);
                  }
                );
            } else if (teamCreationResponse.status.code == 7) {
              this.checkIfServicesFinished();
              this.messageService.add({
                severity: 'error',
                summary: this.translateService.instant('Failed!'),
                detail: this.translateService.instant('The Team Name already existed!')
              });
            } else {
              this.onServiceFailure(teamCreationResponse, teamCreationResponse.status.message);
            }
          }, err => {
            this.onServiceFailure(err);
          }
        );
    }
  }

  openConfirmationOnDeleteMember(member, i) {
    let MemberTypeID = member.memberType;
    console.log(member);
    var MemberTypeName: string;

    if (member.isDriver == false) {
      MemberTypeName = "Technician";
    }
    else if (member.isDriver = true) {
      MemberTypeName = "Driver";
    }
    const dialogRef = this.dialog.open(ConfirmationDialog, {
      data: {
        message: `Are you sure want to remove This ${MemberTypeName} ( ${member.fullName} ) ?`,
        cancelButtonText: "Cancel",
        confirmButtonText: this.translateService.instant('Confirm to proceed'),
      }
    });
    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      if (confirmed && MemberTypeName == "Technician") {
        this.removeTeamMember(member, i);
      }
      else if (confirmed && MemberTypeName == "Driver") {
        this.removeDriver(member);
      }
      else {
        //if false or undegiend
      }
    });
  }




  openConfirmationOnDeleteTeam(team: any) {
    console.log(team);

    const dialogRef = this.dialog.open(ConfirmationDialog, {
      data: {
        message: `Are you sure want to delete This Team ( ${team.name} ) ?`,
        cancelButtonText: "Cancel",
        confirmButtonText: this.translateService.instant('Confirm to proceed'),
      }
    });

    dialogRef.afterClosed().subscribe((confirmed: boolean) => {
      if (confirmed) {
        this.onDeleteTeam(team.id);
      }
      else {
        //if false or undegiend

      }
    });
  }





}
