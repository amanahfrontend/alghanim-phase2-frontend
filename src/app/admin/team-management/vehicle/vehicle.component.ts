import { Component, OnInit } from '@angular/core';
import { LookupService } from '../../../api-module/services/lookup-services/lookup.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { VehicleUpdateModalComponent } from './vehicle-update-modal/vehicle-update-modal.component';
import { MessageService } from 'primeng/primeng';
import { VehicleService } from './vehicle.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.css']
})
export class VehicleComponent implements OnInit {
  vehicles: any[];
  cashedVehicles: any[];
  columns: any[];
  editing: any = {};
  toggleLoading: boolean;
  isLoading: boolean;

  constructor(private lookup: LookupService,
    private modalService: NgbModal,
    private messageService: MessageService,
    private vehicleService: VehicleService,
    private translateService: TranslateService) { }

  onServiceFailure(error) {
    console.error(error)
    this.messageService.add({
      severity: 'error',
      summary: this.translateService.instant('error!'),
      detail: this.translateService.instant('Failed due to network error.')
    });
  }

  ngOnInit() {
    this.toggleLoading = true;
    this.lookup.getAllVehicles()
      .subscribe(
        (results) => {
          this.toggleLoading = false;
          this.vehicles = results.data;
          this.cashedVehicles = JSON.parse(JSON.stringify(this.vehicles));
        }, error => {
          this.onServiceFailure(error);
        }
      );
    this.vehicleService.getVehicle
      .subscribe(
        (res: any) => {
          this.isLoading = true;
          this.lookup.getAllVehicles().subscribe(
            (results) => {
              this.isLoading = false;
              this.vehicles = results.data;
              this.cashedVehicles = JSON.parse(JSON.stringify(this.vehicles));
            }, error => {
              this.onServiceFailure(error);
            }
          );
        }, error => {
          this.onServiceFailure(error);
        }
      );
  }

  onRowClick(row) {
    let modalRef = this.modalService.open(VehicleUpdateModalComponent);
    modalRef.componentInstance.data = row;
    modalRef.result
      .catch(
        () => {
          this.messageService.add({
            severity: 'info',
            summary: this.translateService.instant('Nothing Edited!'),
            detail: this.translateService.instant("You didn't change the old value")
          });
        }
      );
  }

  add() {
    let modalRef = this.modalService.open(VehicleUpdateModalComponent);
    modalRef.result
      .catch(
        () => {
          this.messageService.add({
            severity: 'info',
            summary: this.translateService.instant('Nothing Edited!'),
            detail: this.translateService.instant("You didn't change the old value")
          });
        }
      );
  }


  updateValue(event, cell, rowIndex) {
    this.editing[rowIndex + '-' + cell] = false;
    if (this.vehicles[rowIndex][cell] !== event.target.value) {
      this.vehicles[rowIndex][cell] = event.target.value;
      this.vehicles[rowIndex].updated = true;
      this.messageService.add({
        severity: 'info',
        summary: this.translateService.instant('Nothing Saved!'),
        detail: this.translateService.instant("Click on save button to submit the new value")
      });
    }
    this.vehicles = [...this.vehicles];
  }

  save(row, index) {
    this.isLoading = true;
    this.lookup.updateVehcile(row)
      .subscribe(res => {
        this.isLoading = false;
        if (res.isSucceeded) {
          this.vehicles[index].updated = false;
          this.messageService.add({
            severity: 'success',
            summary: this.translateService.instant('Successful!'),
            detail: this.translateService.instant("Saved Successfully!")
          });
        } else if (!res.isSucceeded && res.status.code == 6) {
          this.vehicles[index] = this.cashedVehicles[index];
          this.vehicles = [...this.vehicles];
          this.messageService.add({
            severity: 'error',
            summary: this.translateService.instant('Value existed before'),
            detail: this.translateService.instant(res.status.message)
          });
        } else {
          this.onServiceFailure(res);
          this.vehicles[index] = this.cashedVehicles[index];
          this.vehicles = [...this.vehicles];
        }
      }, error => {
        this.onServiceFailure(error);
      }
      );
  }

}
