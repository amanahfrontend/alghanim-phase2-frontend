// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyA0aZ9Jo4VMbPBEfKEAKyu8Wz06-AwmVf4",
    authDomain: "ghanim-a8d21.firebaseapp.com",
    databaseURL: "https://ghanim-a8d21.firebaseio.com",
    projectId: "ghanim-a8d21",
    storageBucket: "ghanim-a8d21.appspot.com",
    messagingSenderId: "663318682005",
    appId: "1:663318682005:android:0e6c39930b8eb088"
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
